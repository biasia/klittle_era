package cn.oi.klittle.era.widget.compat.layout.relativeLayout

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
//fixme 子View能够直接识别和使用.lparams{}属性。亲测
open class _KRelativeLayout(ctx: Context) : KShadowLayout(ctx) {

    inline fun <T : View> T.lparams(
            c: Context?,
            attrs: AttributeSet?,
            init: RelativeLayout.LayoutParams.() -> Unit
    ): T {
        val layoutParams = RelativeLayout.LayoutParams(c!!, attrs!!)
        layoutParams.init()
        this@lparams.layoutParams = layoutParams
        return this
    }

    inline fun <T : View> T.lparams(
            c: Context?,
            attrs: AttributeSet?
    ): T {
        val layoutParams = RelativeLayout.LayoutParams(c!!, attrs!!)
        this@lparams.layoutParams = layoutParams
        return this
    }

    inline fun <T : View> T.lparams(
            width: Int = android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
            height: Int = android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
            init: RelativeLayout.LayoutParams.() -> Unit
    ): T {
        val layoutParams = RelativeLayout.LayoutParams(width, height)
        layoutParams.init()
        this@lparams.layoutParams = layoutParams
        return this
    }

    inline fun <T : View> T.lparams(
            width: Int = android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
            height: Int = android.view.ViewGroup.LayoutParams.WRAP_CONTENT
    ): T {
        val layoutParams = RelativeLayout.LayoutParams(width, height)
        this@lparams.layoutParams = layoutParams
        return this
    }

    inline fun <T : View> T.lparams(
            source: ViewGroup.LayoutParams?,
            init: RelativeLayout.LayoutParams.() -> Unit
    ): T {
        val layoutParams = RelativeLayout.LayoutParams(source!!)
        layoutParams.init()
        this@lparams.layoutParams = layoutParams
        return this
    }

    inline fun <T : View> T.lparams(
            source: ViewGroup.LayoutParams?
    ): T {
        val layoutParams = RelativeLayout.LayoutParams(source!!)
        this@lparams.layoutParams = layoutParams
        return this
    }

    inline fun <T : View> T.lparams(
            source: ViewGroup.MarginLayoutParams?,
            init: RelativeLayout.LayoutParams.() -> Unit
    ): T {
        val layoutParams = RelativeLayout.LayoutParams(source!!)
        layoutParams.init()
        this@lparams.layoutParams = layoutParams
        return this
    }

    inline fun <T : View> T.lparams(
            source: ViewGroup.MarginLayoutParams?
    ): T {
        val layoutParams = RelativeLayout.LayoutParams(source!!)
        this@lparams.layoutParams = layoutParams
        return this
    }

//    inline fun <T: View> T.lparams(
//            source: RelativeLayout.LayoutParams?,
//            init: RelativeLayout.LayoutParams.() -> Unit
//    ): T {
//        val layoutParams = RelativeLayout.LayoutParams(source!!)
//        layoutParams.init()
//        this@lparams.layoutParams = layoutParams
//        return this
//    }
//
//    inline fun <T: View> T.lparams(
//            source: RelativeLayout.LayoutParams?
//    ): T {
//        val layoutParams = RelativeLayout.LayoutParams(source!!)
//        this@lparams.layoutParams = layoutParams
//        return this
//    }

}