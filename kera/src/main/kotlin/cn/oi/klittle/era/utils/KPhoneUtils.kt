package cn.oi.klittle.era.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.net.Uri
import android.provider.ContactsContract
import android.telephony.TelephonyManager
import cn.oi.klittle.era.base.KBaseApplication
import cn.oi.klittle.era.exception.KCatchException
import cn.oi.klittle.era.utils.KLoggerUtils.e

object KPhoneUtils {
    /**
     * 获取当前设备的手机号
     *
     * @return
     */
    @SuppressLint("MissingPermission")
    fun getDevicePhone(callback: ((phone: String?) -> Unit)? = null) {
        KPermissionUtils.requestPermissionsReadPhoneState() {
            if (it) {
                val context: Context = KBaseApplication.getInstance()
                val tm = context
                        .getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                //tm.deviceId
                var phone=tm.getLine1Number()
                //只能获取很老SIM卡的手机号码，目前主流的获取用户手机号码一般采用用户主动发送短信到SP或接收手机来获取
                //KLoggerUtils.e("line1Number:\t"+tm.line1Number);
                callback?.let { it(phone) }
            } else {
                KPermissionUtils.showFailure();
            }
        }
    }

    //                //处理返回的data,获取选择的联系人信息
    //                var uri: Uri = data.getData()
    //                var contacts: Array<String>? = KPhoneUtils.getPhoneContacts(uri, this)
    //                contacts?.let {
    //                    if (it.size >= 2) {
    //                        it[0]//名称
    //                        it[1]//手机号；fixme 默认格式是 153 1234 5678；中间有空格。KStringUtils.removeBlank(tel)可以去除中间的空格。
    //                    }
    //                }
    fun getPhoneContacts(uri: Uri?, activity: Activity?): Array<String?>? {
        if (uri == null || activity == null) {
            return null
        }
        try {
            val contact = arrayOfNulls<String>(2)
            //得到ContentResolver对象
            val cr = activity.contentResolver
            //取得电话本中开始一项的光标
            val cursor = cr.query(uri, null, null, null, null)
            if (cursor != null) {
                cursor.moveToFirst()
                //取得联系人姓名
                val nameFieldColumnIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
                contact[0] = cursor.getString(nameFieldColumnIndex)
                //取得电话号码
                val ContactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                val phone = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + ContactId, null, null)
                if (phone != null) {
                    phone.moveToFirst()
                    contact[1] = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                }
                phone!!.close()
                cursor.close()
            } else {
                return null
            }
            return contact
        } catch (e: Exception) {
            e("KPhoneUtils->手机通讯录，获取名称和号码异常：\t" + KCatchException.getExceptionMsg(e))
        }
        return null
    }

}