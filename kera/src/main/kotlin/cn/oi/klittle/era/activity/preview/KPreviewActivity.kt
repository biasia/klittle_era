package cn.oi.klittle.era.activity.preview

import android.os.Bundle
import android.view.View
import cn.oi.klittle.era.R
import cn.oi.klittle.era.activity.photo.manager.KPictureSelector
import cn.oi.klittle.era.base.KBaseActivity
import cn.oi.klittle.era.utils.KAssetsUtils
import cn.oi.klittle.era.utils.KLoggerUtils
import cn.oi.klittle.era.utils.KSharedUtils
import java.io.File

/**
 * fixme 图片预览;在KPictureSelector.openExternalPreview()方法里调用了
 * fixme 共享元素名称； transitionName = "share_kitem_img"；在viewPager上。
 */
open class KPreviewActivity : KBaseActivity() {

    var ui: KPreviewUi? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        //KLoggerUtils.e("getActivity:\t"+getActivity())
        try {
//            enterAnim = R.anim.kera_from_small_to_large_a5
//            exitAnim = R.anim.kera_from_large_to_small_a3
            super.onCreate(savedInstanceState)
            ui = KPreviewUi()
            setContentView(ui?.createView(this))
            ui?.setTitle()
            ui?.viewPager?.setCurrentItem(KPictureSelector.previewIndex, false)
            if (KPictureSelector.isCheckable) {
                ui?.checkNumCallback(KPictureSelector.currentSelectNum)
                //完成
                ui?.complete?.apply {
                    onClick {
                        //KLoggerUtils.e("点击：\t"+isSelected,true)
                        if (isSelected) {
                            complete()
                        }
                    }
                }
                ui?.num?.apply {
                    onClick {
                        if (visibility == View.VISIBLE) {
                            complete()
                        }
                    }
                }
            } else if (KPictureSelector.isSharedable) {
                //分享
                ui?.toolbar?.rightTextView?.onClick {
                    //分享
                    ui?.viewPager?.currentItem?.let {
                        var posiont = it
                        ui?.previewAdapter?.datas?.get(posiont)?.let {
                            KSharedUtils.shareFile(File(it.path))
                        }
                    }

                }
            } else {
            }
            isCompelete = false
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun finish() {
        super.finish()
        try {
            ui?.previewAdapter?.keys?.forEach {
                KAssetsUtils.getInstance().recycleBitmap(it.value)//释放位图
            }
            //ui?.destroy(this)//界面销毁，不需要调用。KBaseActivity里会自动调用。
            overridePendingTransition(0, R.anim.kera_from_large_to_small_a3)//对透明主题，动画可能无效。
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    //完成
    fun complete() {
        try {
            _showProgressbar()//显示进度条
            KPictureSelector.selectCallback() {
                isCompelete = true
                _dismissProgressbar()//关闭进度条
                setResult(KPictureSelector.resultCode_preview)
                finish()//关闭
            }//选中回调
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        var isCompelete = false//是否完成。
    }

    override fun isDestroyViewGroup(): Boolean {
        return true//销毁
    }

    override fun getDestroyPostDelayedTime(): Long {
        //return super.getDestroyPostDelayedTime()
        return 350L//fixme ui销毁延迟时间。kera_from_large_to_small_a3动画时间是200毫秒。
    }

    override fun isDestroyPostDelayed(): Boolean {
        //return false//fixme 不延迟，立即销毁。防止viewGroup里图片被异常释放。
        return true
    }


}
