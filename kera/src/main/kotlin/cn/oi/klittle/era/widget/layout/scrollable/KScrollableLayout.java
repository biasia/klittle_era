package cn.oi.klittle.era.widget.layout.scrollable;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.LinearLayout;
import android.widget.Scroller;

import androidx.viewpager.widget.ViewPager;
//                    滑动监听
//                    setOnScrollListener { currentY, maxY ->
//                        KLoggerUtils.e("currentY:\t" + currentY + "\tmaxY:\t" + maxY+"\t"+(maxY / 2))
//                    }
//            调用案例：
//            kscrollableLayout {
//                //fitsSystemWindows=true
//                //setCloseUpAlgorithm(DefaultCloseUpAlgorithm())
//                //maxScrollY=px.x(500)//指定置顶的位置；大于等于0时才有效。小于0无效。
//                //maxY也是滑动最大值，是自动计算出出来的。
//                orientation = LinearLayout.VERTICAL//继承是线性布局，最好设置一下竖屏。默认就是竖屏。
//                //fixme 第一部分会滚动
//                verticalLayout {
//                    for (i in 0..15) {
//                        textView {
//                            gravity = Gravity.CENTER
//                            text = i.toString()
//                            textSize = kpx.textSizeX(36)
//                            if (i % 2 == 0) {
//                                backgroundColor = Color.GREEN
//                            } else {
//                                backgroundColor = Color.GRAY
//                            }
//                        }.lparams {
//                            width = matchParent
//                            height = kpx.x(100)
//                        }
//                    }
//                }
//                //fixme 第二部分会悬浮置顶
//                verticalLayout {
//                    ktextView {
//                        gravity = Gravity.CENTER
//                        text = "悬浮置顶"
//                        textSize = kpx.textSizeX(36)
//                        textColor = Color.WHITE
//                        isBold(true)
//                        backgroundColor = Color.BLUE
//                        onClick {
//                            //滑动到原点，没有动画效果。本来想添加动画效果的。但是会冲突，所以就不加了。
//                            this@kscrollableLayout.scrollTo(0, 0)//点击滑动到原点
//                            //this@apply.scrollBy(0,-this@apply.scrollY)
//                        }
//                    }.lparams {
//                        width = matchParent
//                        height = kpx.x(100)
//                    }
//                }
//                //fixme 第三部分在悬浮部分的下面。
//                var recyclerView = krecyclerView {
//                    var linearLayoutManager = LinearLayoutManager(ctx)
//                    linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
//                    layoutManager = linearLayoutManager
//                    var datas = mutableListOf<String>()
//                    for (i in 0..100) {
//                        datas.add(i.toString())
//                    }
//                    adapter = KRecyclerAdapter(datas)
//                }
//                //fixme 支持这些控件<!--ScrollView support ViewPager, RecyclerView, ScrollView, ListView, WebView-->
//                //fixme 这个方法必不可少，必须手动设置。不然RecyclerView滑动滑动到顶部时，没有效果。
//                //fixme 主要用于判断第三部分是否滑动到顶部。
//                helper.setCurrentScrollableContainer(recyclerView)
//            }

//fixme 顶部悬停Scrollable【悬浮置顶】
public class KScrollableLayout extends LinearLayout {

    private Context context;
    private Scroller mScroller;
    private float mDownX;
    private float mDownY;
    private float mLastX;
    private float mLastY;
    private VelocityTracker mVelocityTracker;
    private int mTouchSlop;
    private int mMinimumVelocity;
    private int mMaximumVelocity;

    private boolean mIsHorizontalScrolling;
    private float x_down;
    private float y_down;
    private float x_move;
    private float y_move;
    private float moveDistanceX;
    private float moveDistanceY;

    public View mHeadView;//头部View;即第一部分，会滑动的部分。
    private ViewPager childViewPager;

    private DIRECTION mDirection;
    private int mHeadHeight;
    private int mScrollY;
    private int sysVersion;
    private boolean flag1, flag2;
    private int mLastScrollerY;
    private boolean mDisallowIntercept;

    private int minY = 0;
    private int maxY = 0;//最大滑动值，根据头部，回自动计算出来。

    public int maxScrollY = -1;//最大滑动值，自己手动赋值，和maxY同样有效。大于等于0时才有效。小于0无效。

    ///fixme 设置最大滑动值
    ///fixme isContiansHead true 只显示头部内容；false 正常滑动。
    public void setMaxScrollYWithHead(Boolean isContiansHead) {
        if (isContiansHead){
            setMaxScrollYWithHead();
        }else {
            maxScrollY = -1;
        }
    }

    ///fixme 自动设置最大滑动值，只显示出头部HeaderView;(一般都是第三部分隐藏或没有数据的时候，调用);不需要的时候，记得手动赋值 maxScrollY = -1
    ///最好在布局高度初始化完成后再手动调用。
    public void setMaxScrollYWithHead() {
        if (getChildCount() > 0) {
            mHeadView = getChildAt(0);
        }
        if (mHeadView != null) {
            mHeadView.requestLayout();
        }
        requestLayout();
        setMaxScrollYWithHead2();
        OnLayoutCallback onLayoutCallback2 = this.onLayoutCallback;
        setOnLayoutCallback(new OnLayoutCallback() {
            @Override
            public void result() {
                setMaxScrollYWithHead2();
                setOnLayoutCallback(onLayoutCallback2);
            }
        });
    }

    private void setMaxScrollYWithHead2() {
        if (mHeadView != null) {
            maxY = mHeadView.getMeasuredHeight();//最好重新计算一次。
            mHeadHeight = mHeadView.getMeasuredHeight();
            maxScrollY = maxY - (getHeight() - maxY);
            if (maxScrollY <= 0) {
                maxScrollY = 0;
            }
            if (mCurY > maxScrollY) {
                mCurY = maxScrollY;
                scrollBy(0, maxScrollY);
            }
        }
    }

    //布局回调
    public interface OnLayoutCallback {
        void result();
    }

    OnLayoutCallback onLayoutCallback;

    public void setOnLayoutCallback(OnLayoutCallback onLayoutCallback) {
        this.onLayoutCallback = onLayoutCallback;
    }

    private int mCurY;
    private boolean isClickHead;
    private int mScrollMinY = 10;

    enum DIRECTION {
        UP,
        DOWN
    }

    public interface OnScrollListener {
        void onScroll(int currentY, int maxY);
    }

    private OnScrollListener onScrollListener;

    public void setOnScrollListener(OnScrollListener onScrollListener) {
        this.onScrollListener = onScrollListener;
    }

    private KScrollableHelper mHelper;

    public KScrollableHelper getHelper() {
        return mHelper;
    }

    public KScrollableLayout(Context context) {
        this(context, null);
    }

    public KScrollableLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public KScrollableLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        mHelper = new KScrollableHelper();
        mScroller = new Scroller(context);
        final ViewConfiguration configuration = ViewConfiguration.get(context);
        mTouchSlop = configuration.getScaledTouchSlop();
        mMinimumVelocity = configuration.getScaledMinimumFlingVelocity();
        mMaximumVelocity = configuration.getScaledMaximumFlingVelocity();
        sysVersion = Build.VERSION.SDK_INT;
        setOrientation(VERTICAL);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (getChildCount()>0){
            mHeadView = getChildAt(0);
        }else {
            mHeadView=null;
        }
        if (mHeadView != null) {
            measureChildWithMargins(mHeadView, widthMeasureSpec, 0, MeasureSpec.UNSPECIFIED, 0);
            maxY = mHeadView.getMeasuredHeight();
            mHeadHeight = mHeadView.getMeasuredHeight();
        }
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(heightMeasureSpec) + maxY, MeasureSpec.EXACTLY));
        if (onLayoutCallback != null) {
            onLayoutCallback.result();
        }
    }

    @Override
    protected void onFinishInflate() {
        if (mHeadView != null && !mHeadView.isClickable()) {
            mHeadView.setClickable(true);
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt != null && childAt instanceof ViewPager) {
                childViewPager = (ViewPager) childAt;
            }
        }
        super.onFinishInflate();
    }

    MotionEvent ev;//记录滑动事件
    View secondView = null;//第二部分View

    //是否触摸第二部View或第一部分View
    //fixme 这个方法是我自己加的。修复触摸第二部时无效的Bug
    public boolean isSecondView(MotionEvent ev) {
        if (ev == null) {
            return true;
        }
        float y = ev.getY() + getScrollY();
        if (secondView != null) {
            //Log.e("test","第二y：\t"+secondView.getY()+"\t高度:\t"+secondView.getHeight()+"\ty:\t"+y);
            if (y < secondView.getY() + secondView.getHeight()) {
                return true;
            } else {
                return false;
            }
        } else if (getChildCount() > 2) {
            secondView = getChildAt(1);
        }
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mHeadView == null) {
            return super.dispatchTouchEvent(ev);
        }
        this.ev = ev;
        float currentX = ev.getX();
        float currentY = ev.getY();
        float deltaY;
        int shiftX;
        int shiftY;
        shiftX = (int) Math.abs(currentX - mDownX);
        shiftY = (int) Math.abs(currentY - mDownY);
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mDisallowIntercept = false;
                mIsHorizontalScrolling = false;
                x_down = ev.getRawX();
                y_down = ev.getRawY();
                flag1 = true;
                flag2 = true;
                mDownX = currentX;
                mDownY = currentY;
                mLastX = currentX;
                mLastY = currentY;
                mScrollY = getScrollY();
                checkIsClickHead((int) currentY, mHeadHeight, getScrollY());
                initOrResetVelocityTracker();
                mVelocityTracker.addMovement(ev);
                mScroller.forceFinished(true);
                break;
            case MotionEvent.ACTION_MOVE:
                if (mDisallowIntercept) {
                    break;
                }
                initVelocityTrackerIfNotExists();
                mVelocityTracker.addMovement(ev);
                deltaY = mLastY - currentY;
                if (flag1) {
                    if (shiftX > mTouchSlop && shiftX > shiftY) {
                        flag1 = false;
                        flag2 = false;
                    } else if (shiftY > mTouchSlop && shiftY > shiftX) {
                        flag1 = false;
                        flag2 = true;
                    }
                }

                if (flag2 && shiftY > mTouchSlop && shiftY > shiftX && (!isSticked() || mHelper.isTop() || isSecondView(ev))) {
                    if (childViewPager != null) {
                        childViewPager.requestDisallowInterceptTouchEvent(true);
                    }
                    scrollBy(0, (int) (deltaY + 0.5));
                }
                mLastX = currentX;
                mLastY = currentY;
                x_move = ev.getRawX();
                y_move = ev.getRawY();
                moveDistanceX = (int) (x_move - x_down);
                moveDistanceY = (int) (y_move - y_down);
                if (Math.abs(moveDistanceY) > mScrollMinY && (Math.abs(moveDistanceY) * 0.1 > Math.abs(moveDistanceX))) {
                    mIsHorizontalScrolling = false;
                } else {
                    mIsHorizontalScrolling = true;
                }
                break;
            case MotionEvent.ACTION_UP:
                if (flag2 && shiftY > shiftX && shiftY > mTouchSlop) {
                    mVelocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
                    float yVelocity = -mVelocityTracker.getYVelocity();
                    if (Math.abs(yVelocity) > mMinimumVelocity) {
                        mDirection = yVelocity > 0 ? DIRECTION.UP : DIRECTION.DOWN;
                        if (mDirection == DIRECTION.UP && isSticked()) {
                        } else {
                            mScroller.fling(0, getScrollY(), 0, (int) yVelocity, 0, 0, -Integer.MAX_VALUE, Integer.MAX_VALUE);
                            mScroller.computeScrollOffset();
                            mLastScrollerY = getScrollY();
                            invalidate();
                        }
                    }
                    if (isClickHead || !isSticked()) {
                        int action = ev.getAction();
                        ev.setAction(MotionEvent.ACTION_CANCEL);
                        boolean dd = super.dispatchTouchEvent(ev);
                        ev.setAction(action);
                        return dd;
                    }
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                if (flag2 && isClickHead && (shiftX > mTouchSlop || shiftY > mTouchSlop)) {
                    int action = ev.getAction();
                    ev.setAction(MotionEvent.ACTION_CANCEL);
                    boolean dd = super.dispatchTouchEvent(ev);
                    ev.setAction(action);
                    return dd;
                }
                break;
            default:
                break;
        }
        super.dispatchTouchEvent(ev);
        return true;
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private int getScrollerVelocity(int distance, int duration) {
        if (mScroller == null) {
            return 0;
        } else if (sysVersion >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            return (int) mScroller.getCurrVelocity();
        } else {
            return distance / duration;
        }
    }

    @Override
    public void computeScroll() {
        if (mScroller.computeScrollOffset()) {
            final int currY = mScroller.getCurrY();
            if (mDirection == DIRECTION.UP) {
                if (isSticked()) {
                    int distance = mScroller.getFinalY() - currY;
                    int duration = calcDuration(mScroller.getDuration(), mScroller.timePassed());
                    mHelper.smoothScrollBy(getScrollerVelocity(distance, duration), distance, duration);
                    mScroller.forceFinished(true);
                    return;
                } else {
                    scrollTo(0, currY);
                }
            } else {

                if (mHelper.isTop() || isSecondView(ev)) {
                    int deltaY = (currY - mLastScrollerY);
                    int toY = getScrollY() + deltaY;
                    scrollTo(0, toY);
                    if (mCurY <= minY) {
                        mScroller.forceFinished(true);
                        return;
                    }
                }
                invalidate();
            }
            mLastScrollerY = currY;
        }
    }

    @Override
    public void scrollBy(int x, int y) {
        int scrollY = getScrollY();
        int toY = scrollY + y;
        if (toY >= maxY) {
            toY = maxY;
        } else if (toY <= minY) {
            toY = minY;
        }
        //maxScrollY 大于等于0时有效，小于0无效。
        if (maxScrollY >= 0 && toY >= maxScrollY) {
            toY = maxScrollY;
        }
        y = toY - scrollY;
        super.scrollBy(x, y);
    }

    @Override
    public void scrollTo(int x, int y) {
        if (y >= maxY) {
            y = maxY;
        } else if (y <= minY) {
            y = minY;
        }
        //maxScrollY 大于等于0时有效，小于0无效。
        if (maxScrollY >= 0 && y >= maxScrollY) {
            y = maxScrollY;
        }
        mCurY = y;
        if (onScrollListener != null) {
            onScrollListener.onScroll(y, maxY);
        }
        super.scrollTo(x, y);
    }

    private void initOrResetVelocityTracker() {
        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityTracker.obtain();
        } else {
            mVelocityTracker.clear();
        }
    }

    private void initVelocityTrackerIfNotExists() {
        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityTracker.obtain();
        }
    }

    private void recycleVelocityTracker() {
        if (mVelocityTracker != null) {
            mVelocityTracker.recycle();
            mVelocityTracker = null;
        }
    }

    private void checkIsClickHead(int downY, int headHeight, int scrollY) {
        isClickHead = downY + scrollY <= headHeight;
    }

    private int calcDuration(int duration, int timepass) {
        return duration - timepass;
    }

    public void requestScrollableLayoutDisallowInterceptTouchEvent(boolean disallowIntercept) {
        super.requestDisallowInterceptTouchEvent(disallowIntercept);
        mDisallowIntercept = disallowIntercept;
    }

    public boolean isSticked() {
        return mCurY == maxY;
    }

    public int getMaxY() {
        return maxY;
    }

    public void setScrollMinY(int y) {
        mScrollMinY = y;
    }

    public boolean isCanPullToRefresh() {
        if (getScrollY() <= 0 && (mHelper.isTop() || isSecondView(ev)) && !mIsHorizontalScrolling) {
            return true;
        }
        return false;
    }
}
