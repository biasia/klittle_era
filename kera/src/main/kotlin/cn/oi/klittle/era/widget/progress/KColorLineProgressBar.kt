package cn.oi.klittle.era.widget.progress

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.widget.compat.KView

//                调用案例
//                KColorLineProgressBar {
//                    bgColor=Color.TRANSPARENT//背景色
//                    progressColor=Color.parseColor("#00A4FF")//进度条颜色
//                    progress=50//进度值（0~100）
//                    isShowBgLine=false//是否显示背景横线条。
//                }.lparams {
//                    width= matchParent
//                    height=kpx.x(3)
//                }

/**
 * 防微信网页头部的横线进度条。
 */
open class KColorLineProgressBar : KView {

    constructor(viewGroup: ViewGroup) : super(viewGroup.context) {
        viewGroup.addView(this)//直接添加进去,省去addView(view)
    }

    constructor(viewGroup: ViewGroup, HARDWARE: Boolean) : super(viewGroup.context) {
        if (HARDWARE) {
            setLayerType(View.LAYER_TYPE_HARDWARE, null)
        } else {
            setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }
        viewGroup.addView(this)//直接添加进去,省去addView(view)
    }

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {}

    private val paint: Paint by lazy { Paint() }

    //进度值（0~100）
    var progress: Int = 0
        set(value) {
            field = value
            invalidate()
        }

    //进度条颜色
    var progressColor: Int = Color.parseColor("#00A4FF")

    //背景色
    var bgColor: Int = Color.TRANSPARENT

    //背景底部横线条颜色
    var bgLineColor = Color.parseColor("#dddddd")
    var bgLineHeight = kpx.x(1)

    //fixme 是否显示背景横线条。
    var isShowBgLine = false
        set(value) {
            field = value
            invalidate()
        }

    open fun setProgressColor(value: Int): KColorLineProgressBar {
        this.progressColor = value
        invalidate()
        return this
    }

    init {
        paint.isAntiAlias = true
        paint.isDither = true
        paint.style = Paint.Style.FILL
    }


    override fun draw(canvas: Canvas?) {
        super.draw(canvas)
        if (width <= 0 || height <= 0) {
            return
        }
        canvas?.let {
            if (isShowBgLine) {
                //FIXME 显示背景底部横线条
                paint.color = bgLineColor
                var left = 0f
                var right = width.toFloat()
                var bottom = height.toFloat()
                var top = bottom - bgLineHeight
                if (bgLineColor != Color.TRANSPARENT) {
                    var rectF = RectF(left, top, right, bottom)
                    it.drawRect(rectF, paint)
                }
                return
            }
            //FIXME 画背景
            paint.color = bgColor
            var left = 0f
            var top = 0f
            var right = width.toFloat()
            var bottom = height.toFloat()
            if (bgColor != Color.TRANSPARENT) {
                var rectF = RectF(left, top, right, bottom)
                it.drawRect(rectF, paint)
            }
            //FIXME 画进度
            if (progress < 0) {
                progress = 0
            } else if (progress > 100) {
                progress = 100
            }
            if (progress > 0 && progress <= 100 && progressColor != Color.TRANSPARENT) {
                paint.color = progressColor
                right = width.toFloat() * (progress.toFloat() / 100f)
                //KLoggerUtils.e("right:\t" + right + "\tprogress:\t" + progress)
                var rectF = RectF(left, top, right, bottom)
                it.drawRect(rectF, paint)
            }
        }
    }

}