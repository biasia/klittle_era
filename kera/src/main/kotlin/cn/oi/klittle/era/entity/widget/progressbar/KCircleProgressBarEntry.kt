package cn.oi.klittle.era.entity.widget.progressbar

import android.graphics.Color

/**
 * fixme KCircleProgressBar 圆形进度条
 * @param isDraw 是否绘制
 * @param isDrawBgRing 是否绘制环形背景
 * @param isDrawProgressRing 是否绘制环形进度
 * @param bg_ring_color 环形背景色
 * @param bg_ring_width 环形背景宽度。
 * @param bgHorizontalColors 背景水平颜色渐变
 * @param bgVerticalColors 背景垂直颜色渐变(优先级比水平高)
 * @param bgSweepGradientColors 背景环形扫描渐变。（优先级最高）
 * @param bgLeftMargin 环形背景左外补丁
 * @param bgTopMargin 上外补丁
 * @param bgRightMargin 右外补丁
 * @param bgBottomMargin 下外补丁
 * @param startAngle 扇形起始方向。0右边，90下边，180左边，270上边。
 * @param progress_ring_color 环形进度条颜色
 * @param progress_ring_width 环形进度条宽度
 * @param progress 环形进度（0~100f）
 * @param progressHorizontalColors 环形进度条水平颜色渐变
 * @param progressVerticalColors 环形进度条垂直颜色渐变(优先级比水平高)
 * @param progressSweepGradientColors 环形扫描渐变。（优先级最高）
 * @param progressLeftMargin 环形进度条左外补丁
 * @param progressTopMargin 上外补丁
 * @param progressRightMargin 右外补丁
 * @param progressBottomMargin 下外补丁
 */
data class KCircleProgressBarEntry(var isDraw: Boolean = true, var isDrawBgRing: Boolean = true, var isDrawProgressRing: Boolean = true,
                                   var bg_ring_color: Int = Color.TRANSPARENT, var bg_ring_width: Float = -1F,
                                   var bgHorizontalColors: IntArray? = null, var bgVerticalColors: IntArray? = null,
                                   var bgSweepGradientColors: IntArray? = null,
                                   var bgLeftMargin: Float = 0F, var bgTopMargin: Float = 0F, var bgRightMargin: Float = 0F, var bgBottomMargin: Float = 0F,
                                   var startAngle: Float = 270F,
                                   var progress_ring_color: Int = Color.TRANSPARENT, var progress_ring_width: Float = -1F, var progress: Float = 0F,
                                   var progressHorizontalColors: IntArray? = null, var progressVerticalColors: IntArray? = null,
                                   var progressSweepGradientColors: IntArray? = null,
                                   var progressLeftMargin: Float = 0F, var progressTopMargin: Float = 0F, var progressRightMargin: Float = 0F, var progressBottomMargin: Float = 0F) {

    //fixme 环形背景水平渐变数
    open fun bgHorizontalColors(vararg color: Int) {
        bgHorizontalColors = color
    }

    open fun bgHorizontalColors(vararg color: String) {
        bgHorizontalColors = IntArray(color.size)
        bgHorizontalColors?.apply {
            if (color.size > 1) {
                for (i in 0..color.size - 1) {
                    this[i] = Color.parseColor(color[i])
                }
            } else {
                this[0] = Color.parseColor(color[0])
            }
        }
    }

    //fixme 环形背景垂直渐变色
    open fun bgVerticalColors(vararg color: Int) {
        bgVerticalColors = color
    }

    open fun bgVerticalColors(vararg color: String) {
        bgVerticalColors = IntArray(color.size)
        bgVerticalColors?.apply {
            if (color.size > 1) {
                for (i in 0..color.size - 1) {
                    this[i] = Color.parseColor(color[i])
                }
            } else {
                this[0] = Color.parseColor(color[0])
            }
        }
    }


    //fixme 背景环形进度条扫描渐变色
    open fun bgSweepGradientColors(vararg color: Int) {
        bgSweepGradientColors = color
    }

    open fun bgSweepGradientColors(vararg color: String) {
        bgSweepGradientColors = IntArray(color.size)
        bgSweepGradientColors?.apply {
            if (color.size > 1) {
                for (i in 0..color.size - 1) {
                    this[i] = Color.parseColor(color[i])
                }
            } else {
                this[0] = Color.parseColor(color[0])
            }
        }
    }

    //fixme 环形进度条水平渐变数
    open fun progressHorizontalColors(vararg color: Int) {
        progressHorizontalColors = color
    }

    open fun progressHorizontalColors(vararg color: String) {
        progressHorizontalColors = IntArray(color.size)
        progressHorizontalColors?.apply {
            if (color.size > 1) {
                for (i in 0..color.size - 1) {
                    this[i] = Color.parseColor(color[i])
                }
            } else {
                this[0] = Color.parseColor(color[0])
            }
        }
    }

    //fixme 环形进度条垂直渐变色
    open fun progressVerticalColors(vararg color: Int) {
        progressVerticalColors = color
    }

    open fun progressVerticalColors(vararg color: String) {
        progressVerticalColors = IntArray(color.size)
        progressVerticalColors?.apply {
            if (color.size > 1) {
                for (i in 0..color.size - 1) {
                    this[i] = Color.parseColor(color[i])
                }
            } else {
                this[0] = Color.parseColor(color[0])
            }
        }
    }

    //fixme 环形进度条扫描渐变色
    open fun progressSweepGradientColors(vararg color: Int) {
        progressSweepGradientColors = color
    }

    open fun progressSweepGradientColors(vararg color: String) {
        progressSweepGradientColors = IntArray(color.size)
        progressSweepGradientColors?.apply {
            if (color.size > 1) {
                for (i in 0..color.size - 1) {
                    this[i] = Color.parseColor(color[i])
                }
            } else {
                this[0] = Color.parseColor(color[0])
            }
        }
    }

}