package cn.oi.klittle.era.activity.filemanager.adapter

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseActivity
import cn.oi.klittle.era.base.KBaseUi
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.utils.*
import cn.oi.klittle.era.widget.compat.KTextView
import cn.oi.klittle.era.widget.recycler.adapter.KAdapter
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.io.File

/**
 * fixme 这个Adaper适配器是为了方便测试使用的。
 * Created by 彭治铭 on 2019/3/18.
 */
open class KFileAdapter(var datas: ArrayList<File>? = null) : KAdapter<KFileAdapter.Companion.MyViewHolder>() {

    companion object {
        open class MyViewHolder(itemView: View, var viewType: Int) : RecyclerView.ViewHolder(itemView) {
            var left_txt: KTextView? = null//左边的文本
            var item_size: KTextView? = null
            var item_go: KTextView? = null
            var item_layout: View? = null
            var item_del: KTextView? = null

            init {
                //正常
                left_txt = itemView?.findViewById(kpx.id("item_txt"))
                item_size = itemView?.findViewById(kpx.id("item_size"))
                item_go = itemView?.findViewById(kpx.id("item_go"))
                item_layout = itemView?.findViewById(kpx.id("item_layout"))
                item_del = itemView?.findViewById(kpx.id("item_del"))
            }
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var itemView: View? = null
        //正常
        itemView = parent.context.UI {
            KBaseUi.apply {
                verticalLayout {
                    kSwipeMenuLayout {
                        //主布局
                        linearLayout {
                            topPadding = kpx.x(16)
                            bottomPadding = topPadding
                            leftPadding = kpx.x(30)
                            rightPadding = kpx.x(16)
                            gravity = Gravity.CENTER_VERTICAL
                            KSelectorUtils.selectorRippleDrawable(this, Color.WHITE, Color.parseColor("#EFF3F6"))
                            id = kpx.id("item_layout")
                            //左边的文本
                            KTextView(this).apply {
                                gravity = Gravity.CENTER_VERTICAL or Gravity.LEFT
                                id = kpx.id("item_txt")
                                textSize = kpx.textSizeX(30)
                                textColor = Color.BLACK
                                isClickable = false
                                padding = 0
                                isClickable = false
                                setMore(1)
                            }.lparams {
                                width = 0
                                weight = 2f
                                height = wrapContent
                            }
                            linearLayout {
                                gravity = Gravity.CENTER_VERTICAL
                                KTextView(this).apply {
                                    gravity = Gravity.CENTER_VERTICAL
                                    id = kpx.id("item_size")
                                    textSize = kpx.textSizeX(30)
                                    textColor = Color.BLACK
                                    isClickable = false
                                    padding = 0
                                    isClickable = false
                                }.lparams {
                                    width = 0
                                    weight = 2f
                                    height = wrapContent
                                }
                                KTextView(this).apply {
                                    gravity = Gravity.CENTER_VERTICAL or Gravity.RIGHT
                                    id = kpx.id("item_go")
                                    textSize = kpx.textSizeX(30)
                                    textColor = Color.BLACK
                                    isClickable = false
                                    padding = 0
                                    autoBg {
                                        width = kpx.x(50)
                                        height = width
                                        autoBg(R.mipmap.kera_back_reverse)
                                        isAutoCenterHorizontal = false
                                        isAutoCenterVertical = true
                                        isAutoRight = true
                                    }
                                    minHeight = kpx.x(60)
                                }.lparams {
                                    width = 0
                                    weight = 1f
                                    height = wrapContent
                                }
                            }.lparams {
                                width = 0
                                weight = 1f
                                height = wrapContent
                            }
                        }.lparams {
                            width = kpx.x(750)
                            height = wrapContent
                        }
                        //隐藏的侧滑菜单布局
                        ktextView {
                            id = kpx.id("item_del")
                            gravity = Gravity.CENTER
                            backgroundColor(Color.RED)
                            text = getString(R.string.kdel)
                            textColor = Color.WHITE
                            textSize = kpx.textSizeX(36)
                        }.lparams {
                            width = kpx.x(150)//fixme 一定要设置宽度；这个是侧滑菜单的内容
                            height = matchParent
                        }
                        isIos = true//设置是否开启IOS阻塞式交互（true）;即有多个侧滑菜单时，只允许操作一个（true会产生阻塞，false不会阻塞）。
                        isLeftSwipe = true//true 向左侧滑动，false向右侧滑动
                        isSwipeEnable = true//是否开启滑动功能
                    }
                    //分界线
                    view {
                        backgroundColor = Color.LTGRAY
                    }.lparams {
                        width = matchParent
                        height = kpx.x(1)
                    }
                }
            }

        }.view
        return MyViewHolder(itemView, viewType)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder?.left_txt?.setText(null)
        var data = datas?.get(position)
        data?.let {
            //名称
            holder?.left_txt?.setText(it.name?.trim())
            //大小
            holder?.item_size?.setText(KStringUtils.getDataSize(it.length()))
            //打开
            holder?.item_layout?.onClick {
                KIntentUtils.goOpenFile(file = data, isSharedable = true)
            }
            //删除
            holder?.item_del?.let {
                var ctx = it.context
                if (ctx is KBaseActivity) {
                    it.onClick {
                        ctx?._dialogAlert(getString(R.string.kgodel))?.let {
                            it.positive {
                                KFileUtils.getInstance().delFile(data)//fixme 文件删除，亲测有效。
                                //KLoggerUtils.e("是否删除成功：\t" + b)
                                datas?.remove(data)
                                notifyDataSetChanged()
                            }
                            it.show()
                        }
                    }
                }
            }
        }
    }


    override fun getItemViewType(position: Int): Int {
        //return super.getItemViewType(position)
        return position
    }

    override fun getItemCount(): Int {
        return datas?.size ?: 0
    }
}