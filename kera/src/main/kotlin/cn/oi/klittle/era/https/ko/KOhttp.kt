package cn.oi.klittle.era.https.ko

import cn.oi.klittle.era.https.down.KFileDownEntity

//fixme 网络请求调用案例；


//                        var url = ""
//                        KOhttp.url(url).apply {
//                            //fixme .net asp 无法交互时；建议设置为空（为空时，http就不会设置contentType,就会使用默认的。）；
//                            //fixme 如果是java;就不要设置为空。默认就行（不要设置）。
//                            contentType(null)
//                            //isURLEncoder=false;//fixme 是否进行url转码，防止服务器不识别特殊字符。默认是true;如果发送签名，验证错误，可能和这个有关系。把这个设置成false
//                            var fields = java.util.HashMap<String, String>()
//                            var subData = SubData()
//                            subData.commondata = Https.getCommonData("")
//                            fields.put("data", subData.toJson())
//                            isShowParams(true)//是否打印参数
//                            isShowLoad(true)//显示网络进度条
//                            isCacle(false)//不缓存
//                            isSharingDialog(true)//fixme 共享弹窗
//                            isUrlUniqueParams(true)//fixme 唯一标志带上参数。
//                            onSuccess {
//                                //成功回调，返回服务器原始数据
//                                KLoggerUtils.e("提交成功：\t" + it)
//                            }
//                            onFailure {
//                                //网络访问失败回调
//                                KLoggerUtils.e("提交失败：\t" + it)
//                            }
//                            addParam(fields)
//                            isShowLoad(false)
//                            isLocked(true)//网络进度条，是否屏蔽返回键。默认屏蔽。
//                            //开始进度回调
//                            onStart {
//
//                            }
//                            isLocked = true//是否屏蔽网络进度条返回键
//                            onTimeOutTime = 35000L//网络进度条连接超时时间设置
//                            onTimeOut {
//                                //fixme 网络连接超时。可以设置超时时间。
//                            }
//                            //结束进度回调（最后执行，并且一定会回调）
//                            onFinish {
//
//                            }
//                            //fixme 发送post请求（通过泛型，直接返回对应数据）
//                            post<AppHttpResult<String>>() {
//
//                            }
//                        }

object KOhttp {
    //普通网络请求
    fun url(url: String): KHttps {
        return KHttps().url(url)
    }

    //轮询网络请求
    fun polling(url: String): KPolling {
        return KPolling(url = url)
    }

    //FIXME 文件下载。在KFileDownEntity有调用案例。
    fun downUrl(url: String?): KFileDownEntity {
        var entity = KFileDownEntity()
        entity.url = url
        return entity
    }

}