package cn.oi.klittle.era.widget.photo

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.view.ViewGroup
import cn.oi.klittle.era.exception.KCatchException
import cn.oi.klittle.era.utils.KLoggerUtils
import cn.oi.klittle.era.widget.photoview.PhotoView
import org.jetbrains.anko.backgroundDrawable

/**
 * 照片图片控件；这个PhotoView在图片选择器的引用中。在图片预览里面KPreviewActivity有使用。
 */
class KPhotoView : PhotoView {
    constructor(viewGroup: ViewGroup) : super(viewGroup.context) {
        viewGroup.addView(this)//直接添加进去,省去addView(view)
    }

    constructor(context: Context) : super(context) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {}

    override fun draw(canvas: Canvas?) {
        try {
            drawable?.let {
                if (it is BitmapDrawable) {
                    it.bitmap?.let {
                        if (it.isRecycled) {
                            mBitmap=null
//                            context?.let {
//                                if (it is Activity){
//                                    KLoggerUtils.e("位图已释放:\t"+it+"\t"+it.isFinishing)
//                                }
//                            }
                            //KLoggerUtils.e("KPhotoView位图已经释放")
                            //fixme 亲测能够解决图片异常释放问题。
                            return//图片已经释放了，就不要继续执行了。会报错的。
                        }
                    }
                }
            }
            super.draw(canvas)
        } catch (e: Exception) {
            e.printStackTrace()
            KLoggerUtils.e("KPhotoView位图加载异常：\t" + KCatchException.getExceptionMsg(e), true)
        }
    }

    var mBitmap: Bitmap? = null
    override fun setImageBitmap(bm: Bitmap?) {
        super.setImageBitmap(bm)
        mBitmap = bm
        postInvalidate()
    }

    //销毁
    fun onDestroy() {
        backgroundDrawable = null
        if (Build.VERSION.SDK_INT >= 16) {
            background = null
        }
        mBitmap?.let {
            if (!it.isRecycled) {
                it.recycle()
            }
        }
        mBitmap = null
    }

}