package cn.oi.klittle.era.dialog.input

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.TextView
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseDialog
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.widget.compat.KEditText
import org.jetbrains.anko.*
import java.lang.Exception

//                                fixme 文本输入框弹窗，调用案例。
//                                var inputDialog:KTextInputDialog?=null
//                                if (inputDialog==null) {
//                                    inputDialog = KTextInputDialog(ctx)
//                                }
//                                inputDialog?.textCallback {
//                                    KLoggerUtils.e("输入框文本回调：\t"+it)
//                                }
//                                inputDialog?.show()

/**
 * 输入框弹框
 */
open class KTextInputDialog(
        ctx: Context,
        isStatus: Boolean = true,
        isTransparent: Boolean = false
) : KBaseDialog(ctx, isStatus = isStatus, isTransparent = isTransparent) {

    var color_select = Color.parseColor("#42A8E1")//选中颜色
    var color_nor = Color.parseColor("#C8C5C9")//一般颜色
    override fun onCreateView(context: Context): View? {
        return context.UI {
            ui {
                scrollView {
                    isFillViewport = true
                    setVerticalScrollBarEnabled(false)
                    verticalLayout {
                        gravity = Gravity.CENTER
                        kverticalLayout {
                            isClickable = true
                            shadow_radius = kpx.x(12f)
//                            shadow {
//                                bg_color = Color.WHITE
//                                all_radius(kpx.x(32))
//                                shadow_color = Color.parseColor("#333333")//效果比Color.BLACK好。
//                            }
                            radius {
                                bg_color = Color.WHITE
                                all_radius(kpx.x(40))
                            }
                            //标题
                            textView {
                                id = kpx.id("crown_txt_title")
                                textColor = Color.parseColor("#242424")
                                textSize = kpx.textSizeX(34)
                                text = getString(R.string.kpeleaseInput)
                            }.lparams {
                                topMargin = kpx.x(48)
                                leftMargin = kpx.x(24)
                                rightMargin = leftMargin
                                width = wrapContent
                                height = wrapContent
                            }
                            KEditText(this).apply {
                                id = kpx.id("crown_txt_edit")
                                textColor = Color.parseColor("#242424")
                                textSize = kpx.textSizeX(32)
                                //hint = getString(R.string.kinputNotBig)//不区分大小写
                                line {
                                    strokeColor = Color.LTGRAY
                                    strokeWidth = kpx.x(3f)
                                }
                                notContainsRegex("\u0020\n\r")//fixme 不包含空格和换行符。
                                //setCursorColor(Color.BLUE)
                                openDefaultAnimeLine()
                                requestFocus()
                                topPadding = kpx.x(16)
                                bottomPadding = topPadding
                            }.lparams {
                                width = matchParent
                                height = wrapContent
                                leftMargin = kpx.x(24)
                                rightMargin = leftMargin
                                topMargin = kpx.x(32)
                                bottomMargin = topMargin
                            }

                            //按钮
                            linearLayout {
                                gravity = Gravity.CENTER_VERTICAL or Gravity.RIGHT
                                //取消
                                textView {
                                    id = kpx.id("crown_txt_Negative")
                                    //textColor = color_nor//color_nor是子类的属性，视图是在父类构造函数中调用的。此时还无法使用color_nor。
                                    textSize = kpx.textSizeX(34)
                                    leftPadding = kpx.x(24)
                                    rightPadding = leftPadding
                                    topPadding = kpx.x(16)
                                    bottomPadding = kpx.x(32)
                                    text = getString(cn.oi.klittle.era.R.string.kcancel)//取消
                                }.lparams {
                                    width = wrapContent
                                    height = wrapContent
                                }
                                //确定
                                textView {
                                    id = kpx.id("crown_txt_Positive")
                                    textColor = Color.parseColor("#42A8E1")
                                    textSize = kpx.textSizeX(34)
                                    leftPadding = kpx.x(24)
                                    rightPadding = kpx.x(48)
                                    topPadding = kpx.x(16)
                                    bottomPadding = kpx.x(32)
                                    text = getString(cn.oi.klittle.era.R.string.kconfirm)//确定
                                }.lparams {
                                    width = wrapContent
                                    height = wrapContent
                                }
                            }.lparams {
                                width = matchParent
                                height = wrapContent
                            }
                        }.lparams {
                            width = kpx.x(620)
                            height = wrapContent
                        }
                    }
                }
            }
        }.view
    }


    //标题栏文本
    var txt_title: String? = getString(R.string.kpeleaseInput)
    val title: TextView? by lazy { findViewById<TextView>(kpx.id("crown_txt_title")) }
    open fun title(title: String? = null): KTextInputDialog {
        txt_title = title
        return this
    }

    //信息文本
    var txt_mession: String? = ""
    val mession: KEditText? by lazy { findViewById<KEditText>(kpx.id("crown_txt_edit")) }
    open fun mession(mession: String? = null): KTextInputDialog {
        txt_mession = mession
        return this
    }

    val negative: TextView? by lazy { findViewById<TextView>(kpx.id("crown_txt_Negative")) }

    //左边，取消按钮
    open fun negative(
            negative: String? = getString(R.string.kcancel),
            callback: (() -> Unit)? = null
    ): KTextInputDialog {
        this.negative?.setText(negative)
        this.negative?.setOnClickListener {
            callback?.run {
                this()
            }
            dismiss()
        }
        return this
    }

    val positive: TextView? by lazy { findViewById<TextView>(kpx.id("crown_txt_Positive")) }

    //右边，确定按钮
    open fun positive(
            postive: String? = getString(R.string.kconfirm),
            callback: (() -> Unit)? = null
    ): KTextInputDialog {
        this.positive?.setText(postive)
        this.positive?.setOnClickListener {
            callback?.run {
                this()
            }
            dismiss()
        }
        return this
    }

    init {
        try {
            //取消
            negative?.setOnClickListener {
                dismiss()
            }
            //监听软键盘右下角按钮
            mession?.addDone {
                mession?.let {
                    var text = it.text.toString().trim()
                    txt_mession = text
                    textCallback?.let {
                        it(text)//回调
                    }
                }
                dismiss()
            }
            //确定
            positive?.setOnClickListener {
                mession?.let {
                    var text = it.text.toString().trim()//去取文本两端的空格。
                    txt_mession = text
                    textCallback?.let {
                        it(text)//回调
                    }
                }
                dismiss()
            }
            isDismiss(false)//默认不消失
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //文本回调
    var textCallback: ((text: String) -> Unit)? = null

    fun textCallback(textCallback: ((text: String) -> Unit)? = null) {
        this.textCallback = textCallback
    }

    override fun onShow() {
        super.onShow()
        title?.setText(txt_title)
        mession?.setText(txt_mession)
        mession?.let {
            it.requestFocus()//聚焦
            if (it.text.length > 0) {
                it.setSelection(it.text.toString().length)//光标设置在末尾
            }
        }
        negative?.textColor = color_nor
        mession?.requestFocus()//聚焦
    }

    override fun onDismiss() {
        super.onDismiss()
        mession?.clearFocus()//清除焦点。
    }

    override fun onDestroy() {
        super.onDestroy()
        textCallback = null
        mession?.onDestroy()
    }

}