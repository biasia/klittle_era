package cn.oi.klittle.era.dialog.camera

import android.app.Activity
import cn.oi.klittle.era.activity.photo.manager.KPictureSelector
import cn.oi.klittle.era.helper.KUiHelper
import java.io.File

object KFileType {
    val TTYE_IMAGE = 0;//图片
    val TTYE_VIDEO = 1;//视频
    val TTYE_AUDIO = 3;//音频

    ///打开文件
    fun openFile(activity: Activity, file: File, type: Int) {
        if (type == TTYE_IMAGE) {
            //图片
            KPictureSelector.openExternalPreview2(activity,file);
        } else if (type == TTYE_VIDEO) {
            //视频
            KUiHelper.goScreenVideoActivity(activity, file.path, isPortrait = true)
        } else if (type == TTYE_AUDIO) {
            KRecordUtils.playRecord(file);//播放音频文件
        }
    }

}