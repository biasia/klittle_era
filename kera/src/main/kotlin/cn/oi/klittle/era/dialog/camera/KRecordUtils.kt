package cn.oi.klittle.era.dialog.camera

import android.app.Activity
import android.media.MediaPlayer
import android.media.MediaRecorder
import cn.oi.klittle.era.utils.*
import java.io.File
import java.lang.Exception

///录音工具类
object KRecordUtils {

    //申请权限
    fun requestPermissionsRecording(activity: Activity, onRequestPermissionsResult2: ((isAllow: Boolean) -> Unit)? = null) {
        KPermissionUtils.requestPermissionsStorage {
            //存储权限
            if (it) {
                KPermissionUtils.requestPermissionsRecording {
                    //录音权限
                    if (it) {
                        onRequestPermissionsResult2?.let { it(true) }
                    } else {
                        onRequestPermissionsResult2?.let { it(false) }
                        KPermissionUtils.showFailure(activity, KPermissionUtils.perMissionTypeRecording)
                    }
                }
            } else {
                onRequestPermissionsResult2?.let { it(false) }
                KPermissionUtils.showFailure(activity, KPermissionUtils.perMissionTypeStorage)
            }
        }
    }

    var mediaRecorder: MediaRecorder? = null;
    var _isRecording = false;//判断是否正在录音。
    var isACTION_DOWN_RECORD = true;//是否按住录音。
    var isACTION_DOWN = false;//判断手指是否按下。（一般都是按住录音）
    var mediaPlayer: MediaPlayer? = null;
    var recordFile: File? = null;//录音文件

    //创建录音文件
    fun createRecordFile(): File? {
        try {
            var path = KPathManagerUtils.getAppRecordPath();
//            recordFile = KFileUtils.getInstance().createFile(path, System.currentTimeMillis().toString() + ".pcm")
            recordFile = KFileUtils.getInstance().createFile(path, System.currentTimeMillis().toString() + ".mp3")
        } catch (e: Exception) {
            KLoggerUtils.e("录音文件创建异常：\t" + e.message)
        }
        return recordFile;
    }

    //开始录音；callback录音成功时，回调。
    fun startRecording(activity: Activity, callback: (isSuccess: Boolean) -> Unit) {
//        KLoggerUtils.e("mediaRecorder：\t" + mediaRecorder + "\t_isRecording:\t" + _isRecording)
        if (mediaRecorder != null && _isRecording) {
            return;//正在录音。
        }
        requestPermissionsRecording(activity) {
//            KLoggerUtils.e("录音权限：\t" + it);//fixme 低版本系统，5.0会直接返回true。
            if (it) {
                try {
//                    if (mediaRecorder == null) {
//                        mediaRecorder = MediaRecorder();
//                    } else {
//                        mediaRecorder?.release();
//                    }
//                    KLoggerUtils.e("========================1");
                    stopRecording();//fixme 先停止之前的录音。
                    var isR = false;//是否继续录音。
                    if (isACTION_DOWN_RECORD) {
                        //fixme 手指按下录音。
                        if (isACTION_DOWN) {
                            isR = true;
                        } else {
                            //fixme 手指没有按下。所以不录音。
                            return@requestPermissionsRecording;
                        }
                    } else {
                        isR = true;
                    }
                    if (isR) {
                        mediaRecorder = MediaRecorder();
                        ///设置音频源
                        mediaRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC);

//                        ///fixme .PCM
//                        ///设置输出格式为
//                        mediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT)
//                        ///设置音频编码为默认;
//                        mediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT)

                        //fixme .amr
                        //mediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
                        //mediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)

                        //fixme .mp3
                        //mediaRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC);
                        mediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                        mediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

                        var file = createRecordFile();
                        if (file != null) {
                            ///设置输出文件路径
                            mediaRecorder?.setOutputFile(file.absolutePath)
                            //准备录音
                            //FIXME 这一步，android 5.0 低版本好像会自动弹出录音权限访问弹窗。会卡住，不会继续往下执行。
                            //fixme 亲测。旧版PDA好像只会问一次。后面就不问了。旧版本PDA可能还会奔溃。新版本的PDA好像可以正常使用。
                            mediaRecorder?.prepare();
                            var isR = false;//是否继续录音。
                            if (isACTION_DOWN_RECORD) {
                                //fixme 手指按下录音。
                                if (isACTION_DOWN) {
                                    isR = true;
                                } else {
                                    //fixme 手指没有按下。所以不录音。
                                    return@requestPermissionsRecording;
                                }
                            } else {
                                isR = true;
                            }
                            ///fixme 再判断一次；防止手指点击过快。
                            if (isR) {
                                //开始录音
                                mediaRecorder?.start();
                                _isRecording = true;//正在录音
                                callback?.let { it(true) }//fixme 录音回调成功。
                            } else {
                                stopRecording();
                            }
                        }
                    }
                } catch (e: Exception) {
                    KLoggerUtils.e("录音异常：\t" + e.message)
                    KPermissionUtils.showFailure(activity, KPermissionUtils.perMissionTypeRecording)
                    callback?.let { it(false) }
                }
            } else {
                callback?.let { it(false) }//fixme 没有录音权限。直接返回失败。
            }
        }
    }

    //停止录音,返回录音文件。
    fun stopRecording(): File? {
        try {
            if (mediaRecorder != null) {
                mediaRecorder?.stop();//start()和stop()间隔太短；stop()会报错异常。
                mediaRecorder?.release();
                mediaRecorder = null;
                _isRecording = false;//停止录音
            }
//            KLoggerUtils.e("录音停止");
        } catch (e: Exception) {
            KLoggerUtils.e("录音停止异常：\t" + e.message)//录音停止异常：	stop failed.
            try {
                if (mediaRecorder != null) {
                    mediaRecorder?.release();//fixme 再次释放资源，这一步不会报错。亲测。
                    mediaRecorder = null;
                    _isRecording = false;//停止录音
                }
//            KLoggerUtils.e("录音停止");
            } catch (e: Exception) {
                KLoggerUtils.e("录音停止异常2：\t" + e.message)
            }
        }
        return recordFile;
    }

    ///播放音频文件
    fun playRecord(file: File?) {
        try {
            stopPlayRecord();
            mediaPlayer = MediaPlayer();//fixme 直接重新实例化最好。mediaPlayer?.release();可能会导致播放音频异常。
            mediaPlayer?.setDataSource(file?.path)
            mediaPlayer?.prepare();
            mediaPlayer?.start();
        } catch (e: Exception) {
            KLoggerUtils.e("播放音频文件异常：\t" + e.message + "\t" + file?.path + "\t" + file?.length())
        }
    }

    ///停止播放音频文件
    fun stopPlayRecord() {
        if (mediaPlayer != null) {
            mediaPlayer?.stop();
            mediaPlayer?.release();
            mediaPlayer = null;
        }
    }

}