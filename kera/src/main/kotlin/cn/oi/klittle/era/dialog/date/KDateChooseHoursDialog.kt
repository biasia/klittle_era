package cn.oi.klittle.era.dialog.date

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.View
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseDialog
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.entity.feature.KDateChooseEntity
import cn.oi.klittle.era.utils.KCalendarUtils
import cn.oi.klittle.era.utils.KProportionUtils
import cn.oi.klittle.era.utils.KStringUtils
import cn.oi.klittle.era.widget.KRollerView

/**
 * 日期选择器，精确到：年，月，日，时，分。
 * Created by 彭治铭 on 2018/6/3.
 */
//使用说明
//                button {
//                    text = "日期"
//                    var dateChoose = KDateChooseEntity()//默认不设置，就的当前日期。
//                    dateChoose.yyyy = "2019"
//                    dateChoose.MM = "07"//有没有0都兼容。
//                    dateChoose.dd = "9"//09也可以，兼容0。
//                    //dateChoose.setTime("2018-01-22")
//                    var dateChooseHoursDialog: KDateChooseHoursDialog? = null
//                    onClick {
//                        if (dateChooseHoursDialog == null) {
//                            dateChooseHoursDialog = KDateChooseHoursDialog(ctx, dateChoose).setCallBack {
//                                dateChoose = it
//                                KLoggerUtils.e("日期回调：\t" + dateChoose.toString())
//                            }
//                        }
//                        dateChooseHoursDialog?.show()
//                    }
//                }
/**
 * @param ctx: Context
 * @param dateChoose 时间选择器
 * @param isStatus 是否有状态栏
 * @param isTransparent 是否透明
 */
open class KDateChooseHoursDialog(ctx: Context, var dateChoose: KDateChooseEntity = KDateChooseEntity(), isStatus: Boolean = true, isTransparent: Boolean = true) : KBaseDialog(ctx, R.layout.kera_dialog_date_choose_hours, isStatus, isTransparent) {
    val yyyy: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_yyyy) }
    val MM: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_MM) }
    val dd: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_dd) }
    val hours: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_hours) }
    val minutes: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_minutes) }

    init {
        if (ctx is Activity) {
            //KProportionUtils.getInstance().adapterWindow(ctx, dialog?.window)//适配
            findViewById<View>(R.id.crown_date_parent)?.let {
                KProportionUtils.getInstance().adapterAllView(ctx, it, false, false)
            }
        }
        dialog?.window?.setWindowAnimations(R.style.kera_window_bottom)//动画
        //取消
        findViewById<View>(R.id.crown_txt_cancel)?.setOnClickListener {
            dismiss()
        }
        //完成
        findViewById<View>(R.id.crown_txt_ok)?.setOnClickListener {
            dismiss()
        }
        //年
        var list_yyyy = ArrayList<String>()
        for (i in 2010..2040) {
            list_yyyy.add(i.toString())
        }
        yyyy?.setLineColor(Color.TRANSPARENT)?.setItems(list_yyyy)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#444444"))?.setSelectTextColor(Color.parseColor("#444444"))
        //月
        var list_MM = ArrayList<String>()
        for (i in 1..12) {
            list_MM.add(i.toString())
        }
        MM?.setLineColor(Color.TRANSPARENT)?.setItems(list_MM)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#444444"))?.setSelectTextColor(Color.parseColor("#444444"))
        MM?.setItemSelectListener(object : KRollerView.ItemSelectListener {
            override fun onItemSelect(item: String?, position: Int) {
                //月份监听
                updateDays()
            }
        })
        //日
        dd?.setLineColor(Color.TRANSPARENT)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#444444"))?.setSelectTextColor(Color.parseColor("#444444"))

        //时
        var list_hours = ArrayList<String>()
        for (i in 0..23) {
            list_hours.add(i.toString())
        }
        hours?.setLineColor(Color.TRANSPARENT)?.setItems(list_hours)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#444444"))?.setSelectTextColor(Color.parseColor("#444444"))


        //分
        var list_minutes = ArrayList<String>()
        for (i in 0..59) {
            list_minutes.add(i.toString())
        }
        minutes?.setLineColor(Color.TRANSPARENT)?.setItems(list_minutes)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#444444"))?.setSelectTextColor(Color.parseColor("#444444"))


        //fixme 设置数据滚轮循环效果
        yyyy?.isCyclic = true
        MM?.isCyclic = true
        dd?.isCyclic = true
        hours?.isCyclic = true
        minutes?.isCyclic = true

        isDismiss(true)
    }

    open fun updateDays() {
        //日，联动，更加月份而改变
        var list_dd = ArrayList<String>()
        var MM = MM?.currentItemValue
        MM = KStringUtils.addZero(MM)
        val mDay = KCalendarUtils.getMonthOfDay(yyyy?.currentItemValue + "-" + MM, "yyyy-MM")//天数
        //KLoggerUtils.e("月份：\t"+MM+"\t天数：\t"+mDay)
        for (i in 1..mDay) {
            list_dd.add(i.toString())
        }
        dd?.setItems(list_dd)
        dateChoose.dd?.toFloat()?.toInt()?.let {
            if (it > mDay) {
                dateChoose.dd = mDay?.toString()
            }
        }
    }

    override fun onShow() {
        super.onShow()
        try {
            updateDays()
            //选中
            yyyy?.setCurrentPostion(yyyy!!.getItemPostion(dateChoose.yyyy))
            MM?.setCurrentPostion(MM!!.getItemPostion(dateChoose.MM))
            dd?.setCurrentPostion(dd!!.getItemPostion(dateChoose.dd))
            hours?.setCurrentPostion(hours!!.getItemPostion(dateChoose.HH))
            minutes?.setCurrentPostion(minutes!!.getItemPostion(dateChoose.mm))
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onDismiss() {
        super.onDismiss()
    }

    //日期返回回调
    open fun setCallBack(callbak: (dateChoose: KDateChooseEntity) -> Unit): KDateChooseHoursDialog {
        //完成
        findViewById<View>(R.id.crown_txt_ok)?.setOnClickListener {
            dateChoose.yyyy = yyyy?.currentItemValue
            dateChoose.MM = MM?.currentItemValue
            dateChoose.dd = dd?.currentItemValue
            dateChoose.HH = hours?.currentItemValue
            dateChoose.mm = minutes?.currentItemValue
            callbak(dateChoose)
            dismiss()
        }
        return this
    }

}