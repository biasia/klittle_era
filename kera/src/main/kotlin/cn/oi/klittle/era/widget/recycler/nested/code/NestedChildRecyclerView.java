package cn.oi.klittle.era.widget.recycler.nested.code;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cn.oi.klittle.era.widget.recycler.KRecyclerView;

public class NestedChildRecyclerView extends KRecyclerView {
    private HanldeEvent mHanldeEvent;
    private float mCurPosY;
    private float mPosY;
    private int mTouchStop = 10;

    public void init(Context context) {
        final ViewConfiguration vc = ViewConfiguration.get(context);
        //根据自己需求可调整大小
        mTouchStop = Util.px2dp(context, vc.getScaledTouchSlop());
        setLayoutManager(new LinearLayoutManager(getContext()));
        //用于监听RecyclerView能否上下滑动
        initListener();
    }

    public NestedChildRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public NestedChildRecyclerView(Context context) {
        super(context);
        init(context);
    }

    public NestedChildRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public NestedChildRecyclerView(ViewGroup viewGroup) {
        super(viewGroup.getContext());
        init(viewGroup.getContext());
        viewGroup.addView(this);//直接添加进去,省去addView(view)
    }


    @Override
    public void onScrollStateChanged(int state) {
        super.onScrollStateChanged(state);
        //当子RvSon停止滚动时，将RvParent的mScrollState也置成SCROLL_STATE_IDLE状态，否则。。。你可以取消试试
        if (state == SCROLL_STATE_IDLE && mHanldeEvent instanceof RecyclerView) {
            RecyclerView recyclerView = (RecyclerView) mHanldeEvent;
            recyclerView.stopScroll();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if (mHanldeEvent == null) {
            return false;
        }
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mCurPosY = e.getRawY();
                mHanldeEvent.setChildView(NestedChildRecyclerView.this);
                mHanldeEvent.setCurPosY(mCurPosY);
                break;
            case MotionEvent.ACTION_MOVE:
                mPosY = mCurPosY;
                mCurPosY = e.getRawY();
                //向上滑动
                if (mCurPosY - mPosY > mTouchStop) {
                    if (canScrollVertically(-1)) {
                        mHanldeEvent.setIntercept(false);
                    }
                }
                //向下滑动
                else if (mCurPosY - mPosY < -mTouchStop) {
                    if (canScrollVertically(1)) {
                        mHanldeEvent.setIntercept(false);
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                mHanldeEvent.setChildView(null);
                break;
            case MotionEvent.ACTION_CANCEL:
                //如果去掉，滑动事件从RvSon过度到RvParent会出现不平滑。　
                if (mHanldeEvent.getIntercept()) {
                    e.setAction(MotionEvent.ACTION_DOWN);
                    mHanldeEvent.actionIntercept(e);
                }
                break;
            default:
                break;
        }
        return super.onTouchEvent(e);
    }

    private void initListener() {
        addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mHanldeEvent == null) {
                    return;
                }
                //滑动到底部
                if (!canScrollVertically(1)) {
                    mHanldeEvent.setIntercept(true);
                }
                //滑动到顶部
                else if (!canScrollVertically(-1)) {
                    mHanldeEvent.setIntercept(true);
                }
            }
        });
    }


    public void setHanldeEvent(HanldeEvent hanldeEvent) {
        mHanldeEvent = hanldeEvent;
    }

    @Override
    public boolean onInterceptHoverEvent(MotionEvent event) {
        return true;
    }
}
