package cn.oi.klittle.era.dialog.timi

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.Gravity
import android.view.View
import android.widget.ScrollView
import android.widget.TextView
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseDialog
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.exception.KCatchException
import cn.oi.klittle.era.utils.KLoggerUtils
import cn.oi.klittle.era.widget.compat.KTextView
import org.jetbrains.anko.*
import java.lang.Exception

//fixme 没有标题，只有确定一个按钮。

//        val alert: KTimiDialog by lazy { KTimiDialog(this) }
//        alert.mession("是否确认退出？").positive("确定") {
//            KToast.showSuccess("点击确定")
//        }.isDismiss(false).show()

open class KTimiDialog(var act: Context, isStatus: Boolean = true, isTransparent: Boolean = false) :
        KBaseDialog(act, isStatus = isStatus, isTransparent = isTransparent) {

    override fun onCreateView(context: Context): View? {
        return context.UI {
            //fixme 放在scrollView，文本框高度自适应。亲测，高度能随文本的变化而变化。能大能小。
            scrollView {
                isFillViewport = true
                setOverScrollMode(View.OVER_SCROLL_NEVER);//设置滑动到边缘时无效果模式
                setVerticalScrollBarEnabled(false);//滚动条隐藏
                verticalLayout {
                    gravity = Gravity.CENTER
                    verticalLayout {
                        gravity = Gravity.CENTER_HORIZONTAL
                        isClickable = true
                        setBackgroundResource(R.drawable.kera_drawable_alert)
                        if (Build.VERSION.SDK_INT >= 21) {
                            z = kpx.x(24f)//会有投影，阴影效果。
                        }
                        //内容
                        KTextView(this).apply {
                            id = kpx.id("crown_txt_mession")
                            textColor = Color.parseColor("#242424")
                            textSize = kpx.textSizeX(36)
                            gravity = Gravity.CENTER_VERTICAL
                            setLineSpacing(kpx.textSizeX(20f), 1f)
                        }.lparams {
                            width = wrapContent
                            height = wrapContent
                            leftMargin = kpx.x(24)
                            rightMargin = leftMargin//右边比左边少一点，效果较好。
                            topMargin = kpx.x(32)
                            bottomMargin = kpx.x(16)
                        }

                        //横线
                        view {
                            id = kpx.id("crown_line")
                            //backgroundColor = Color.parseColor("#F2F2F2")
                            backgroundColor = Color.parseColor("#50000000")
                        }.lparams {
                            width = matchParent
                            height = kpx.x(1)
                        }

                        //确定
                        textView {
                            id = kpx.id("crown_txt_Positive")
                            //textColor = Color.parseColor("#239F93")
                            //textColor = Color.parseColor("#7B7BF7")
                            textColor = Color.parseColor("#42A8E1")
                            textSize = kpx.textSizeX(36)
                            gravity = Gravity.CENTER
                            text = getString(R.string.kconfirm)//确定
                        }.lparams {
                            width = matchParent
                            height = kpx.x(100)
                        }
                    }.lparams {
                        width = kpx.x(620)
                        height = wrapContent
                    }
                }
            }
        }.view
    }

    //信息文本
    var txt_mession: String? = ""
    val mession: KTextView? by lazy { findViewById<KTextView>(kpx.id("crown_txt_mession")) }
    open fun mession(mession: String? = null): KTimiDialog {
        txt_mession = mession
        return this
    }

    val positive: TextView? by lazy { findViewById<TextView>(kpx.id("crown_txt_Positive")) }

    //确定按钮
    open fun positive(postive: String? = getString(R.string.kconfirm), callback: (() -> Unit)? = null): KTimiDialog {
        this.positive?.setText(postive)
        this.positive?.setOnClickListener {
            callback?.run {
                this()
            }
            dismiss()
        }
        return this
    }

    init {
        try {
            //确定
            positive?.setOnClickListener {
                dismiss()
            }
            isDismiss(false)//默认不消失
            //isLocked(true)//fixme 屏蔽返回键(关闭只能点确定按钮)
            mession?.onLineCountChangeCallBack {
                //fixme 文本行数变化监听。
                mession?.let {
                    if (it.lineCount == 1) {
                        it.topPadding = kpx.x(50)
                        it.bottomPadding = it.topPadding
                    } else if (it.lineCount == 2) {
                        it.topPadding = kpx.x(30)
                        it.bottomPadding = it.topPadding
                    } else {
                        it.topPadding = 0
                        it.bottomPadding = it.topPadding
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onShow() {
        super.onShow()
        ctx?.runOnUiThread {
            try {
                //mession?.setText(txt_mession)
                mession?.setAutoSplitText(txt_mession)
                //fixme 监听文本框的内容高度变化。
//                mession?.onGlobalLayoutListener(false) {
//                    mession?.let {
//                        if (it.lineCount == kpx.x(150)) {
//                            it.topPadding = kpx.x(50)
//                            it.bottomPadding = it.topPadding
//                        } else if (it.lineCount == 2) {
//                            it.topPadding = kpx.x(30)
//                            it.bottomPadding = it.topPadding
//                        } else {
//                            it.topPadding = 0
//                            it.bottomPadding = it.topPadding
//                        }
//                    }
//                }
            } catch (e: Exception) {
                KLoggerUtils.e("KTimiDialog显示异常：\t" + KCatchException.getExceptionMsg(e), true)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mession?.onDestroy()
    }
}