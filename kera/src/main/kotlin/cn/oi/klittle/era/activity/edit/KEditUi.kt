package cn.oi.klittle.era.activity.edit

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseUi
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.toolbar.KToolbar
import cn.oi.klittle.era.widget.compat.KEditText
import cn.oi.klittle.era.widget.compat.KTextView
import org.jetbrains.anko.*

open class KEditUi : KBaseUi() {

    var toolbar: KToolbar? = null
    var edit: KEditText? = null
    var txt:KTextView?=null
    override fun destroy(activity: Activity?) {
        super.destroy(activity)
        toolbar = null
    }

    override fun createView(ctx: Context?): View? {

        return ctx?.UI {
            verticalLayout {
                backgroundColor = Color.parseColor("#F5F6FA")
                toolbar = KToolbar(this, getActivity(), false)?.apply {
                    //标题栏背景色
                    contentView?.apply {
                        backgroundColor = Color.WHITE
                    }
                    //左边返回文本（默认样式自带一个白色的返回图标）
                    leftTextView?.apply {
                        autoBg {
                            width = kpx.x(48)
                            height = width
                            autoBg(R.mipmap.kera_back_black)
                        }
                    }
                    leftTextView2?.apply {
                        textColor = Color.BLACK
                        textSize = kpx.textSizeX(32)
                        gravity = Gravity.CENTER
                        text = getString(R.string.ktextedit)//文本编辑
                        visibility = View.VISIBLE//显示
                    }
                    titleTextView?.apply {
                        textColor = Color.WHITE
                        textSize = kpx.textSizeX(32)
                        gravity = Gravity.CENTER
                    }
                    rightTextView?.apply {
                        textColor = Color.BLACK
                        textSize = kpx.textSizeX(32)
                        setText(getString(R.string.ksave))//保存
                        isBold(true)
                    }
                }
                scrollView {
                    isFillViewport = true
                    setOverScrollMode(View.OVER_SCROLL_NEVER);//设置滑动到边缘时无效果模式
                    setVerticalScrollBarEnabled(false);//滚动条隐藏
                    verticalLayout {
                        edit = keditText {
                            leftPadding = kpx.x(16)
                            rightPadding = leftPadding
                            topPadding = leftPadding
                            bottomPadding = topPadding
                            hint = getString(R.string.kpeleaseInput)//请输入
                            textColor = Color.BLACK
                            textSize = kpx.textSizeX(32)
                            hintTextColor = Color.parseColor("#999999")
                            gravity = Gravity.LEFT or Gravity.TOP
                            isFocusable = true
                            isFocusableInTouchMode = true
                            backgroundColor(Color.WHITE)
                            notContainsRegex("\u0020\n\r")//不包含空格和换行符。
                        }.lparams {
                            width = matchParent
                            height = kpx.y(600)
                            topMargin = kpx.x(16)
                        }
                        txt=ktextView {
                            leftPadding = kpx.x(16)
                            rightPadding = leftPadding
                            topPadding = leftPadding
                            bottomPadding = topPadding
                            gravity = Gravity.CENTER_VERTICAL or Gravity.RIGHT
                            textColor = Color.BLACK
                            textSize = kpx.textSizeX(34)
                            backgroundColor(Color.WHITE)
                        }.lparams {
                            width = matchParent
                            height = wrapContent
                        }
                    }
                }.lparams {
                    width = matchParent
                    height = matchParent
                }
            }
        }?.view
    }
}