package cn.oi.klittle.era.utils

import cn.oi.klittle.era.activity.photo.tools.StringUtils
import java.math.BigDecimal

///数值加减计算，防止精度缺失，如：0.3+0.6=0.9；double 会变成：0.8999999999999999
object KBigDecimalUtils {

    ///获取小数点后面的位数。
    fun getDecimalDigitsLength(num: String?): Int {
        num?.trim()?.let {
            if (it.contains(".")) {
                it.split('.')?.let {
                    if (it.size >= 2) {
                        return it[1].trim().length;
                    }
                }
            }
        }
        return 0;
    }

    ///与原有字符串，保持小数点位数一致。
    fun getDecimalDigits(num1: String?, num2: String?, num3: String?): String? {
        var num10 = getDecimalDigitsLength(num1);
        var num20 = getDecimalDigitsLength(num2);
        var num30 = getDecimalDigitsLength(num3);
        if (num10 >= num20) {
            if (num10 > num30) {
                var n0 = num10 - num30;
                var nn0 = ""
                for (i in 1..n0) {
                    nn0 += "0";
                }
                if (num30 > 0) {
                    return num3 + nn0;
                } else {
                    return num3 + "." + nn0;
                }
            }
        } else {
            if (num20 > num30) {
                var n0 = num20 - num30;
                var nn0 = ""
                for (i in 1..n0) {
                    nn0 += "0";
                }
                if (num30 > 0) {
                    return num3 + nn0;
                } else {
                    return num3 + "." + nn0;
                }
            }
        }
        return num3;
    }

    ///加法;放心精度不会缺失。0.3+0.6=0.9；0.333+0.6=0.933；亲测。 0.30+0.6=0.90,0不会抹去，与原有小数位数，会保持一致。
    ///BigDecimal最好传入字符串或者int类型的数字;double类型不行，
    fun add(num1: String?, num2: String?): String? {
        if (KStringUtils.isEmpty(num1) && KStringUtils.isEmpty(num2)) {
            return ""
        }
        if (KStringUtils.isEmpty(num1)) {
            return num2;
        }
        if (KStringUtils.isEmpty(num2)) {
            return num1;
        }
        val bignum1 = BigDecimal(num1)
        val bignum2 = BigDecimal(num2)
        //加法
        var bignum3 = bignum1.add(bignum2);
        //return bignum3.toString();//但是 0.30+0.60=0.9 会自动去除末尾的0.
        return getDecimalDigits(num1, num2, bignum3.toString())//与原有小数位保持一致。
    }

    ///加法；最好传字符串，这里double 0.60 字符串会变成0.6 ，0会抹去。
    fun add(num1: Double?, num2: Double?): Double? {
        //return add(num1?.toString(), num2?.toString())
        if (num1 != null && num2 != null) {
            val b1 = BigDecimal.valueOf(num1);
            val b2 = BigDecimal.valueOf(num2);
            return b1.add(b2).toDouble();
        } else if (num1 != null) {
            return num1;
        } else if (num2 != null) {
            return num2;
        }
        return null;
    }

    ///减法
    fun subtract(num1: String?, num2: String?): String? {
        if (KStringUtils.isEmpty(num1) && KStringUtils.isEmpty(num2)) {
            return ""
        }
        if (KStringUtils.isEmpty(num1)) {
            return num2;
        }
        if (KStringUtils.isEmpty(num2)) {
            return num1;
        }
        val bignum1 = BigDecimal(num1)
        val bignum2 = BigDecimal(num2)
        //减法
        var bignum3 = bignum1.subtract(bignum2);
        //return bignum3.toString();//但是 0.30+0.60=0.9 会自动去除末尾的0.
        return getDecimalDigits(num1, num2, bignum3.toString())//与原有小数位保持一致。
    }

    ///减法
    fun subtract(num1: Double?, num2: Double?): Double? {
        //return subtract(num1?.toString(), num2?.toString())
        if (num1 != null && num2 != null) {
            val b1 = BigDecimal.valueOf(num1);
            val b2 = BigDecimal.valueOf(num2);
            return b1.subtract(b2).toDouble();
        } else if (num1 != null) {
            return num1;
        } else if (num2 != null) {
            return num2;
        }
        return null;
    }

    ///乘法
    fun multiply(num1: String?, num2: String?): String? {
        if (KStringUtils.isEmpty(num1) && KStringUtils.isEmpty(num2)) {
            return ""
        }
        if (KStringUtils.isEmpty(num1)) {
            return num2;
        }
        if (KStringUtils.isEmpty(num2)) {
            return num1;
        }
        val bignum1 = BigDecimal(num1)
        val bignum2 = BigDecimal(num2)
        //乘法
        var bignum3 = bignum1.multiply(bignum2);
        //return bignum3.toString();//但是 0.30+0.60=0.9 会自动去除末尾的0.
        return getDecimalDigits(num1, num2, bignum3.toString())//与原有小数位保持一致。
    }

    ///乘法
    fun multiply(num1: Double?, num2: Double?): Double? {
        //return multiply(num1?.toString(), num2?.toString());
        if (num1 != null && num2 != null) {
            val b1 = BigDecimal.valueOf(num1);
            val b2 = BigDecimal.valueOf(num2);
            return b1.multiply(b2).toDouble();
        } else if (num1 != null) {
            return num1;
        } else if (num2 != null) {
            return num2;
        }
        return null;
    }

    ///除法
    fun divide(num1: String?, num2: String?): String? {
        if (KStringUtils.isEmpty(num1) && KStringUtils.isEmpty(num2)) {
            return ""
        }
        if (KStringUtils.isEmpty(num1)) {
            return num2;
        }
        if (KStringUtils.isEmpty(num2)) {
            return num1;
        }
        val bignum1 = BigDecimal(num1)
        val bignum2 = BigDecimal(num2)
        //除法
        var bignum3 = bignum1.divide(bignum2);
        //return bignum3.toString();//但是 0.30+0.60=0.9 会自动去除末尾的0.
        return getDecimalDigits(num1, num2, bignum3.toString())//与原有小数位保持一致。
    }

    ///除法
    fun divide(num1: Double?, num2: Double?): Double? {
        //return divide(num1?.toString(), num2?.toString());
        if (num1 != null && num2 != null) {
            val b1 = BigDecimal.valueOf(num1);
            val b2 = BigDecimal.valueOf(num2);
            return b1.divide(b2).toDouble();
        } else if (num1 != null) {
            return num1;
        } else if (num2 != null) {
            return num2;
        }
        return null;
    }

}