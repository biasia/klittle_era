package cn.oi.klittle.era.sms

import android.app.Activity
import android.database.ContentObserver
import android.database.Cursor
import android.net.Uri
import android.os.Handler
import android.util.Log
import java.util.regex.Pattern

//                            var mContext= getActivity();
//                            if (mObserver!=null){
//                                mContext?.getContentResolver()?.unregisterContentObserver(mObserver!!);
//                            }
//                            //实例化短信监听器
//                            mObserver = SMSContentObserver(getActivity(),callBack = {
//                                KLoggerUtils.e("短信内容:"+it);
//                                text = "读取短信"+it;
//                                //注销短信监听
////                                if (mObserver!=null){
////                                    mContext?.getContentResolver()?.unregisterContentObserver(mObserver!!);
////                                }
//                            });
//                            // 注册短信变化监听
//                            if (mObserver!=null){
//                                mContext?.contentResolver?.registerContentObserver(Uri.parse("content://sms/"), true, mObserver!!)
//                            }

///callBack 短信回调
class SMSContentObserver(var activity: Activity?, var callBack: ((smsContent: String) -> Unit)? = null) : ContentObserver(Handler()) {
    val SMS_URI_INBOX = "content://sms/inbox" //收信箱
    private var smsContent = "" //验证码
    private var smsID = ""
    var SMS_ADDRESS_PRNUMBER = "10692278147465857" //fixme 短息发送提供商 10692278147465857
    var regex="[0-9]{6}";//fixme 匹配到6位的验证码

    ///短信观察者 收到一条短信时 onchange方法会执行两次，所以比较短信id，如果一致则不处理
    override fun onChange(selfChange: Boolean) {
        super.onChange(selfChange)
        Log.e("test", "短信=======================1\t");
        var cursor: Cursor? = null // 光标
        // 读取收件箱中指定号码的短信
        //cursor = activity!!.contentResolver.query(Uri.parse(SMS_URI_INBOX), arrayOf("_id", "address", "body", "read"),  //要读取的属性
        //        "address=? and read=?", arrayOf(SMS_ADDRESS_PRNUMBER, "0"),  //fixme 查询条件赋值,添加了查询条件之后，反而查询不到了。
        //        "date desc") //排序
        val inboxUri = Uri.parse("content://sms/inbox")
        cursor = activity!!.getContentResolver().query(inboxUri, null, null, null, "date desc")
        Log.e("test", "短信=======================2\t" + cursor);
        if (cursor != null) {
            if (cursor.moveToFirst()){
                var index_id=cursor.getColumnIndex("_id");
                val address: String = cursor.getString(cursor.getColumnIndex("address"))
                val body: String = cursor.getString(cursor.getColumnIndex("body"))

                Log.e("test", "短信=======================4\t" + index_id + "\t" + address + "\t" + body);
                var _id=cursor.getString(index_id);
                Log.e("test", "短信=======================5\t" + smsID + "\t" + _id);
                //比较和上次接收到短信的ID是否相等
                if (smsID != _id) {
                    val smsbody = cursor.getString(cursor.getColumnIndex("body"))
                    Log.e("test", "短信body:\t" + smsbody);//fixme 测试发现，还是无法正确读取。没有读到最新的短信消息。
                    //用正则表达式匹配验证码
                    val pattern = Pattern.compile(regex)
                    val matcher = pattern.matcher(smsbody)
                    if (matcher.find()) { //匹配到6位的验证码
                        smsContent = matcher.group() //fixme 短信內容
                        if (smsContent!=null&&smsContent.length>0){
                            callBack?.let { it(smsContent) }
                        }
                    }
                    smsID = cursor.getString(cursor.getColumnIndex("_id"))
                }
            }
        }
    }
}