package cn.oi.klittle.era.entity.widget.compat

import android.graphics.Color

/**
 * fixme 颜色渐变实体类
 * @param horizontalColors 水平颜色渐变
 * @param verticalColors 垂直颜色渐变 最后绘制，即优先级最高;fixme 水平渐变和垂直渐变同时有效。垂直渐变会覆盖水平渐变的上面。
 * @param top_color 上半部分颜色
 * @param bottom_color 下半部分颜色
 * @param left_color 左半部分颜色
 * @param right_color 右半部分颜色
 * @param left_top_color 左上角部分颜色
 * @param right_top_color 右上角部分颜色
 * @param left_bottom_color 左下角部分颜色
 * @param right_bottom_color 右下角部分颜色
 * @param isDraw 是否绘制
 */
data class KGradientEntity(var horizontalColors: IntArray? = null, var verticalColors: IntArray? = null,
                           var top_color: Int = Color.TRANSPARENT, var bottom_color: Int = Color.TRANSPARENT, var left_color: Int = Color.TRANSPARENT, var right_color: Int = Color.TRANSPARENT,
                           var left_top_color: Int = Color.TRANSPARENT, var right_top_color: Int = Color.TRANSPARENT, var left_bottom_color: Int = Color.TRANSPARENT, var right_bottom_color: Int = Color.TRANSPARENT,
                           var isDraw: Boolean = true) {
    //fixme 水平渐变颜色数组值【均匀渐变】
    open fun horizontalColors(vararg color: Int) {
        verticalColors = null
        horizontalColors = color
    }

    open fun horizontalColors(vararg color: String) {
        verticalColors = null
        horizontalColors = IntArray(color.size)
        horizontalColors?.apply {
            if (color.size > 0) {
                for (i in 0..color.size - 1) {
                    this[i] = Color.parseColor(color[i])
                }
            }
        }
    }

    //fixme 垂直渐变颜色数组值【均匀】
    open fun verticalColors(vararg color: Int) {
        horizontalColors = null
        verticalColors = color
    }

    //fixme 如：verticalColors("#00dedede","#dedede") 向上的阴影线
    open fun verticalColors(vararg color: String) {
        horizontalColors = null
        verticalColors = IntArray(color.size)
        verticalColors?.apply {
            if (color.size > 0) {
                for (i in 0..color.size - 1) {
                    this[i] = Color.parseColor(color[i])
                }
            }
        }
    }

    //fixme 上半部分颜色
    open fun top_color(top_color: Int) {
        this.top_color = top_color
    }

    open fun top_color(top_color: String) {
        this.top_color = Color.parseColor(top_color)
    }

    //fixme 下半部分颜色
    open fun bottom_color(bottom_color: Int) {
        this.bottom_color = bottom_color
    }

    open fun bottom_color(bottom_color: String) {
        this.bottom_color = Color.parseColor(bottom_color)
    }

    //fixme 左半部分颜色
    open fun left_color(left_color: Int) {
        this.left_color = left_color
    }

    open fun left_color(left_color: String) {
        this.left_color = Color.parseColor(left_color)
    }

    //fixme 右半部分颜色
    open fun right_color(right_color: Int) {
        this.right_color = right_color
    }

    open fun right_color(right_color: String) {
        this.right_color = Color.parseColor(right_color)
    }

    //fixme 左上角部分颜色
    open fun left_top_color(left_top_color: Int) {
        this.left_top_color = left_top_color
    }

    open fun left_top_color(left_top_color: String) {
        this.left_top_color = Color.parseColor(left_top_color)
    }

    //fixme 右上角部分颜色
    open fun right_top_color(right_top_color: Int) {
        this.right_top_color = right_top_color
    }

    open fun right_top_color(right_top_color: String) {
        this.right_top_color = Color.parseColor(right_top_color)
    }

    //fixme 左下角部分颜色
    open fun left_bottom_color(left_bottom_color: Int) {
        this.left_bottom_color = left_bottom_color
    }

    open fun left_bottom_color(left_bottom_color: String) {
        this.left_bottom_color = Color.parseColor(left_bottom_color)
    }

    //fixme 右下角部分颜色
    open fun right_bottom_color(right_bottom_color: Int) {
        this.right_bottom_color = right_bottom_color
    }

    open fun right_bottom_color(right_bottom_color: String) {
        this.right_bottom_color = Color.parseColor(right_bottom_color)
    }
}