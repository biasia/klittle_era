package cn.oi.klittle.era.dialog.progressbar

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.Gravity
import android.view.View
import android.view.View.OVER_SCROLL_NEVER
import cn.oi.klittle.era.base.KBaseDialog
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.utils.KLoggerUtils
import cn.oi.klittle.era.widget.progress.KNumberProgressBar
import org.jetbrains.anko.*
import java.lang.Exception

// var progressbar = KDownProgressbarDialog(act)
// progressbar?.setProgressbar(bias)//显示下载进度(0~100)。
//fixme 网络下载进度条。

open class KDownProgressbarDialog(var act: Context, isStatus: Boolean = true, isTransparent: Boolean = false) :
        KBaseDialog(act, isStatus = isStatus, isTransparent = isTransparent) {

    val numberProgressBar: KNumberProgressBar? by lazy { findViewById<KNumberProgressBar>(kpx.id("crown_knumberProgressBar")) }
    override fun onCreateView(context: Context): View? {
        return context.UI {
            ui {
                //fixme 放在scrollView，文本框高度自适应。亲测，高度能随文本的变化而变化。能大能小。
                scrollView {
                    setOverScrollMode(OVER_SCROLL_NEVER);//设置滑动到边缘时无效果模式
                    setVerticalScrollBarEnabled(false);//滚动条隐藏
                    isFillViewport = true
                    verticalLayout {
                        gravity = Gravity.CENTER
                        kverticalLayout {
                            if (Build.VERSION.SDK_INT >= 21) {
                                z = kpx.x(24f)//会有投影，阴影效果。
                            }
                            shadow_radius = kpx.x(12f)
                            shadow {
                                bg_color = Color.WHITE
                                all_radius(kpx.x(0))
                                shadow_color = Color.parseColor("#333333")//效果比Color.BLACK好。
                            }
                            gravity = Gravity.CENTER_VERTICAL
                            leftPadding = kpx.x(32 + 10)
                            rightPadding = leftPadding
                            topPadding = leftPadding
                            bottomPadding = topPadding

                            knumberProgressBar {
                                id = kpx.id("crown_knumberProgressBar")
                                //KLoggerUtils.e("id:\t"+id)
                            }.lparams {
                                width = matchParent
                                height = kpx.x(36)
                                topMargin = kpx.x(16)
                                bottomMargin = topMargin
                            }

                        }.lparams {
                            width = kpx.x(700)
                            height = wrapContent
                        }
                    }.lparams {
                        width = matchParent
                        height = matchParent
                    }
                }
            }
        }.view
    }

    init {
        try {
            isDismiss(false)//默认不消失
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * 设置下载进度
     * @param bias (0~100)
     */
    fun setProgressbar(bias: Int?) {
        bias?.let {
            //KLoggerUtils.e("下载进度：\t" + bias)
            act?.runOnUiThread {
                //KLoggerUtils.e("numberProgressBar:\t"+numberProgressBar+"\tdialog:\t"+dialog+"\t"+isShow()+"\t"+ctx+"\t"+dialog?.findViewById<View?>(kpx.id("crown_knumberProgressBar")))
                numberProgressBar?.setProgress(bias)
            }
        }
    }

    override fun onShow() {
        super.onShow()
    }
}