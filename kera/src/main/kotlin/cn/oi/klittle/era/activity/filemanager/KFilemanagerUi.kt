package cn.oi.klittle.era.activity.filemanager

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import cn.oi.klittle.era.R
import cn.oi.klittle.era.activity.filemanager.adapter.KFileAdapter
import cn.oi.klittle.era.base.KBaseUi
import cn.oi.klittle.era.base.KUi
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.toolbar.KToolbar
import cn.oi.klittle.era.widget.compat.KTextView
import cn.oi.klittle.era.widget.recycler.KRecyclerView
import cn.oi.klittle.era.widget.web.K3WebView
import org.jetbrains.anko.*

open class KFilemanagerUi : KUi() {

    var toolbar: KToolbar? = null
    override fun destroy(activity: Activity?) {
        super.destroy(activity)
        toolbar = null
    }

    var recycler: KRecyclerView? = null
    var fileAdapt: KFileAdapter? = null
    override fun createView(ctx: Context?): View? {

        return ctx?.UI {
            verticalLayout {
                //fitsSystemWindows = true
                backgroundColor = Color.WHITE
                toolbar = KToolbar(this, getActivity(), false,hasLine=true)?.apply {
                    //标题栏背景色
                    contentView?.apply {
                        backgroundColor = Color.WHITE
                    }
                    //左边返回文本（默认样式自带一个白色的返回图标）
                    leftTextView?.apply {
                        autoBg { autoBgColor = Color.BLACK }
                    }
                    titleTextView?.apply {
                        text = getString(R.string.kfile_management)//文件管理
                        textColor = Color.BLACK
                    }
                }
                krecyclerView {
                    setLinearLayoutManager(true)
                    if (fileAdapt == null) {
                        fileAdapt = KFileAdapter()
                    }
                    adapter = fileAdapt
                }.lparams {
                    width = matchParent
                    height = matchParent
                }
            }
        }?.view
    }

    //显示进度条
    open fun showProgress() {
        _dialogProgressbar(false)?.show()
    }

    open fun dissmissProgress() {
        _dialogProgressbar(false)?.dismiss()
    }
}