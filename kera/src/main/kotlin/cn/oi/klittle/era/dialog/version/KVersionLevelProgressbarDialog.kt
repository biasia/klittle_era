package cn.oi.klittle.era.dialog.version

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.Gravity
import android.view.View
import android.view.View.OVER_SCROLL_NEVER
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseActivity
import cn.oi.klittle.era.base.KBaseApplication
import cn.oi.klittle.era.base.KBaseDialog
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.exception.KCatchException
import cn.oi.klittle.era.utils.KAppUtils
import cn.oi.klittle.era.utils.KFileLoadUtils
import cn.oi.klittle.era.utils.KLoggerUtils
import cn.oi.klittle.era.widget.progress.KNumberProgressBar
import org.jetbrains.anko.*
import java.io.File
import java.lang.Exception

//fixme 版本更新,网络下载进度条。

open class KVersionLevelProgressbarDialog(var act: Context, isStatus: Boolean = true, isTransparent: Boolean = false) :
        KBaseDialog(act, isStatus = isStatus, isTransparent = isTransparent) {

    //apk下载链接
    var url: String? = null

    //文件名，包括后缀。如果为null或""空，会自动获取网络上的名称。
    var srcFileName: String? = null

    //文件下载目录，可以为空。如果为空。会使用默认下载目录 KFileLoadUtils里的cacheDir
    var downDir: String? = null

    //显示错误信息
    open fun showError(result: String?, code: Int) {
        //下载失败；错误代码：code
        if (result != null) {
            //KToast.showError(result + ";" + getString(R.string.kerror_code) + ":\t" + code)
            act?.let {
                if (it is KBaseActivity) {
                    it.runOnUiThread {
                        it._dialogInfo(result + ";\n" + getString(R.string.kerror_code) + ":\t" + code)
                    }
                }
            }
        }
    }

    open fun loadDown() {
        if (ctx != null && url != null) {
            //fixme 放心，支持断点下载(不会重复下载)
            KFileLoadUtils.getInstance(true).downLoad(ctx, url, downDir, srcFileName, object : KFileLoadUtils.RequestCallBack {
                override fun onStart() {
                    //开始下载
                    //KLoggerUtils.e("开始下载")
                }

                override fun onFailure(isLoad: Boolean?, result: String?, code: Int, file: File?) {
                    //下载失败（关闭弹窗）
                    dismiss()
                    //下载失败
                    if (isLoad!! && ctx != null) {
                        //已经下载(apk安装包自动进行安装)
                        file?.let {
                            if (KAppUtils.isApk(it)) {
                                KAppUtils.installation(KBaseApplication.getInstance(), file)
                            }
                        }
                    } else {
                        if (result != null) {
                            //KToast.showError(result)
                            showError(result, code)
                        } else {
                            showError(getString(R.string.kappdownfail), code)//下载失败
                        }
                    }
                }

                override fun onSuccess(file: File?) {
                    //下载完成安装（关闭弹窗）
                    dismiss()
                    file?.let {
                        //apk安装包自动进行安装。
                        if (KAppUtils.isApk(it)) {
                            KAppUtils.installation(KBaseApplication.getInstance(), file)
                        }
                    }
                }

                override fun onLoad(current: Long, max: Long, bias: Int) {
                    //下载进度
                    //KLoggerUtils.e("下载进度：\t" + bias)
                    act?.runOnUiThread {
                        numberProgressBar?.setProgress(bias)
                    }
                }
            })
        }
    }

    val numberProgressBar: KNumberProgressBar? by lazy { findViewById<KNumberProgressBar>(kpx.id("crown_knumberProgressBar")) }
    override fun onCreateView(context: Context): View? {
        return context.UI {
            ui {
                //fixme 放在scrollView，文本框高度自适应。亲测，高度能随文本的变化而变化。能大能小。
                scrollView {
                    setOverScrollMode(OVER_SCROLL_NEVER);//设置滑动到边缘时无效果模式
                    setVerticalScrollBarEnabled(false);//滚动条隐藏
                    isFillViewport = true
                    verticalLayout {
                        gravity = Gravity.CENTER
                        kverticalLayout {
                            if (Build.VERSION.SDK_INT >= 21) {
                                z = kpx.x(24f)//会有投影，阴影效果。
                            }
                            shadow_radius = kpx.x(12f)
                            shadow {
                                bg_color = Color.WHITE
                                all_radius(kpx.x(0))
                                shadow_color = Color.parseColor("#333333")//效果比Color.BLACK好。
                            }
                            gravity = Gravity.CENTER_VERTICAL
                            leftPadding = kpx.x(32 + 10)
                            rightPadding = leftPadding
                            topPadding = leftPadding
                            bottomPadding = topPadding

                            knumberProgressBar {
                                id = kpx.id("crown_knumberProgressBar")
                            }.lparams {
                                width = matchParent
                                height = kpx.x(36)
                                topMargin = kpx.x(16)
                                bottomMargin = topMargin
                            }

                        }.lparams {
                            width = kpx.x(700)
                            height = wrapContent
                        }
                    }.lparams {
                        width = matchParent
                        height = matchParent
                    }
                }
            }
        }.view
    }

    init {
        try {
            isDismiss(false)//默认不消失
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onShow() {
        super.onShow()
        ctx?.runOnUiThread {
            try {
                loadDown()//fixme 开始下载
            } catch (e: Exception) {
                KLoggerUtils.e("KVersionLevelProgressbarDialog显示异常：\t" + KCatchException.getExceptionMsg(e), true)
            }
        }
    }
}