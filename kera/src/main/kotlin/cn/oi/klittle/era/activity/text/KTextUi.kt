package cn.oi.klittle.era.activity.text

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseUi
import cn.oi.klittle.era.comm.KToast
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.helper.KUiHelper
import cn.oi.klittle.era.toolbar.KToolbar
import cn.oi.klittle.era.widget.compat.KEditText
import cn.oi.klittle.era.widget.compat.KTextView
import org.jetbrains.anko.*

open class KTextUi : KBaseUi() {

    var toolbar: KToolbar? = null
    var txt: KTextView? = null
    override fun destroy(activity: Activity?) {
        super.destroy(activity)
        toolbar = null
        txt?.setText(null)
        txt = null
    }

    override fun createView(ctx: Context?): View? {

        return ctx?.UI {
            verticalLayout {
                backgroundColor = Color.parseColor("#F5F6FA")
                toolbar = KToolbar(this, getActivity(), false)?.apply {
                    //标题栏背景色
                    contentView?.apply {
                        backgroundColor = Color.WHITE
                    }
                    //左边返回文本（默认样式自带一个白色的返回图标）
                    leftTextView?.apply {
                        autoBg {
                            width = kpx.x(48)
                            height = width
                            autoBg(R.mipmap.kera_back_black)
                        }
                    }
                    leftTextView2?.apply {
                        textColor = Color.BLACK
                        textSize = kpx.textSizeX(32)
                        isBold(true)
                        gravity = Gravity.CENTER
                        text = getString(R.string.ktextlook)//文本查看
                        visibility = View.VISIBLE//显示
                        setMore(1)
                    }
                    if (KTextActivity.isSharedable) {
                        rightTextView?.apply {
                            autoBg {
                                width = kpx.x(48)
                                height = kpx.x(48)
                                autoBg(R.mipmap.kera_share)//分享图标
                            }
                        }
                    }
                }
                kScrollViewBar(ctx, this)?.apply {
                    openUpAnime = false
                    openDownAnime = false
                    verticalLayout {
                        txt = ktextView {
                            textColor = Color.BLACK
                            textSize = kpx.textSizeX(33)
                            //isBold(true)
                            leftPadding = kpx.x(8)
                            bottomPadding = leftPadding
                        }.lparams {
                            width = matchParent
                            height = matchParent
                        }
                    }
                }?.lparams {
                    width = matchParent
                    height = matchParent
                }
            }
        }?.view
    }
}