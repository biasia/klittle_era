package cn.oi.klittle.era.widget.recycler.nested.code;

import android.view.MotionEvent;
import android.view.View;

public interface HanldeEvent {
    void setIntercept(boolean intercept);
    boolean getIntercept();
    void setChildView(View childView);
    void setCurPosY(float curPosY);
    void actionIntercept(MotionEvent event);
}
