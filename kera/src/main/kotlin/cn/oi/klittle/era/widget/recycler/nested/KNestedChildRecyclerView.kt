package com.idogfooding.glassscanner.widget.rv

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import cn.oi.klittle.era.widget.recycler.nested.code.HanldeEvent
import cn.oi.klittle.era.widget.recycler.nested.code.NestedChildRecyclerView

/**
 * 嵌套RecyclerView子类RecyclerView；解决 RecyclerView嵌套RecyclerView滑动冲突的问题。这个效果不好，没有KRecyclerViewAtRecycleView好。
 * 1.记得调用 setHanldeEvent() 设置父类 RecyclerView；
 * 测试发现，垂直滑动正常，水平滑动可能还是有点卡顿。
 */
class KNestedChildRecyclerView : NestedChildRecyclerView {
    constructor(context: Context) : super(context) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {}

    constructor(viewGroup: ViewGroup) : super(viewGroup.context) {
        viewGroup.addView(this)//直接添加进去,省去addView(view)
    }

    ///设置父类RecyclerView，即：赋值KNestedRecyclerView即可。
    override fun setHanldeEvent(hanldeEvent: HanldeEvent?) {
        super.setHanldeEvent(hanldeEvent)
    }


}