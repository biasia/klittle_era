package cn.oi.klittle.era.entity.feature

import cn.oi.klittle.era.utils.KLoggerUtils
import cn.oi.klittle.era.utils.KPinyin4jUtils
import cn.oi.klittle.era.utils.KRegexUtils

//                fixme 调用案例
//                var pinyin=KPinYinEntry()
//                var cStr=edt?.text?.toString()?.trim()
//                pinyin.resetStr(cStr)//初始化数据
//                //pinyin?.clearStr()//清除数据
/**
 * 中文转拼音实体类
 */
class KPinYinEntry {
    var cStr: String? = null//中文
    var cStrHYP: String? = null//拼音(取的cStrHYs数组第一个),如：中国 zhongguo
    var cStrHYF: String? = null//首字母,如：中国 zg
    var cStrHYPs: ArrayList<String>? = null//英文集合（小写） fixme （一般就一个，有多音字时才会出现数组）
    var cStrHYFs: ArrayList<String>? = null//英文首字母集合（都是小写）

    constructor(cStr: String? = null) {
        resetStr(cStr)
    }

    constructor() {
    }

    /**
     * 判断是否包含该字符;
     * @param str 字符
     * @param isRegexCstr true 中文一起判断，false 只判断拼音。fixme 默认false只判断拼音。
     */
    fun contains(str: String?, isRegexCstr: Boolean = false): Boolean {
        if (cStr == null) {
            return false
        }
        str?.trim()?.let {
            if (it.length <= 0) {
                return false
            }
            if (isRegexCstr && cStr!!.contains(it)) {
                return true
            }
            if (KRegexUtils.isLetter(it)) {//fixme 正则判断是否为纯字母
                it.toLowerCase()?.let { //转小写字母
                    var str = it
                    //首先判断拼音首字母
                    cStrHYFs?.forEach {
                        //KLoggerUtils.e("it:\t"+it+"\tstr:\t"+str)
                        if (it.contains(str)) {
                            return true
                        }
                    }
                    //再判断拼音集合
                    cStrHYPs?.forEach {
                        if (it.contains(str)) {
                            return true
                        }
                    }
                }
            }
        }
        return false
    }

    //重置数据
    fun resetStr(cStr: String?) {
        this.cStr = cStr
        if (this.cStr == null) {
            clearStr()
        }
        cStr?.trim()?.let {
            if (it.length > 0) {
                if (cStrHYPs == null) {
                    cStrHYPs = arrayListOf()
                }
                cStrHYPs?.clear()
                if (cStrHYFs == null) {
                    cStrHYFs = arrayListOf()
                }
                cStrHYFs?.clear()
                //fixme 查询一个字符串，如："大重好哈哈哈",大约最多31毫秒。300个是 252毫秒。1000个是635毫秒。三千个是1786毫秒。
                //fixme 性能方面还是比较可以的。字符越长长，耗时越多。
                KPinyin4jUtils.toHanyuPinyinStringArrayAndFirstArray(it, cStrHYPs!!, cStrHYFs!!)//获取拼音和首字母集合
                cStrHYPs?.let {
                    if (it.size > 0) {
                        cStrHYP = it[0]
                    }
                }
                cStrHYFs?.let {
                    if (it.size > 0) {
                        cStrHYF = it[0]
                    }
                }
            }
        }
    }

    //清除数据
    fun clearStr() {
        cStr = null
        cStrHYP = null
        cStrHYPs?.clear()
        cStrHYPs = null
        cStrHYFs?.clear()
        cStrHYFs = null
    }

}