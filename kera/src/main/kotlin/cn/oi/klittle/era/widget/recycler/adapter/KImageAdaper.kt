package cn.oi.klittle.era.widget.recycler.adapter

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseActivity
import cn.oi.klittle.era.base.KBaseUi
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.helper.KUiHelper
import cn.oi.klittle.era.utils.KLoggerUtils
import cn.oi.klittle.era.utils.KRegexUtils
import cn.oi.klittle.era.widget.compat.KTextView
import cn.oi.klittle.era.widget.compat.layout.linearLayout.KVerticalLayout
//import kotlinx.android.synthetic.main.kera_activity_capture.*
import org.jetbrains.anko.*

/**
 * fixme 图片适配器
 * @param datas 图片地址集合
 * @param isUpload 是否上传图片。
 * @param spanCount 每行显示图片个数
 */
open class KImageAdaper(var datas: ArrayList<String>? = null, var isUpload: Boolean = false, var spanCount: Int = 5) :
        KAdapter<KImageAdaper.Companion.MyViewHolder>() {

    companion object {

        open class MyViewHolder(itemView: View, var viewType: Int) :
                RecyclerView.ViewHolder(itemView) {
            var item_img: KTextView? = null//图片
            var item_layout: KVerticalLayout? = null//最外层布局

            init {
                //正常kpx
                item_img = itemView?.findViewById(kpx.id("item_img"))
                item_layout = itemView?.findViewById(kpx.id("item_layout"))
            }
        }

    }

    var imageMaxWidth = 0//图片最大宽度。
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var itemView: View? = null
        //正常
        itemView = parent.context.UI {
            KBaseUi.apply {
                var offset = kpx.x(32)//图片与图片之间留点间隙。
                imageMaxWidth = (kpx.screenWidth() / spanCount) - offset
                verticalLayout {
                    gravity = Gravity.CENTER
                    klinearLayout {
                        gravity = Gravity.CENTER
                        id = kpx.id("item_layout")
                        //图片
                        KTextView(this).apply {
                            id = kpx.id("item_img")
                            textSize = kpx.textSizeX(30)//字体小一点，防止显示不全。
                            //textColor = Color.parseColor("#ff3000")
                            textColor = Color.parseColor("#999999")
                            gravity = Gravity.CENTER_VERTICAL
                        }.lparams {
                            width = imageMaxWidth
                            height = width
                        }
                    }.lparams {
                        width = imageMaxWidth
                        height = width
                    }
                }
            }
        }.view
        return MyViewHolder(
                itemView,
                viewType
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        //正常
        var mPosition = position
        if (isUpload) {
            mPosition = position - 1
            if (position == 0) {
                //fixme 图片上传
                holder?.item_img?.let {
                    it.autoBg {
                        width = kpx.x(100)
                        height = kpx.x(100)
                        autoBg(R.mipmap.kera_add)
                    }
                    it.onClick {
                        getActivity()?.let {
                            if (it is KBaseActivity) {
                                it.pictrueSelectorForPath {
                                    datas?.addAll(it)
                                    notifyDataSetChanged()
                                }
                            }
                        }
                    }
                }
                return
            }
        } else {
            if (datas == null && position == 0) {
                holder?.item_img?.let {
                    it.setText(getString(cn.oi.klittle.era.R.string.kimage_empty))//图片为空。
                    return
                }
            }
        }
        var data = datas?.get(mPosition)
        data?.let {
            var mPath = it
            holder?.item_img?.let {
                if (KRegexUtils.isUrl(mPath)) {
                    //fixme 网络图片
                    it.autoBg {
                        resId = null
                        filePath = null
                        width = imageMaxWidth - kpx.x(16)
                        height = width
                        autoBgFromUrl(mPath)
                        isAutoCenter = true
                    }
                } else {
                    //fixme 本地图片
                    it.autoBg {
                        resId = null
                        url = null
                        width = imageMaxWidth - kpx.x(16)
                        height = width
                        autoBgFromFile(mPath)
                        isAutoCenter = true
                    }
                }
                it.onClick {
                    //fixme 图片预览
                    getActivity()?.let {
                        if (it is KBaseActivity) {
                            it.pictruePreviewForPath(position = mPosition, paths = datas?.toMutableList())
                        }
                    }
                }
                if (isUpload) {
                    it.autoBg2 {
                        width = kpx.x(40)
                        height = kpx.x(40)
                        autoBg(R.mipmap.kera_error)
                        isAutoRight = true
                        isAutoCenterVertical = false
                        isAutoCenterHorizontal = false
                        isAutoCenter = false
                        onClickCallback {
                            getActivity()?.let {
                                if (it is KBaseActivity) {
                                    //fixme 是否删除该图片？
                                    it._dialogAlert(getString(R.string.kisDelimg))?.apply {
                                        positive {
                                            datas?.remove(mPath)//fixme 移除
                                            notifyDataSetChanged()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        if (isUpload) {
            //上传图片
            datas?.let {
                return it.size + 1
            }
            return 1
        }
        return datas?.size ?: 1
    }
}