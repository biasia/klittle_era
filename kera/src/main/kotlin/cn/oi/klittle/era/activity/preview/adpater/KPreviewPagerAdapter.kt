package cn.oi.klittle.era.activity.preview.adpater

//import org.jetbrains.anko.sdk25.coroutines.onClick
import android.app.Activity
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.VideoView
import androidx.viewpager.widget.PagerAdapter
import cn.oi.klittle.era.R
import cn.oi.klittle.era.activity.photo.entity.KLocalMedia
import cn.oi.klittle.era.activity.preview.KPreviewActivity
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.utils.KAssetsUtils
import cn.oi.klittle.era.utils.KGlideUtils
import cn.oi.klittle.era.utils.KLoggerUtils
import cn.oi.klittle.era.utils.KRegexUtils
import cn.oi.klittle.era.widget.compat.KView
import cn.oi.klittle.era.widget.photo.KPhotoView
import cn.oi.klittle.era.widget.video.KVideoView
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick

open class KPreviewPagerAdapter(var datas: MutableList<KLocalMedia>?) : PagerAdapter() {
    var keys = mutableMapOf<Int, String>()
    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view === obj//只有返回true时。才会显示视图
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        //super.destroyItem(container, position, obj)
        container?.removeView(obj as View)
        KAssetsUtils.getInstance().recycleBitmap(keys.get(position))//fixme 位图释放
        //KLoggerUtils.e("位图释放：\t"+keys.get(position)+"\t"+container?.context)
        videoMap?.get(position)?.let {
            it.suspend()//释放
        }
        videoMap?.remove(position)
    }

    //在addOnPageChangeListener{}里面进行了暂停操作。
    var videoMap: MutableMap<Int, VideoView?>? = mutableMapOf()//缓存视频控件

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        var itemView = container.context.UI {
            verticalLayout {
                gravity = Gravity.CENTER
                backgroundColor = Color.BLACK
                datas?.let {
                    if (it.size > position) {
                        datas?.get(position)?.let {
                            if (it.isVideo() || it.isAudio()) {
                                relativeLayout {
                                    //视频音频
                                    var video = KVideoView(this).apply {
                                        var path = it.path
                                        if (it.isCompressed && it.compressPath != null) {
                                            path = it.compressPath
                                        }
                                        prepare(path)
                                    }.lparams {
                                        width = matchParent
                                        height = matchParent
                                        centerInParent()//居中
                                    }
                                    videoMap?.put(position, video)
                                    var kView = KView(ctx).apply {
                                        autoBg {
                                            isCenterCrop = true
                                            width = kpx.x(105)
                                            height = kpx.x(100)
                                            autoBg(R.mipmap.kera_play2)
                                        }
                                        video?.apply {
                                            //播放完成（画面会停留在最后一帧）
                                            setOnCompletionListener {
                                                //播放完成
                                                autoBg {
                                                    isDraw = true
                                                }
                                            }
                                            setOnErrorListener { mp, what, extra ->
                                                //播放错误
                                                autoBg {
                                                    isDraw = true
                                                }
                                                true
                                            }
                                            onStart {
                                                autoBg {
                                                    isDraw = false
                                                }
                                            }
                                            onPause {
                                                autoBg {
                                                    isDraw = true
                                                }
                                            }
                                        }
                                        onClick {
                                            video?.toggle()
                                        }
                                        onDoubleTap {
                                            video?.pause()//暂停
                                            ctx?.let {
                                                if (it is Activity) {
                                                    it.finish()//双击关闭
                                                }
                                            }
                                        }
                                    }.lparams {
                                        width = matchParent
                                        height = matchParent
                                        centerInParent()
                                    }
                                    addView(kView)
                                }.lparams {
                                    width = matchParent
                                    height = matchParent
                                }
                            } else {
                                //图片
                                //PhotoView是第三方PictureSelector图片选择器里的控件
                                var photoView = KPhotoView(ctx).apply {
                                    scaleType = ImageView.ScaleType.FIT_CENTER
                                    datas?.let {
                                        if (it.size > position) {
                                            var overrideWidth = 480
                                            var overrideHeight = 800
//                                            var overrideWidth=720
//                                            var overrideHeight=1280
                                            datas?.get(position)?.let {
                                                //KLoggerUtils.e("path:\t" + it.path + "\tcompressPath:\t" + it.compressPath+"\tisGif:\t"+it.isGif())
                                                if (it.isGif()) {
                                                    //fixme Gif动态图片;(gif不能压缩，压缩之后就没有反应了。)
                                                    KGlideUtils.setGif(it.path, overrideWidth, overrideHeight, this)
                                                } else {
                                                    var path = it.path
                                                    if (it.isCompressed && it.compressPath != null) {
                                                        path = it.compressPath
                                                    }
                                                    //KLoggerUtils.e("path:\t"+it.path+"\tcompressPath:\t"+it.compressPath)
                                                    //KLoggerUtils.e("位图加载：\t"+path+"\t"+ctx)
                                                    if (path != null) {
                                                        if (KRegexUtils.isUrl(path)) {
                                                            //网络图片
                                                            KGlideUtils.getBitmapFromUrl(path, overrideWidth, overrideHeight, isCenterCrop = false) { key, bitmap ->
                                                                //KLoggerUtils.e("位图加载完成：\t"+key+"\t"+bitmap?.isRecycled)
                                                                ctx?.runOnUiThread {
                                                                    if (bitmap != null && !bitmap.isRecycled) {
                                                                        setImageBitmap(bitmap)
                                                                    }
                                                                }
                                                                keys?.put(position, key)//fixme 保存位图对应的键值key
                                                            }
                                                        } else {
                                                            //本地图片
                                                            KGlideUtils.getBitmapFromPath(path, overrideWidth, overrideHeight, isCenterCrop = false) { key, bitmap ->
                                                                //KLoggerUtils.e("位图加载完成：\t"+key+"\t"+bitmap?.isRecycled+"\twidth:\t"+bitmap?.width+"\theight:\t"+bitmap?.height+"\tposition:\t"+position)
                                                                ctx?.runOnUiThread {
                                                                    if (bitmap != null && !bitmap.isRecycled) {
                                                                        setImageBitmap(bitmap)
                                                                    }
                                                                }
                                                                keys?.put(position, key)//fixme 保存位图对应的键值key
                                                            }
                                                        }
                                                    } else {
                                                    }
                                                }
                                            }
                                            onClick {
                                                ctx?.let {
//                                                    if (it is Activity) {
//                                                        if (Build.VERSION.SDK_INT >= 28) {//28为android 9.0
//                                                            it.onBackPressed()//fixme 调用返回键关闭，效果比finish()好。
//                                                        } else {
//                                                            it.finish()//单击关闭
//                                                        }
//                                                    }
                                                    if (it is KPreviewActivity) {
                                                        it.ui?.toggleView()
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }.lparams {
                                    width = matchParent
                                    height = matchParent
                                    //leftMargin = kpx.x(4)
                                    //rightMargin = leftMargin
                                }
                                addView(photoView)
                            }
                        }
                    }
                }
            }
        }.view
        container.addView(itemView)//fixme 注意，必不可少。不然显示不出来。这里是itemView，不是view哦。之前就写错了，死活不出来
        return itemView
    }

    var mCount = datas?.size ?: 0
    override fun getCount(): Int {
        //不要return datas?.size?:0 这样很容易异常。如果getCount()每次返回的值不一样。会很容易异常崩溃的。
        //所以为了安全，最好将getCount()一开始就初始化固定。
        return mCount
    }

}
