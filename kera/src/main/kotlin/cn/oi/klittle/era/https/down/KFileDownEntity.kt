package cn.oi.klittle.era.https.down

import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseApplication
import cn.oi.klittle.era.base.KBaseUi
import cn.oi.klittle.era.comm.KToast
import cn.oi.klittle.era.utils.KAppUtils
import cn.oi.klittle.era.utils.KFileLoadUtils
import cn.oi.klittle.era.utils.KFileUtils
import java.io.File

//                        fixme 调用案例，放心KFileLoadUtils里面做了防重复下载。回调函数是实时最新的。可放心使用。亲测。
//                        var url = " http://test.bwg2017.com/UploadFile/AppVersion/202010201055245616.apk"
//                        KOhttp.downUrl(url).apply {
//                            onStart {
//                                KLoggerUtils.e("开始下载")
//                            }
//                            onLoad { current, max, bias ->
//                                KLoggerUtils.e("下载进度：\tcurrent:\t" + current + "\tmax:\t" + max + "\tbias:\t" + bias)
//                            }
//                            onFailure { isLoad, result, code, file ->
//                                KLoggerUtils.e("下载失败：\tisLoad:\t" + isLoad + "\tresult:\t" + result + "\tcode:\t" + code + "\tfile:\t" + file?.length())
//                            }
//                            onSuccess {
//                                KLoggerUtils.e("下载成功：\t" + it?.length() + "\t" + file?.absoluteFile)
//                            }
//                            get {
//                                //FIXME 不管成功还是失败，只要最后下载问不为空，就会回调（且最后回调）。
//                                //返回下载文件
//                                if (isAppComplete()) {
//                                    //apk安装包完整，进行安装
//                                    KAppUtils.installation(it)
//                                }
//                                destroy()//销毁（需要自己最后手动执行）。
//                            }
//                        }
//fixme 文件下载实体类。基本上只要指定url即可。其他参数会自动判断赋值。
open class KFileDownEntity {
    var url: String? = null//下载链接

    var downDir: String? = null//文件下载路径，可以为空，如果为空。会使用KFileLoadUtils的默认下载目录 cacheDir fixme 文件下载完成之后，会自动重新判断赋值。

    //fixme isApk未指定下载文件类型时，下载前，也会自动判断下载文件类型是否为apk
    var isApk: Boolean? = null//下载的是否为APK安装包。默认不是。fixme 文件下载完成之后，会自动重新判断赋值。
    var isAppComplete: Boolean? = null//判断下载的apk安装包是否完整。fixme 文件下载完成之后，会自动重新判断赋值。
    var fileName: String? = null//文件名，包括文件后缀。可以为空，如果为null或""空，会自动获取网络上的名称。fixme 文件下载完成之后，会自动重新判断赋值。
    var file: File? = null//fixme 下载文件，下载完成之后，会自动赋值。

    /**
     * fixme 判断下载的ApK文件是否完整。
     */
    fun isAppComplete(): Boolean {
        file?.let {
            if (it.length() > 0) {
                isApk?.let {
                    if (it) {
                        isAppComplete?.let {
                            if (it) {
                                return true
                            }
                        }
                    }
                }
            }
        }
        return false
    }

    //fixme 下载回调（下载成功，还是下载失败（重复下载）。只要下载文件File不为空，就会回调。成功失败都会回调。）；最后回调。
    private var callBack: ((file: File) -> Unit)? = null

    fun get(callBack: ((file: File) -> Unit)? = null) {
        this.callBack = callBack
        down(this)
    }

    //文件下载完成时，回调。
    private fun complete(file: File?) {
        this.file = file
        file?.let {
            if (it.length() > 0) {
                fileName = KFileUtils.getInstance().getFileName(it)//获取文件名，包括后缀名。
                fileName?.toLowerCase()?.trim()?.let {
                    if (it.contains(".apk")) {
                        isApk = true//是否为apk安装包
                        isAppComplete = KAppUtils.isAppComplete(KBaseApplication.getInstance(), file.absolutePath)//判断安装包是否完整。
                    } else {
                        isApk = false
                        isAppComplete = false
                    }
                    downDir = KFileUtils.getInstance().getFileDir(file.absolutePath)//下载文件所在目录地址。
                }
            } else {
                isApk = false
                isAppComplete = false
            }
        }
    }

    //fixme 开始回调
    private var start: (() -> Unit)? = null

    fun onStart(start: (() -> Unit)? = null): KFileDownEntity {
        this.start = start
        return this
    }

    //fixme 失败回调
    private var failure: ((isLoad: Boolean?, result: String?, code: Int, file: File?) -> Unit)? = null

    /**
     * 下载失败回调
     * @param isLoad true 文件已经下载（重复下载）。false没有下载
     * @param result 失败文本原因
     * @param code 失败代码
     * @param file 下载文件，isLoad为true时会返回。
     */
    fun onFailure(failure: ((isLoad: Boolean?, result: String?, code: Int, file: File?) -> Unit)? = null): KFileDownEntity {
        this.failure = failure
        return this
    }

    //fixme 成功回调
    private var success: ((file: File?) -> Unit)? = null

    fun onSuccess(success: ((file: File?) -> Unit)? = null): KFileDownEntity {
        this.success = success
        return this
    }

    //fixme 下载进度回调
    private var load: ((current: Long, max: Long, bias: Int) -> Unit)? = null

    /**
     * @param current 当前文件下载大小
     * @param max 文件总大小
     * @param bias 下载百分比（0~100）
     */
    fun onLoad(load: ((current: Long, max: Long, bias: Int) -> Unit)? = null): KFileDownEntity {
        this.load = load
        return this
    }

    //fixme 销毁，不会自动执行。需要自己手动销毁。
    //之所以要手动执行，是因为考虑，这些参数信息对调用者可能有用。
    fun destroy() {
        url = null
        downDir = null
        isApk = null
        isAppComplete = null
        fileName = null
        file = null
        start = null
        failure = null
        success = null
        load = null
        callBack = null
    }

    companion object {
        private fun down(entity: KFileDownEntity) {
            //fixme kotlin使用案例; 下载url必须为具体的文件地址。不然无法下载；像 http://test.bwg2017.com/GlassSystem/ErpConfigManage.aspx 这样的就无法下载。
            //var url = "http://dl.ludashi.com/ludashi/ludashi_home.apk"//鲁大师app；指向具体的apk网络文件路径即可。亲测可行。
            entity?.apply {
                if (url != null) {
                    url?.let {
                        if (isApk == null) {
                            //fixme isApk未指定下载文件类型时，自动判断下载文件类型是否为apk
                            KFileUtils.getInstance().getFileName(it)?.toLowerCase()?.trim()?.let {
                                if (it.contains(".apk")) {
                                    isApk = true
                                    isAppComplete = false
                                } else {
                                    isApk = false
                                    isAppComplete = false
                                }
                            }
                        }
                    }
                    KFileLoadUtils.getInstance(isApk).downLoad(KBaseApplication.getInstance(), url, downDir, fileName, object : KFileLoadUtils.RequestCallBack {
                        override fun onStart() {
                            //开始下载
                            //KLoggerUtils.e("开始下载")
                            start?.let {
                                it()
                            }
                        }

                        override fun onFailure(isLoad: Boolean?, result: String?, code: Int, file: File?) {
                            //下载失败（关闭弹窗）
//                            if (isLoad!!) {
//                                //已经下载(进行安装)
//                                file?.let {
//                                    KAppUtils.installation(ctx, file)
//                                }
//                            } else {
//                                KToast.showError("下载失败")
//                                KLoggerUtils.e("下载失败")
//                            }
                            isLoad?.let {
                                if (it) {
                                    complete(file)//下载完成调用一下
                                }
                            }
                            failure?.let {
                                it(isLoad, result, code, file)
                            }
                            //失败也会回调
                            file?.let {
                                if (it.length() > 0) {
                                    callBack?.let {
                                        it(file)
                                    }
                                }
                            }
                        }

                        override fun onSuccess(file: File?) {
                            //KLoggerUtils.e("下载成功：\t" + file?.path)
                            //下载完成安装（关闭弹窗）
                            //KAppUtils.installation(ctx, file)
                            complete(file)//下载完成调用一下
                            success?.let {
                                it(file)
                            }
                            file?.let {
                                if (it.length() > 0) {
                                    callBack?.let {
                                        it(file)
                                    }
                                }
                            }
                        }

                        override fun onLoad(current: Long, max: Long, bias: Int) {
                            //下载进度
                            //KLoggerUtils.e("下载进度：\t" + bias)
                            load?.let {
                                it(current, max, bias)
                            }
                        }
                    })
                } else {
                    KToast.showError(KBaseUi.getString(R.string.urlEnpty))//错误提示：文件下载地址为空
                }
            }
        }
    }
}