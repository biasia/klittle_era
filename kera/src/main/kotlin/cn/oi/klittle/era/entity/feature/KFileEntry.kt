package cn.oi.klittle.era.entity.feature

import android.app.Activity
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseActivity
import cn.oi.klittle.era.base.KBaseActivityManager
import cn.oi.klittle.era.base.KBaseEntity
import cn.oi.klittle.era.base.KBaseUi
import cn.oi.klittle.era.comm.KToast
import cn.oi.klittle.era.dialog.progressbar.KDownProgressbarDialog
import cn.oi.klittle.era.dialog.progressbar.KProgressDialog
import cn.oi.klittle.era.utils.*
import java.io.File
import java.lang.Exception

//                    var fileEntry=KFileEntry(url)//传入网络下载地址即可
//                    fileEntry.goOpenFile()//打开文件
/**
 * 文件下载管理实体类;支持，txt文本，图片，视频，apk安装包等。亲测有效。
 * fixme 一个KFileEntry（）就下载一个url;即：url值一定设置就不要再改了。如果要改url;建议重新实例化一个KFileEntry（）；
 * fixme 即：一个KFileEntry（）最好就对应一个url。（这样不会出现下载错乱的问题）
 */
open class KFileEntry : KBaseEntity {

    private var isSilentDownload:Boolean=false;//fixme 是否初始化的时候，静默下载。默认不下载。
    var url: String? = null
        //文件下载地址
        set(value) {
            field = value
            //fixme isSilentDownload 静默下载。
            if (isSilentDownload&&value != null) {
                if (KRegexUtils.isUrl(value)) {
                    if (value != null && !value.equals(pUrl)) {//与上次url做一下判断，预防重复下载。
                        down(downCallBack)//fixme 静默下载
                    }
                } else {
                    file?.let {
                        if (it.absolutePath.equals(value)) {
                            return
                        }
                    }
                    success(File(value))
                }
            }
        }
    var file: File? = null//下载文件
    var charsetName: String? = "GB2312"//文本编码格式: "GB2312"、"UTF-8";不然会乱码。txt文本格式一般都是 GB2312格式。
    var isSharedable: Boolean = true//是否具备分享能力，主要争对文本和图片。

    var isPrintDownLog: Boolean = false//是否打印下载进度log日志。

    /**
     *  @param isSilentDownload是否初始化的时候，静默下载。默认不下载。
     */
    constructor(url: String? = null,isSilentDownload:Boolean=false) {
        this.url = url
        this.isSilentDownload=isSilentDownload;
    }

    //判断文件本地是否已经存在。
    open fun exists(): Boolean {
        file?.let {
            if (it.exists() && it.length() > 0) {
                var f = it
                fileLength?.let {
                    if (f.length() == it) {
                        return true
                    }
                }
            }
        }
        return false
    }

    /**
     * 打开文件
     * @param callback 打开成功回调。
     */
    open fun goOpenFile(callback: (() -> Unit)? = null) {
        if (url != null) {
            if (exists()) {
                KIntentUtils.goOpenFile(file = file, fileName = fileName, charsetName = charsetName, isSharedable = isSharedable)
                callback?.let { it() }
            } else {
                if (KRegexUtils.isUrl(url)) {
                    //文件不存在，再自动下载一次。
                    showProgressbar()//开启进度条
                    if (exists()) {//再判断一次。fixme 亲测，预防万一，必须再判断一次。
                        dissmissProgressbar()
                        KIntentUtils.goOpenFile(file = file, fileName = fileName, charsetName = charsetName, isSharedable = isSharedable)
                        callback?.let { it() }
                    } else {
                        down() {
                            dissmissProgressbar()//关闭进度条
                            file?.let {
                                if (it.length() > 0) {
                                    //fixme 文件下载完成，再自动打开。downCallBack(null)下载回调置顶，即可取消自动播放。
                                    KIntentUtils.goOpenFile(file = file, fileName = fileName, charsetName = charsetName, isSharedable = isSharedable)
                                    callback?.let { it() }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //分享文件
    open fun goSharedFile() {
        if (url != null) {
            if (exists()) {
                KSharedUtils.shareFile(file)
            } else {
                //文件不存在，再自动下载一次。
                showProgressbar()//开启进度条
                if (exists()) {//再判断一次。fixme 亲测，预防万一，必须再判断一次。
                    dissmissProgressbar()
                    KSharedUtils.shareFile(file)
                } else {
                    down() {
                        dissmissProgressbar()//关闭进度条
                        file?.let {
                            if (it.length() > 0) {
                                //fixme 文件下载完成，再自动打开。downCallBack(null)下载回调置顶，即可取消自动播放。
                                KSharedUtils.shareFile(file)
                            }
                        }
                    }
                }
            }
        }
    }

    //保存文件到本地SD卡相册里。
    open fun saveFileToDCIM() {
        KFileUtils.getInstance().saveFileToDCIM(filePath)
    }

    open fun getActivity(): Activity? {
        return KBaseActivityManager.getInstance().stackTopActivity
    }

    open fun getString(id: Int, formatArgs: String?): String {
        try {
            if (formatArgs != null) {
                //fixme 不要去除空格；空格也占位，有时也需要也很重要的。
                return KBaseUi.getContext().resources.getString(id, formatArgs) as String
            }
            return KBaseUi.getContext().getString(id) as String
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    open fun getString(id: Int): String {
        return KBaseUi.getString(id, null)
    }

    //public open var progressbar: KProgressDialog? = null
    public open var progressbar: KDownProgressbarDialog? = null
    public open var isStatus: Boolean = true
    public open var isDark: Boolean? = null

    //显示进度条
    open fun showProgressbar() {
        //KLoggerUtils.e("显示进度条1：\t" + getActivity())
        getActivity()?.let {
            var act = it
            it.runOnUiThread {
                if (progressbar == null) {
                    progressbar = KDownProgressbarDialog(act, isStatus)
                }
                if (isDark == null) {
                    if (it is KBaseActivity) {
                        progressbar?.isDark(it.isDark())
                    }
                } else {
                    progressbar?.isDark(isDark)
                }
                //不屏蔽返回键，返回键可以取消。
                progressbar?.isLocked(false) {
                    downCallBack(null)//下载完成回调置空。fixme 即：按下返回键时，取消自动播放。
                }
                progressbar?.show()
            }
        }
        //KLoggerUtils.e("显示进度条2：\t" + progressbar?.isShow())
    }

    //关闭进度条
    open fun dissmissProgressbar() {
        progressbar?.dismiss()
        //KLoggerUtils.e("关闭进度条：\t" + progressbar?.isShow())
    }

    //下载成功回调
    open fun success(file: File?) {
        //KLoggerUtils.e("下载成功：\t" + file?.absolutePath)
        this.file = file
        file?.let {
            if (it.length() > 0) {
                fileLength = it.length()
            }
            filePath = it.absolutePath
            if (fileName == null) {
                //fixme (为空的时候，才赋值，不为空就以手动赋值的为主。)
                fileName = it.name//包括文件名后缀
            }
            downCallBack?.let { it() }//fixme 下载成功回调
            downCallBack = null
            fileSuffix = KFileUtils.getInstance().getFileSuffix(it, false)
        }
    }

    //下载失败回调
    open fun failure(result: String?, code: Int) {
        //下载失败：result 错误代码：code
        KToast.showError(getString(R.string.kappdownfail) + ":\t" + result + "\n" + getString(R.string.kerror_code) + "\t" + code)
    }

    //下载进度回调
    open fun load(current: Long, max: Long, bias: Int) {
        progressbar?.setProgressbar(bias)//显示下载进度。
        //KLoggerUtils.e("下载进度：\t"+progressbar+"\t"+"\t"+progressbar?.numberProgressBar+"\t"+progressbar?.numberProgressBar?.progress)
    }

    var downDir = KPathManagerUtils.getFileLoadDownPath()//文件下载目录

    //fixme 以下属性：文件下载完成之后，会自动赋值。
    var fileName: String? = null//文件名，包含后缀名。(为空的时候，会获取网络名称，不为空就以手动赋值的为主。)
    var fileSuffix: String? = null//文件名后缀，如：txt
    var filePath: String? = null//文件地址
    var fileLength: Long? = null//文件长度
    var downCallBack: (() -> Unit)? = null//下载成功回调
    open fun downCallBack(downCallBack: (() -> Unit)? = null) {
        this.downCallBack = downCallBack
    }

    private var pUrl: String? = null//记录上一次的下载url

    //下载
    open fun down(downCallBack: (() -> Unit)? = this.downCallBack) {
        if (url == null) {
            return
        }
        if (!KRegexUtils.isUrl(url)) {
            return
        }
        //KLoggerUtils.e("url:\t" + url + "\tpUrl:\t" + pUrl)
        var isSameUrl = url.equals(pUrl)//判断与上次下载的url是否相同。
        if (!exists() || !isSameUrl) {
            if (file != null && fileName != null && !isSameUrl) {
                fileName = null//fixme url改变时，文件名要重新置空。防止下载的文件又是一样的。
            }
            fileName?.let {
                if (!it.contains(".")) {
                    fileName = null//fixme 没有后缀名，不合法。置空。
                }
            }
            //KLoggerUtils.e("进来了\t" + fileName)
            pUrl = url//记得当前下载url
            downCallBack(downCallBack)
            KFileLoadUtils.getInstance(true).downLoad(getActivity(), url, downDir, fileName, object : KFileLoadUtils.RequestCallBack {
                override fun onStart() {
                    //开始下载
                    load(0, 0, 0)
                }

                override fun onFailure(isLoad: Boolean?, result: String?, code: Int, file: File?) {
                    //下载失败
                    dissmissProgressbar()//fixme 关闭进度条
                    if (isLoad!!) {
                        //下载失败（文件已经下载）
                        success(file)
                    } else {
                        failure(result, code)
                    }
                }

                override fun onSuccess(file: File?) {
                    //下载成功
                    dissmissProgressbar()//fixme 关闭进度条
                    success(file)
                }

                override fun onLoad(current: Long, max: Long, bias: Int) {
                    //下载进度
                    load(current, max, bias)
                    if (isPrintDownLog) {
                        KLoggerUtils.e("当前下载大小:\t" + KStringUtils.getDataSize(current) + "\t总大小：\t" + KStringUtils.getDataSize(max) + "\t进度:\t" + bias + "%", isLogEnable = true)
                    }
                }
            })
        }
    }

    //销毁
    open fun destroy() {
        url = null
        file = null
        fileLength = null
        fileName = null
        filePath = null
        fileSuffix = null
        charsetName = null
        downCallBack = null
        dissmissProgressbar()
        progressbar?.onDestroy()
        progressbar = null
    }
}