package cn.oi.klittle.era.widget.compat.layout.relativeLayout

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Build
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import cn.oi.klittle.era.exception.KCatchException
import cn.oi.klittle.era.utils.KLoggerUtils
import cn.oi.klittle.era.widget.compat.K7RadiusWidget
import cn.oi.klittle.era.widget.compat.layout.linearLayout.KVerticalLayout
import org.jetbrains.anko.backgroundDrawable
import java.lang.Exception

//            fixme 使用案例：_KRelativeLayout子View能够识别 .lparams{}；可以直接正常使用。
//
//                krelativeLayout {
//                    var mDirection = KBubblesEntry.DIRECTION_LEFT//气泡方向
//                    var mMargin = kpx.x(30)//radius外补丁
//                    var norColor = Color.parseColor("#F5F6FA")//常态颜色
//                    var pressColor = Color.parseColor("#C4C5C8")//按下颜色
//                    bubbles {
//                        direction = mDirection//fixme 气泡方向；如果是左方向，默认就是左垂直居中绘制。即：默认居中绘制。
//                        bubblesWidth = kpx.x(20)
//                        bubblesHeight = kpx.x(30)
//                        bg_color = norColor
//                        if (direction == KBubblesEntry.DIRECTION_LEFT || direction == KBubblesEntry.DIRECTION_RIGHT) {
//                            xOffset = kpx.x(11f)//气泡的偏移量（整体偏移）；正数向右偏移，负数向左偏移(fixme 即：正数向内偏移，负数向外偏移。)
//                        } else {
//                            yOffset = kpx.x(4f)
//                        }
//                        all_radius = 45f//气泡顶点的圆角角度（只对顶点有效）
//                        bubblesOffset = -kpx.x(20f)//气泡顶点的偏移量
//                    }
//                    bubbles_press {
//                        bg_color = pressColor
//                        all_radius = 0f
//                        bubblesOffset = 0f
//                        strokeColor = Color.RED
//                        strokeWidth = kpx.x(3f)
//                        isDrawRightStroke = false//fixme 是否绘制右边的边框（气泡顶点对应的底边）;默认不绘制。方便和radius组合对接。
//                        if (direction == KBubblesEntry.DIRECTION_LEFT || direction == KBubblesEntry.DIRECTION_RIGHT) {
//                            xOffset = kpx.x(14f)
//                        } else {
//                            yOffset = kpx.x(4f)
//                        }
//                        dashWidth = kpx.x(15f)
//                        dashGap = kpx.x(10f)
//                    }
//                    radius {
//                        all_radius(45)
//                        bg_color = norColor
//                        strokeWidth = 0f
//                        if (mDirection == KBubblesEntry.DIRECTION_LEFT) {
//                            leftMargin = mMargin
//                        } else if (mDirection == KBubblesEntry.DIRECTION_RIGHT) {
//                            rightMargin = mMargin
//                        } else if (mDirection == KBubblesEntry.DIRECTION_TOP) {
//                            topMargin = mMargin
//                        } else if (mDirection == KBubblesEntry.DIRECTION_BOTTOM) {
//                            bottomMargin = mMargin
//                        }
//                        setAutoPaddingForRadius(kpx.x(16), this)//fixme 根据radius的外补丁，自动设置文本内补丁。
//                    }
//                    radius_press {
//                        bg_color = pressColor
//                        strokeColor = Color.RED
//                        strokeWidth = kpx.x(3f)
//                        dashWidth = kpx.x(15f)
//                        dashGap = kpx.x(10f)
//                    }
//                    //fixme 子View
//                    kview {
//                        text = "我的名字叫诺亚方舟。哦"
//                        gravity = Gravity.CENTER_VERTICAL
//                        var mDirection = KBubblesEntry.DIRECTION_RIGHT//气泡方向
//                        var mMargin = kpx.x(30)//radius外补丁
//                        var norColor = Color.parseColor("#00CAFC")//常态颜色
//                        var pressColor = Color.parseColor("#00A2CA")//按下颜色
//                        bubbles {
//                            direction = mDirection//fixme 气泡方向；如果是左方向，默认就是左垂直居中绘制。即：默认居中绘制。
//                            bubblesWidth = kpx.x(20)
//                            bubblesHeight = kpx.x(30)
//                            bg_color = norColor
//                            if (direction == KBubblesEntry.DIRECTION_LEFT || direction == KBubblesEntry.DIRECTION_RIGHT) {
//                                xOffset = kpx.x(11f)//气泡的偏移量（整体偏移）；正数向右偏移，负数向左偏移(fixme 即：正数向内偏移，负数向外偏移。)
//                            } else {
//                                yOffset = kpx.x(4f)
//                            }
//                            all_radius = 45f//气泡顶点的圆角角度（只对顶点有效）
//                            bubblesOffset = -kpx.x(20f)//气泡顶点的偏移量
//                        }
//                        bubbles_press {
//                            bg_color = pressColor
//                            all_radius = 0f
//                            bubblesOffset = 0f
//                            strokeColor = Color.RED
//                            strokeWidth = kpx.x(3f)
//                            isDrawRightStroke = false//fixme 是否绘制右边的边框（气泡顶点对应的底边）;默认不绘制。方便和radius组合对接。
//                            if (direction == KBubblesEntry.DIRECTION_LEFT || direction == KBubblesEntry.DIRECTION_RIGHT) {
//                                xOffset = kpx.x(14f)
//                            } else {
//                                yOffset = kpx.x(4f)
//                            }
//                            dashWidth = kpx.x(15f)
//                            dashGap = kpx.x(10f)
//                        }
//                        radius {
//                            all_radius(45)
//                            bg_color = norColor
//                            strokeWidth = 0f
//                            if (mDirection == KBubblesEntry.DIRECTION_LEFT) {
//                                leftMargin = mMargin
//                            } else if (mDirection == KBubblesEntry.DIRECTION_RIGHT) {
//                                rightMargin = mMargin
//                            } else if (mDirection == KBubblesEntry.DIRECTION_TOP) {
//                                topMargin = mMargin
//                            } else if (mDirection == KBubblesEntry.DIRECTION_BOTTOM) {
//                                bottomMargin = mMargin
//                            }
//                            setAutoPaddingForRadius(kpx.x(16), this)//fixme 根据radius的外补丁，自动设置文本内补丁。
//                        }
//                        radius_press {
//                            bg_color = pressColor
//                            strokeColor = Color.RED
//                            strokeWidth = kpx.x(3f)
//                            dashWidth = kpx.x(15f)
//                            dashGap = kpx.x(10f)
//                        }
//                    }.lparams {
//                        width = kpx.x(400)
//                        height = kpx.x(100)
//                    }
//                }.lparams {
//                    width = matchParent
//                    height = wrapContent
//                    topMargin = kpx.x(8)
//                }
open class KRelativeLayout : KPathView {

    constructor(viewGroup: ViewGroup) : super(viewGroup.context) {
        viewGroup.addView(this)//直接添加进去,省去addView(view)
    }

    constructor(context: Context) : super(context) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {}

    //fixme 兼容之前的版本。返回自己本身。
    open fun inner(block: KRelativeLayout.() -> Unit): KRelativeLayout {
        block(this)
        return this
    }

}