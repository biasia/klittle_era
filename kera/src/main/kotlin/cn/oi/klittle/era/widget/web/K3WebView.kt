package cn.oi.klittle.era.widget.web

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup

//                    调用案例：
//                    kWebView {
//                        radius {
//                            leftMargin=kpx.x(16)
//                            rightMargin=leftMargin
//                            all_radius=kpx.x(100f)
//                            bg_color=Color.WHITE
//                            strokeColor=Color.BLUE
//                            strokeWidth=kpx.x(5f)
//                        }
//                        var url="http://test.jh.bwg2017.com/Index_ws.aspx?L=&ProcessId=&Line="
//                        loadUrl(url){
//                            KLoggerUtils.e("加载进度：\t"+it)
//                        }
//                    }.lparams {
//                        width = matchParent
//                        height = kpx.x(600)
//                    }
/**
 * 自定义WebView
 */
open class K3WebView : K2MyWebView {
    constructor(viewGroup: ViewGroup) : super(viewGroup.context) {
        viewGroup.addView(this)//直接添加进去,省去addView(view)
    }

    constructor(context: Context) : super(context) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {}


    //返回上一页
    override fun goBack() {
        super.goBack()
    }

}