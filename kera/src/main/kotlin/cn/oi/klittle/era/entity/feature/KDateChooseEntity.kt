package cn.oi.klittle.era.entity.feature

import cn.oi.klittle.era.utils.KCalendarUtils
import cn.oi.klittle.era.utils.KLoggerUtils
import cn.oi.klittle.era.utils.KStringUtils

/**
 * 日期选择数据（保存了，年，月，日，及其下标）;
 * fixme 日期格式最标准的是："yyyy-MM-dd HH:mm:ss.SSS" 年-月-日 时：分：秒.毫秒
 * Created by 彭治铭 on 2018/6/3.
 */
open class KDateChooseEntity {

    var yyyy: String?//年
    var MM: String?//月

    var dd: String?//日

    var HH: String?//时
    var mm: String?//分

    var ss: String?//秒
    var SSS: String?//毫秒

    //设置日期。精确年月日
    fun setTime(time: String?, format: String = "yyyy-MM-dd") {
        time?.let {
            KCalendarUtils.getCalendar(time, format)?.let {
                yyyy = KCalendarUtils.getYear(it).toString()
                MM = KCalendarUtils.getMonth(it).toString()
                dd = KCalendarUtils.getDate(it).toString()
                HH = KCalendarUtils.getHourOfDay(it).toString()//获取时间--小时（24小时制）
                mm = KCalendarUtils.getMinute(it).toString()//分钟
                ss = KCalendarUtils.getSecond(it).toString()//秒
                SSS = KCalendarUtils.getMilliSecond(it).toString()//毫秒
            }
        }
    }

    //设置日期。精确分。
    fun setTimeFormm(time: String?, format: String = "yyyy-MM-dd HH:mm") {
        setTime(time, format)
    }

    //设置日期。精确秒。
    fun setTimeForss(time: String?, format: String = "yyyy-MM-dd HH:mm:ss") {
        setTime(time, format)
    }

    //设置日期。精确毫秒
    fun setTimeForSSS(time: String?, format: String = "yyyy-MM-dd HH:mm:ss.SSS") {
        setTime(time, format)
    }

    init {
        //初始化，年月日，以当前时间为标准
        yyyy = KCalendarUtils.getYear().toString()
        MM = KCalendarUtils.getMonth().toString()
        dd = KCalendarUtils.getDate().toString()
        HH = KCalendarUtils.getHourOfDay().toString()//获取时间--小时（24小时制）
        mm = KCalendarUtils.getMinute().toString()//分钟
        ss = KCalendarUtils.getSecond().toString()//秒
        SSS = KCalendarUtils.getMilliSecond().toString()//毫秒
    }

    //默认格式 yyyy-MM-dd 年月日
    open override fun toString(): String {
        try {
            if (MM != null && dd != null) {
                var M = MM
                if (M!!.length < 2) {
                    M = "0" + M
                }
                var d = dd
                if (d!!.length < 2) {
                    d = "0" + d
                }

                return yyyy + "-" + KStringUtils.addZero2(M) + "-" + KStringUtils.addZero2(d)//返回年月日，fixme 格式：yyyy-MM-dd
            }
            yyyy?.let {
                return it//返回年
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    //FIXME 精确到分，格式 yyyy-MM-dd HH:mm
    open fun toStringFormm(): String {
        try {
            return toString() + " " + KStringUtils.addZero2(HH) + ":" + KStringUtils.addZero2(mm)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    //FIXME 精确到秒，格式 yyyy-MM-dd HH:mm:ss
    open fun toStringForss(): String {
        try {
            return toString() + " " + KStringUtils.addZero2(HH) + ":" + KStringUtils.addZero2(mm) + ":" + KStringUtils.addZero2(ss)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    //fixme 时分秒都作00处理。
    open fun toStringForssWith00(): String {
        try {
            return toString() + " 00:00:00"
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    //FIXME 精确到毫秒，格式 yyyy-MM-dd HH:mm:ss.SSS
    open fun toStringForSSS(): String {
        try {
            return toString() + " " + KStringUtils.addZero2(HH) + ":" + KStringUtils.addZero2(mm) + ":" + KStringUtils.addZero2(ss) + "." + KStringUtils.addZero3(SSS)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

    //fixme 时分秒毫秒都作00处理。
    open fun toStringForSSSWith00(): String {
        try {
            return toString() + " 00:00:00.000"
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ""
    }

}