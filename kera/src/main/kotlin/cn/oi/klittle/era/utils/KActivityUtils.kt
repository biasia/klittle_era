package cn.oi.klittle.era.utils

import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import cn.oi.klittle.era.base.KBaseActivityManager
import cn.oi.klittle.era.base.KBaseApplication
import cn.oi.klittle.era.exception.KCatchException

/**
 * fixme Activity工具类
 */
object KActivityUtils {

    fun getContext(): Context {
        return KBaseApplication.getInstance()
    }

    fun getActivity(): Activity? {
        return KBaseActivityManager.getInstance().stackTopActivity
    }

    /**
     * 默认就从Res目录下读取
     * 获取String文件里的字符,<string name="names">你好%s</string>//%s 是占位符,位置随意
     * @param formatArgs 是占位符
     */
    open fun getString(id: Int, formatArgs: String? = null): String {
        if (formatArgs != null) {
            return getContext().resources.getString(id, formatArgs) as String
        }
        return getContext().getString(id) as String
    }

    var packageInfo: PackageInfo? = null;

    //获取当前应用的所有Activity（只能获取本应用的Activity,不需要权限。）
    fun getAppAllActivity(): PackageInfo? {
        if (packageInfo == null || packageInfo?.activities == null) {
            getActivity()?.let {
                var packageManager = it.getPackageManager();
                try {
                    packageInfo = packageManager.getPackageInfo(
                            it.getPackageName(), PackageManager.GET_ACTIVITIES
                    );
                } catch (e: Exception) {
                    KLoggerUtils.e(
                            "获取所有Activity异常：\t" + KCatchException.getExceptionMsg(e),
                            isLogEnable = true
                    )
                }
            }
        } else {
            packageInfo?.activities?.let {
                if (it.size <= 0) {
                    packageInfo = null
                }
            }
        }
        return packageInfo
    }

    /**
     * @param activityName 要查找的指定Activity名称。不区分大小分。支持模仿匹配。
     * @return 返回指定Activity的完整名称（全限名。）
     */
    fun getAppActivity(activityName: String?): String? {
        try {
            activityName?.let {
                var name = it.toLowerCase().trim()
                //fixme 优先完全匹配。
                getAppAllActivity()?.activities?.forEach {
                    var aName = it.name.substring(
                            it.name.lastIndexOf(
                                    "."
                            ) + 1
                    ).toLowerCase().trim()
                    //KLoggerUtils.e("aName:\t"+aName+"\tname:\t"+name)
                    if (aName.equals(name)) {
                        return it.name//返回Activity的完整名称（全限名）。
                    }
                }
                //fixme 完全匹配不上，再模糊匹配。
                getAppAllActivity()?.activities?.forEach {
                    var aName = it.name.toLowerCase().trim()
                    if (aName.contains(name)) {
                        return it.name//返回Activity的完整名称（全限名）。
                    }
                }
            }
        } catch (e: java.lang.Exception) {
            KLoggerUtils.e(
                    "获取指定Activity异常：\t" + KCatchException.getExceptionMsg(e),
                    isLogEnable = true
            )
        }
        return null
    }

}