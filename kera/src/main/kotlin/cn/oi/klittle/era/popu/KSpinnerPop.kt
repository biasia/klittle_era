package cn.oi.klittle.era.popu

//import org.jetbrains.anko.sdk25.coroutines.onClick
import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.View.OVER_SCROLL_NEVER
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.recyclerview.widget.RecyclerView
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseUi
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.utils.KPopuWindowUtils
import cn.oi.klittle.era.widget.compat.layout.relativeLayout.KRelativeLayout
import cn.oi.klittle.era.widget.recycler.KRecyclerView
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick

//fixme 如果视图不显示，基本就两个原因： 1：没有数据 2：上下文错误，将context改成ctx试试。

//fixme 最新完整调用案例，带气泡。

//    var sp: KSpinnerPop? = null
//    var datas: MutableList<String>? = null//数据
//    fun showPop(view: View) {
//        //数据
//        //fixme 记得添加数据，不然不会显示哦。
//        datas?.let {
//            if (it.size > 20) {
//                it.clear()//数据清除。
//            }
//            //每次新增五条数据
//            for (i in it.size..it.size + 4) {
//                datas?.add(i.toString() + "-" + KStringUtils.getRandom(5))
//            }
//        }
//        //初始化数据
//        if (datas == null) {
//            datas = mutableListOf()
//            for (i in 0..3) {
//                datas?.add(i.toString() + "-" + KStringUtils.getRandom(5))
//            }
//        }
//        var isAsDropDown = true//fixme 是否显示在指定控件的下方。true(显示在下方) 是；false 不是（显示在上方）
//        var popupWidth = kpx.screenWidth() / 2//popuwidnow的显示宽度。
//        var popuHeight = ViewGroup.LayoutParams.WRAP_CONTENT//popuwindow显示在控件下方，默认最好设置成高度自适应。下方显示，高度能够自适应。
//        if (!isAsDropDown) {
//            //fixme 显示在上方，popuwindow最好指明具体的高度。上方高度无法自适应。
//            popuHeight = kpx.x(100) * 4
//        }
//        var norColor = Color.WHITE//正常色
//        var pressColor = Color.parseColor("#EFF3F6")//按下颜色
//        if (sp == null || sp!!.pop == null) {
//            sp = KSpinnerPop(ctx, datas!!)
//            //创建recyclerView内部item视图,参数为下标position
//            sp?.onCreateView(popupWidth = popupWidth, popuHeight = popuHeight,isAddNavigation = false) {
//                ctx.UI {
//                    verticalLayout {
//                        backgroundColor = Color.WHITE
//                        //要设置具体的宽度kpx.screenWidth();不要使用wrapContent和matchParent
//                        var layoutParams = ViewGroup.LayoutParams(popupWidth, kpx.x(100))
//                        setLayoutParams(layoutParams)
//                        gravity = Gravity.CENTER
//                        KBaseUi.apply {
//                            ktextView {
//                                id = kpx.id("pop_txt")
//                                gravity = Gravity.CENTER
//                                textSize = kpx.textSizeX(30f)
//                                textColor = Color.BLACK
//                                KSelectorUtils.selectorRippleDrawable(this, norColor, pressColor)
//                            }.lparams {
//                                width = matchParent
//                                height = kpx.x(99)
//                            }
//                        }
//                        view {
//                            backgroundColor = Color.LTGRAY//分界线
//                        }.lparams {
//                            width = matchParent
//                            height = kpx.x(1)
//                        }
//                    }
//                }.view //注意最好手动配置item的宽度和高度。
//            }
//            //fixme sp?.onCreateView()调用之后，sp?.containerLayout容器才会有值。
//            sp?.containerLayout?.apply {
//                radius {
//                    all_radius = kpx.x(90f)
//                    bg_color = norColor
//                    if (isAsDropDown) {
//                        topMargin = kpx.x(30)
//                    } else {
//                        bottomMargin = kpx.x(30)
//                    }
//                }
//                if (isAsDropDown) {
//                    topPadding = kpx.x(30)
//                } else {
//                    bottomPadding = kpx.x(30)
//                }
//                bubbles {
//                    if (isAsDropDown) {
//                        direction = KDirection.TOP
//                    } else {
//                        direction = KDirection.BOTTOM
//                    }
//                    bubblesWidth = kpx.x(45)
//                    bubblesHeight = kpx.x(30)
//                    all_radius = 10f
//                    bg_color = norColor
//                }
//            }
//            //视图刷新[业务逻辑都在这处理]，返回 视图itemView和下标postion
//            sp?.onBindView { itemView, position ->
//                itemView?.findViewById<KTextView>(kpx.id("pop_txt"))?.apply {
//                    setText("" + datas?.get(position))
//                    onClick {
//                        sp?.dismiss()//关闭
//                    }
//                }
//            }
//        }
//        //显示(会自动刷新,亲测有效)
//        if (isAsDropDown) {
//            sp?.showAsDropDownCenter(view, yoff = -kpx.x(20))//居中显示在控件下方
//        } else {
//            sp?.showAsDropUp(view, xoff = 0, yoff = 0, popuHeight = popuHeight)//显示在控件上方
//        }
//    }

//fixme sp?.dismiss()//关闭
//fixme sp?.destroy()//销毁（需要自己手动调用）
//fixme setOnDismissListener() pop消失监听(pop弹窗消失的时候才会回调)。亲测有效。

//传入的list和原有list是同一个对象，已经绑定。只要不重新赋值=，就会一直相互影响。
//styleAnime动画文件
open class KSpinnerPop(var context: Context, var datas: MutableList<*>?, var styleAnime: Int = R.style.kera_window_alpha_scale_drop) {
    var pop: PopupWindow? = null

    //recyclerView外的容器
    var containerLayout: KRelativeLayout? = null
    var recyclerView: KRecyclerView? = null
    var myAdapter: MyAdapter? = null//适配器暴露出来，方便操作。

    //记录popuwind的宽带和高度。
    var popupWidth: Int = 0
    var popuHeight: Int = 0

    /**
     * fixme 创建itemView视图 [需要自动手动实现]
     * @param popWidth 视图宽度;必须在一开始初始化的时候就设置，后面设置都无效。亲测有效。
     * @param popuHeight fixme popuwindow的高度，显示在控件下方，最好设置成高度自适应。（ViewGroup.LayoutParams.WRAP_CONTENT 能够自适应）；控件上方显示时，要指明具体的高度。上方高度无法自适应。
     * @param isNeverScroll 设置RecyclerView滑动到边缘时无效果模式
     * @param isAddNavigation fixme true 加上底部导航的高度（实现高度全屏）；false不加上底部导航的高度。默认不添加。
     * @param itemView 视图
     */
    open fun onCreateView(popupWidth: Int = ViewGroup.LayoutParams.MATCH_PARENT, popuHeight: Int = ViewGroup.LayoutParams.WRAP_CONTENT, isNeverScroll: Boolean = true,isAddNavigation:Boolean=false, itemView: (viewType: Int) -> View) {
        this.popupWidth = popupWidth
        this.popuHeight = popuHeight//fixme 下方显示自适应（可以自适应），上方显示指明具体的高度（无法自适应）。
        KBaseUi.apply {
            var view = context.UI {
                verticalLayout {
                    if (popuHeight > 0) {
                        //fixme 具体的高度,最外层容器必须设置一下。不然就无效。
                        var layoutParams = ViewGroup.LayoutParams(wrapContent, popuHeight)
                        setLayoutParams(layoutParams)
                    } else {
                        var layoutParams = ViewGroup.LayoutParams(wrapContent, matchParent)
                        setLayoutParams(layoutParams)
                    }
                    //recyclerView容器布局
                    containerLayout = krelativeLayout {
                        onClick {
                            pop?.dismiss()//fixme 防止触摸屏幕外不消失弹窗。
                        }
                        id = kpx.id("spinnerPop_centerView")
                        KBaseUi.apply {
                            recyclerView = krecyclerView {
                                id = kpx.id("spinnerPop_recyclerView")
                                setLinearLayoutManager(true)
                                if (myAdapter == null) {
                                    myAdapter = MyAdapter(this@KSpinnerPop, itemView)
                                }
                                adapter = myAdapter
                                if (isNeverScroll) {
                                    setOverScrollMode(OVER_SCROLL_NEVER);//设置滑动到边缘时无效果模式
                                    setVerticalScrollBarEnabled(false);//滚动条隐藏
                                }
                            }.lparams {
                                width = wrapContent
                                height = wrapContent
                            }
                        }
                    }.lparams {
                        width = wrapContent
                        height = wrapContent
                        if (popuHeight > 0) {
                            height = popuHeight
                        }
                    }
                    //fixme 填充popuwindow的底部
                    view {
                        onClick {
                            //关闭
                            dismiss()
                        }
                    }.lparams {
                        width = matchParent
                        height = matchParent
                    }
                }
            }.view
            pop = KPopuWindowUtils.getInstance().showPopuWindow(view, styleAnime, popupWidth, popuHeight,isAddNavigation)
        }
    }

    var onBindView: ((itemView: View, position: Int) -> Unit)? = null

    //fixme 刷新视图，数据展示+业务逻辑+点击事件 都在这里处理 [需要自动手动实现]
    open fun onBindView(onBindView: (itemView: View, position: Int) -> Unit) {
        this.onBindView = onBindView
    }

    //fixme 下方显示[每次显示的时候，布局都会重新刷新]
    open fun showAsDropDown(view: View?, xoff: Int = 0, yoff: Int = 0) {
        view?.let {
            //fixme pou系统自动showAsDropDown（）下方显示的方法。上方显示的方法系统没有。
            pop?.showAsDropDown(it, xoff, yoff)//fixme xoff 和 yoff是偏移量。
            //数据刷新（亲测有效）
            recyclerView?.adapter?.notifyDataSetChanged()
        }
    }

    //fixme 在指定控件下，水平居中显示。
    open fun showAsDropDownCenter(view: View?, yoff: Int = 0) {
        var xoff = 0
        view?.let {
            if (popupWidth > 0) {
                //计算水平居中位置。
                if (it.width > popupWidth) {
                    xoff = (it.width - popupWidth) / 2
                } else {
                    xoff = -(popupWidth - it.width) / 2
                }
            }
        }
        showAsDropDown(view, xoff, yoff)
    }

    /**
     * fixme 在指定控件上方显示[每次显示的时候，布局都会重新刷新]
     * @param view 指定控件
     * @param xoff x偏移量（左右）
     * @param yoff y偏移量（伤心）
     * @param popuHeight fixme popuwindow的高度；上方显示时，一定要指明具体的高度(大于0)。不然无法控制。
     */
    open fun showAsDropUp(view: View?, xoff: Int = 0, yoff: Int = 0, popuHeight: Int = this.popuHeight): Unit {
        if (view != null) {
            //获取需要在其上方显示的控件的位置信息
            val location = IntArray(2)
            view.getLocationOnScreen(location)
            //在控件上方显示
            pop?.showAtLocation(view, Gravity.NO_GRAVITY, location[0] + view.width / 2 - popupWidth / 2 + xoff, location[1] - popuHeight + yoff)
            //数据刷新（亲测有效）
            recyclerView?.adapter?.notifyDataSetChanged()
        }
    }

    //fixme 关闭
    open fun dismiss() {
        pop?.dismiss()
        //pop?.setOnDismissListener {} 这个是popuwindow关闭时监听回调，系统自带的方法。
    }

    //fixme popuwindow消失监听。亲测有效。如果没有效果，注意 pop是否为空。是否还没实例化。
    //fixme 注意：sp?.onCreateView（）调用之后，pop才实例化哦。
    open fun setOnDismissListener(callback: (() -> Unit)? = null) {
        if (callback != null) {
            pop?.setOnDismissListener {
                callback?.let { it() }
            }
        }

    }

    //fixme 销毁（数据也会一起清除掉）
    open fun destroy() {
        KBaseUi.destroyViewGroup(pop?.contentView)
        containerLayout?.onDestroy()
        recyclerView?.onDestroy()
        pop?.setOnDismissListener(null)
        pop?.dismiss()
        containerLayout = null
        recyclerView = null
        pop = null
        datas?.clear()//清除数据
        datas = null
    }

    companion object {
        class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {}
        class MyAdapter(var sp: KSpinnerPop, var itemView: (viewType: Int) -> View) : RecyclerView.Adapter<MyViewHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
                return MyViewHolder(itemView(viewType))//自定义View每次都是重新实例话出来的
            }

            override fun getItemCount(): Int {
                sp?.datas?.let {
                    return it.size
                }
                return 0
            }

            override fun getItemViewType(position: Int): Int {
                return position
            }

            override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
                sp.onBindView?.let {
                    it(holder.itemView, position)
                }
            }
        }
    }
}