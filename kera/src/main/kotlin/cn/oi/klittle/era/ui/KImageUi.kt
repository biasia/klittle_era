package cn.oi.klittle.era.ui

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KUi
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.widget.recycler.KRecyclerView
import cn.oi.klittle.era.widget.recycler.adapter.KImageAdaper
import org.jetbrains.anko.*

//                var imageUi:KImageUi?=KImageUi()
//                imageUi?.addView(this)?.lparams {
//                    width= matchParent
//                    height= wrapContent
//                    leftMargin=kpx.x(32)
//                    rightMargin=leftMargin
//                }
//                imageUi?.updateViews(true)

/**
 * 图片上传或显示适配器
 * Created by 彭治铭 on 2019/3/13.
 */
class KImageUi : KUi() {

    var recycler: KRecyclerView? = null
    var imageAdapter: KImageAdaper? = null
    override fun createView(ctx: Context?): View? {
        return ctx?.UI {
            verticalLayout {
                var mPadding = kpx.x(32)
                kverticalLayout {
                    radius {
                        bg_color = Color.WHITE
                        all_radius(kpx.x(40))
                    }
                    leftPadding = mPadding
                    rightPadding = leftPadding
                    topPadding = mPadding
                    bottomPadding = topPadding
                    //图片信息
                    ktextView {
                        textSize = kpx.textSizeX(33)
                        textColor = Color.parseColor("#000000")
                        text = getString(R.string.kimageinfo)
                    }
                    linearLayout {
                        gravity = Gravity.CENTER_VERTICAL
                        recycler = krecyclerView {
                            var spanCount = 4
                            setGridLayoutManager(spanCount)
                            if (imageAdapter == null) {
                                imageAdapter = KImageAdaper(spanCount = spanCount)
                            }
                            adapter = imageAdapter
                        }.lparams {
                            width = matchParent
                            height = wrapContent
                        }
                    }.lparams {
                        width = matchParent
                        height = wrapContent
                        topMargin = kpx.x(16)
                    }
                }
            }

        }?.view
    }

    /**
     * @param isUpload 是否上传图片。
     */
    fun updateViews(isUpload: Boolean = false) {
        imageAdapter?.let {
            it.isUpload = isUpload//是否上传图片
            if (it.datas == null) {
                it.datas = arrayListOf()
                it.datas?.let {
                    it.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1607577626371&di=d3233bcd49e45805407b6d257cccc89d&imgtype=0&src=http%3A%2F%2Fattach.bbs.miui.com%2Fforum%2F201105%2F17%2F113554rnu40q7nbgnn3lgq.jpg")
                    it.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1607577626370&di=24f3ec907f11ea7cc78327fe39307389&imgtype=0&src=http%3A%2F%2Fimg.pconline.com.cn%2Fimages%2Fupload%2Fupc%2Ftx%2Fwallpaper%2F1308%2F29%2Fc0%2F25038622_1377746019192.jpg")
                }
            }
        }
    }

}