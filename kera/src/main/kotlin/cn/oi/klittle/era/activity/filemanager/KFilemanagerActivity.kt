package cn.oi.klittle.era.activity.filemanager

import android.os.Bundle
import cn.oi.klittle.era.base.KBaseActivity
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.utils.KFileUtils
import cn.oi.klittle.era.utils.KPathManagerUtils
import java.io.File


/**
 * fixme 文件管理
 */
open class KFilemanagerActivity : KBaseActivity() {

    override fun isEnableSliding(): Boolean {
        return true
    }

    override fun shadowSlidingWidth(): Int {
        //return super.shadowSlidingWidth()
        return kpx.screenWidth() / 2
    }

    override fun isDark(): Boolean {
        return true
    }

    var ui: KFilemanagerUi? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            super.onCreate(savedInstanceState)
            ui = KFilemanagerUi()
            setContentView(ui?.createView(this))
            initEvent()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun initEvent() {
        var apkPath = KPathManagerUtils.getApkLoadDownPath()
        var filePath = KPathManagerUtils.getFileLoadDownPath()
        var datas: ArrayList<File>? = KFileUtils.getInstance().readfiles(apkPath)
        KFileUtils.getInstance().readfiles(filePath, datas)
        ui?.fileAdapt?.let {
            if (it.datas == null) {
                it.datas = datas
            }
            it.notifyDataSetChanged()
        }
    }

    override fun onResume() {
        super.onResume()
    }


}