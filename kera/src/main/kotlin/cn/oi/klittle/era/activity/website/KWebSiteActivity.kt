package cn.oi.klittle.era.activity.website

import android.os.Bundle
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseActivity
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.entity.feature.KFileEntry
import cn.oi.klittle.era.helper.KUiHelper
import cn.oi.klittle.era.utils.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import java.io.File

//fixme 测试api 28;android 9.0没有问题，低版本的没试过。现在主流机器基本都是9.0和10.0的了。
//本地txt文件也能查看（不会乱码），如：content://com.example.myapplication.kera.provider/external_cache_path/2020092414361313144.txt ；即通过fileUri传递。
//也能打开在线文本，如："http://test.bwg2017.com/UploadFile/ContractFile/2020092414361313144.txt"

//fixme webView无法通过路径直渐打开 file?.absolutePath；当能通过uri打开。本地分享的 fileUri也可以。

/**
 * fixme 网页查看
 */
open class KWebSiteActivity : KBaseActivity() {

    override fun isEnableSliding(): Boolean {
        return false//fixme 页面滑动关闭取消，防止和网页滑动事件冲突。
    }

    override fun shadowSlidingWidth(): Int {
        //return super.shadowSlidingWidth()
        return kpx.screenWidth() / 2
    }

    override fun isDark(): Boolean {
        return true
    }

    companion object {
        var mUrl: String? = null//fixme 当前页面的最顶级url。
        var isUseWideViewPort: Boolean = false//fixme true完整的显示手机屏幕;false是BridgeWebView默认样式，整个页面会被放大。
        private var mUrl2: String? = null//fixme 当前页面的最顶级url。防止异常，再多加一个。
        private var isFirstUrl = true//fixme 是否为第一次加载成功的url。

        var mData: String? = null//html文本内容。
        var mDataTitle: String? = null//文本标题
        var isShowScan: Boolean = true;//是否显示二维码扫码
    }

    var ui: KWebSiteUi? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            mUrl2 = null
            isFirstUrl = true
            super.onCreate(savedInstanceState)
            ui = KWebSiteUi()
            setContentView(ui?.createView(this))
            initEvent()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        //KLoggerUtils.e("isLoadData:\t"+isLoadData)
        if (!isLoadData) {
            //fixme mUrl是原始加载的url。以此来判断是否为最顶级的url。亲测有效。
            if (ui?.webView?.url == null) {
                super.onBackPressed()
            }
            ui?.webView?.url?.let {
                //KLoggerUtils.e(""+it+"\t"+ mUrl+"\t"+ mUrl2)
                if (it.trim().equals(mUrl?.trim()) || it.trim().equals(mUrl2?.trim())) {
                    super.onBackPressed()
                } else {
                    ui?.webView?.goBack()
                }
            }
        } else {
            super.onBackPressed()
        }
    }

    //fixme 页面加载
    fun loadUrl(url: String?) {
        if (KRegexUtils.isUrl(url)) {
            isLoadData = false
            url?.let {
                setUrl(it)
            }
            url?.let {
                if (it.length > 0) {
                    //ui?.showProgress()
                    ui?.setProgress(0)
                    //fixme 开始加载
                    //KLoggerUtils.e("url:\t" + url)
                    ui?.webView?.loadUrl(url) { progress ->
                        //KLoggerUtils.e("progress:\t" + progress + "\turl:\t" + ui?.webView?.url)
                        //KLoggerUtils.e("progress:\t" + progress)
                        ui?.setProgress(progress)
                        if (progress >= 100) {
                            ui?.swipeRefreshLayout?.setRefreshing(false);//刷新完成
                            if (isFirstUrl) {
                                isFirstUrl = false
                                mUrl2 = ui?.webView?.url//fixme 记录首次加载成功的Url
                            }
                            //ui?.dissmissProgress()
                        }
                    }
                }
            }
        }
    }

    fun setUrl(url: String?) {
        //KLoggerUtils.e("url：\t"+url)
        //KLoggerUtils.e("标题：\t"+ui?.webView?.title)
        var url = url;
        ui?.toolbar?.leftEditText?.let {
            if (it.equals(url)) {
                return
            }
        }
        ui?.webView?.title?.trim()?.let {
            if (it.length > 0) {
                url = it;//fixme 获取网页标题。
            }
        }
        ui?.toolbar?.leftEditText?.setText(url)
        ui?.toolbar?.leftEditText?.text?.trim()?.let {
            if (it.length > 0) {
                //ui?.toolbar?.leftEditText?.setSelection(0)//光标。
                ui?.toolbar?.leftEditText?.setSelection(it.length)
            }
        }
    }

    var isLoadData = false;//fixme 是否加载html文本内容。

    //直接加载html内容;亲测可行，显示的内容就是打开本地html文件一样。
    //即：把html文本内容复制到本地文件，改名.html;本地浏览器打开即可。
    fun loadData(data: String?) {
        data?.let {
            if (it.length > 0) {
                mDataTitle?.trim()?.let {
                    if (it.length > 0) {
                        setUrl(it)//设置标题。
                    }
                }
                isLoadData = true
                ui?.webView?.loadDataWithBaseURL("", data, "text/html", "UTF-8", "");
            } else {
                isLoadData = false
            }
        }
        if (data == null) {
            isLoadData = false
        }
    }

    fun initEvent() {
        setSoftInputMode_adjustnothing_hidden()
        ui?.webView?.setUseWideViewPort(isUseWideViewPort)
        mData?.trim()?.let {
            if (it.length > 0) {
                loadData(mData)//fixme 加载文本内容
            }
        }
        if (!isLoadData) {
            loadUrl(mUrl)//fixme 加载url
        }
        ui?.toolbar?.rightTextView?.apply {
            onClick {
                //loadUrl()//fixme 重新加载刷新
                //fixme 二维码扫描回调结果
                KUiHelper.goQr_codeActivity {
                    it?.trim()?.let {
                        if (KRegexUtils.isUrl(it)) {
                            var url = it
                            if (KRegexUtils.isThunder(url)) {
                                KIntentUtils.goThunderLink(link = url)//fixme 打开迅雷下载。
                            } else if (KRegexUtils.isUrlFile(url)) {//fixme 普通文件下载。
                                GlobalScope.async {
                                    runOnUiThread {
                                        isOnPause = false//fixme 防止弹窗不显示。
                                        var str = getString(R.string.kfaxianwangluofile)//发现网络文件
                                        var strName = KRegexUtils.getUrlFileName(url)//网络文件名
                                        var str3 = getString(R.string.kshifouxiazai2)//是否下载
                                        _dialogAlert(str + ":\n" + strName + "\n" + str3 + "?")?.apply {
                                            positive(getString(R.string.kxiazai)) {
                                                //fixme 弹窗不能在onActivityResult{}里初始化，所以开协程。
                                                //KLoggerUtils.e("url:\t" + url)
                                                var fileEntry = KFileEntry(url)//传入网络下载地址即可
                                                //打开文件
                                                fileEntry.goOpenFile() {
                                                    fileEntry?.destroy()//销毁。
                                                }
                                            }
                                            show()
                                        }
                                    }
                                }
                            } else {
                                loadUrl(it)//页面加载。
                            }
                        } else if (KRegexUtils.isMobileNO(it)) {
                            //电话号码，跳转到拨号界面
                            KIntentUtils.goCallPhone(getActivity(), it)
                        } else {
                            KUiHelper.goQr_codeResultActivity(it)//跳转二维码结果页面。
                        }
                    }
                }
            }
        }
        ui?.toolbar?.leftEditText?.let {
            //fixme 监听软键盘右下角按钮。
            it.addSearch {
                var url = it.text?.toString()?.trim()
                //KLoggerUtils.e("URL:\t"+url)
                //KLoggerUtils.e("URL:\t"+url);
                //var url = url?.trim();
                //手输入的域名，自动补全。
                if (!KRegexUtils.isUrl(url)) {
                    var pre = "https://www."
                    url = pre + url;
                    if (!KRegexUtils.isContaintUrlSuffix(url)) {
                        var suf = ".com"
                        url = url + suf;
                    }
                }
                //KLoggerUtils.e("URL2:\t" + url);
                loadUrl(url)
                //ui?.toolbar?.leftEditText?.hiddenSoftKeyboard()//关闭软键盘(这里没有什么效果。)
            }
        }
        //fixme 页面加载监听。
        ui?.webView?.let {
            //页面加载开始监听
            it.onPageStarted {
                if (!isLoadData) {
                    setUrl(it)
                }
            }
            //页面加载结算监听（这个百分百能监听到）
            it.onPageFinished {
                if (!isLoadData) {
                    setUrl(it)
                }
            }
        }
        //fixme 进度实时加载回调。
        ui?.webView?.progressCallBack {
            ui?.setProgress(it)
        }
        //fixme 页面刷新
        ui?.swipeRefreshLayout?.setOnRefreshListener {
            if (!isLoadData) {
                ui?.swipeRefreshLayout?.setRefreshing(true);//正在刷新
                ui?.webView?.reload {
                    if (it >= 100) {
                        ui?.swipeRefreshLayout?.setRefreshing(false)//刷新完成
                    }
                }
            } else {
                loadData(mData)
                ui?.swipeRefreshLayout?.setRefreshing(false)//刷新完成
            }
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun finish() {
        super.finish()
        mData = null
        mDataTitle = null
        isLoadData = false
        mUrl = null
        mUrl2 = null
    }

}