package cn.oi.klittle.era.activity.text

import android.os.Bundle
import android.view.View
import cn.oi.klittle.era.base.KBaseActivity
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.utils.KFileUtils
import cn.oi.klittle.era.utils.KLoggerUtils
import cn.oi.klittle.era.utils.KSharedUtils
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.io.File


/**
 * fixme 文本查看器
 */
open class KTextActivity : KBaseActivity() {

    override fun isEnableSliding(): Boolean {
        return true
    }

    override fun shadowSlidingWidth(): Int {
        //return super.shadowSlidingWidth()
        return kpx.screenWidth() / 2
    }

    override fun isDark(): Boolean {
        return true
    }

    var ui: KTextUi? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            super.onCreate(savedInstanceState)
            ui = KTextUi()
            setContentView(ui?.createView(this))
            initEvent()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        var charsetName: String = "GB2312"//文本编码格式: "GB2312"、"UTF-8";不然会乱码。txt文本格式一般都是 GB2312格式。
        var isSharedable: Boolean = true//是否具备分享能力。
        var fileName: String? = null//文件名。（显示指定的文件名。）；finish（）中会清空。
    }

    fun initEvent() {
        var file = getExtraData<File>()
        file?.let {
            GlobalScope.async {
                //var content = KFileUtils.ReadTxtFile(file)
                var content = KFileUtils.ReadTxtFile(file, charsetName)
                //KLoggerUtils.e("文本内容：\t"+content)
                if (!isFinishing) {
                    runOnUiThread {
                        if (!isFinishing) {
                            var name = file.name
                            fileName?.trim()?.let {
                                if (it.length > 0) {
                                    name = it
                                }
                            }
                            ui?.toolbar?.leftTextView2?.setText(name)
                            ui?.txt?.setText(content)
                        } else {
                            content = null
                        }
                    }
                } else {
                    content = null
                }
            }
        }
        if (isSharedable) {
            ui?.toolbar?.rightTextView?.onClick {
                //分享
                KSharedUtils.shareFile(file)
            }
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun finish() {
        try {
            super.finish()
            fileName = null//文件名情况。
            ui?.destroy(this)//界面销毁
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}