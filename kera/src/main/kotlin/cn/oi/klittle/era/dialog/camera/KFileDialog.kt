package cn.oi.klittle.era.dialog.camera

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import androidx.core.view.GestureDetectorCompat
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseDialog
import cn.oi.klittle.era.comm.KToast
import cn.oi.klittle.era.utils.KFileUtils
import cn.oi.klittle.era.utils.KLoggerUtils
import cn.oi.klittle.era.utils.KPictureUtils
import cn.oi.klittle.era.utils.KProportionUtils
import java.io.File

//                        if (file == null) {
//                            file = KFileDialog(act);
//                            file?.EXTRA_DURATION_LIMIT=10;//fixme 视频录制最大时间限制，单位秒
//                            file?.isCompress=true;//是否压缩图片
//                        }
//                        file?.setCallback { file, type ->
//                            KLoggerUtils.e("文件路径：" + file.path + "\t文件大小：\t" + file.length() + "\tisImage:\t" + KRegexUtils.isImage(file.path) + "\tisVideo:\t" + KRegexUtils.isVideo(file.path)
//                                    + "\tisVoice:\t" + KRegexUtils.isVoice(file.path));
//                            //文件路径：/data/user/0/cn.nbjianghua.bwg.app.test/cache/data/record/1710825204397.pcm	文件大小：	5912
//                            KFileType.openFile(this@MainActivity5, file, type)
//                        }
//                        file?.show();
/**
 * 视频，图片，语音选择
 * Created by 彭治铭 on 2018/6/3.
 */
//使用说明
/**
 * @param ctx: Context
 * @param dateChoose 时间选择器
 * @param isStatus 是否有状态栏
 * @param isTransparent 是否透明
 */
///20秒音频大小才 37106 B(单位字节)；1M都没有。 2秒大约也就 5912 B ;录音体积大小，很小的。
///手机视频大小：10 秒 26M  12秒 30M；29M；11秒 28M。
///15秒 35M；20秒 51M；33秒80M；1分钟 148M。
open class KFileDialog(ctx: Context, isStatus: Boolean = true, isTransparent: Boolean = true) : KBaseDialog(ctx, R.layout.kera_dialog_camera, isStatus, isTransparent) {

    var EXTRA_DURATION_LIMIT: Int = 10//fixme 视频录制最大时间，单位秒。这么默认设置最大时长不超过10秒。
    var isCompress = true;//fixme 是否压缩图片（默认压缩）；只压缩相机拍照的图片；视频不会压缩（不支持。）

    init {
        if (ctx is Activity) {
            //KProportionUtils.getInstance().adapterWindow(ctx, dialog?.window)//适配
            findViewById<View>(R.id.crown_date_parent)?.let {
                KProportionUtils.getInstance().adapterAllView(ctx, it, false, false)
            }
        }
        dialog?.window?.setWindowAnimations(R.style.kera_window_bottom)//动画
        //拍视频
        findViewById<View>(R.id.crown_txt_paishiping)?.setOnClickListener {
            KPictureUtils.cameraVideo(EXTRA_DURATION_LIMIT = EXTRA_DURATION_LIMIT) {
                dismiss()
                var file = it;
                _callback?.let { it(file, KFileType.TTYE_VIDEO) }
            }
        }
        //拍摄照片
        findViewById<View>(R.id.crown_txt_paizhaopian)?.setOnClickListener {
            if (isCompress) {
                //相机拍照，只返回压缩后的图片。原文件会自动删除。只保留压缩后的。
                KPictureUtils.cameraCompress { it ->
                    dismiss()
                    var file = it;
                    _callback?.let { it(file, KFileType.TTYE_IMAGE) }
                }
            } else {
                //相机拍照，图片不会压缩。
                KPictureUtils.camera {
                    dismiss()
                    var file = it;
                    _callback?.let { it(file, KFileType.TTYE_IMAGE) }
                }
            }
        }
        //语音
        findViewById<View>(R.id.crown_txt_yuying)?.let {
            if (ctx is Activity) {
                var view = it;
                KRecordUtils.isACTION_DOWN_RECORD = true;//手指长按触摸时，录音。
                it.setOnTouchListener { v, event ->
                    try {
                        event?.action?.let {
                            if (it == MotionEvent.ACTION_DOWN) {
//                                KLoggerUtils.e("=========手指按下")
                                KRecordUtils.isACTION_DOWN = true;//fixme 手指按下
                                view.setBackgroundColor(Color.parseColor("#F2F2F2"))
                                KRecordUtils.startRecording(activity = ctx) {
                                    if (it) {
//                                        KToast.showInfo(getString(R.string.luying));//正在录音
                                        (findViewById<View>(R.id.crown_txt_yuying_txt) as TextView)?.let {
                                            it.setTextColor(Color.BLUE)
                                            it.setText(getString(R.string.luying));//fixme 正在录音
                                        }
                                    } else {
                                        view.setBackgroundColor(Color.parseColor("#FFFFFF"))
                                        //录音失败，一般都是没有录音权限导致的。
                                        dismiss();//FIXME 直接关闭当前弹窗，防止覆盖权限提示框。
                                    }
                                }
                            } else if (it == MotionEvent.ACTION_UP) {
//                                KLoggerUtils.e("=========手指离开")
                                KRecordUtils.isACTION_DOWN = false;//fixme 手指离开
                                view.setBackgroundColor(Color.parseColor("#FFFFFF"))
                                (findViewById<View>(R.id.crown_txt_yuying_txt) as TextView)?.let {
                                    it.setTextColor(Color.BLACK)
                                    it.setText(getString(R.string.yuying2));//fixme 语音(按住说话)
                                }
                                var file = KRecordUtils.stopRecording()
                                if (file != null) {
                                    //大于1秒的大小是 3639；两秒的大小 5367
                                    if (file.length() > 4000) {
                                        _callback?.let { it(file, KFileType.TTYE_AUDIO) }
                                        KToast.showInfo(getString(R.string.luyingover));//录音结束
                                        dismiss();//fixme 录音成功，再关闭弹窗比较好。
                                    } else {
                                        KToast.showError(getString(R.string.luyingover2))//说话时间太短。
                                        try {
                                            var b = file?.delete();//fixme 删除无效文件
//                                        KLoggerUtils.e("是否删除完成：\t" + b)
                                        } catch (e: Exception) {
                                            KLoggerUtils.e("文件删除异常：\t" + e.message);
                                        }
                                    }
                                }
                                //dismiss();
                            }
                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                    true
                    //false;
                }
            }
        }
//        findViewById<View>(R.id.crown_txt_yuying)?.setOnClickListener {
//            KPictureUtils.cameraRecord {//fixme 这个系统录音返回的文件为空。
//                dismiss()
//                var file = it;
//                _callback?.let { it(file, KFileType.TTYE_AUDIO) }
//            }
//        }

        //取消
        findViewById<View>(R.id.crown_txt_cancel)?.setOnClickListener {
            dismiss()
        }
        isDismiss(true)
    }

    var _callback: ((file: File, type: Int) -> Unit)? = null;

    ///fixme 回调
    open fun setCallback(callback: ((file: File, type: Int) -> Unit)): KFileDialog {
        this._callback = callback;
        return this;
    }

    override fun onShow() {
        super.onShow()
        try {

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onDismiss() {
        super.onDismiss()
    }

}