package cn.oi.klittle.era.activity.website

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseUi
import cn.oi.klittle.era.base.KUi
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.helper.KUiHelper
import cn.oi.klittle.era.toolbar.KToolbar
import cn.oi.klittle.era.widget.compat.KTextView
import cn.oi.klittle.era.widget.compat.KView
import cn.oi.klittle.era.widget.progress.KColorLineProgressBar
import cn.oi.klittle.era.widget.web.K3WebView
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.swipeRefreshLayout

open class KWebSiteUi : KUi() {

    var toolbar: KToolbar? = null
    var webView: K3WebView? = null
    var progressLine: KColorLineProgressBar? = null
    var swipeRefreshLayout:SwipeRefreshLayout?=null
    override fun destroy(activity: Activity?) {
        super.destroy(activity)
        toolbar = null
    }

    override fun createView(ctx: Context?): View? {

        return ctx?.UI {
            verticalLayout {
                //fitsSystemWindows = true
                backgroundColor = Color.WHITE
                toolbar = KToolbar(this, getActivity(), false, hasLine = false)?.apply {
                    //标题栏背景色
                    contentView?.apply {
                        backgroundColor = Color.WHITE
                    }
                    //左边返回文本（默认样式自带一个白色的返回图标）
                    leftTextView?.apply {
                        autoBg {
                            width = kpx.x(48)
                            height = width
                            autoBg(R.mipmap.kera_error2)
                            autoLeftPadding = kpx.x(12f)
                        }
                    }
                    leftEditText?.apply {
                        textColor = Color.BLACK
                        //textSize = kpx.textSizeX(32)
                        textSize = kpx.textSizeX(30)
                        isBold(true)
                        gravity = Gravity.CENTER_VERTICAL
                        visibility = View.VISIBLE//显示
                        //默认不包含中文，空格和换行(回车键)。
                        //notContainsRegex { }//显示不限制。允许输入
                        maxWidth = kpx.x(600)
                        if (!KWebSiteActivity.isShowScan){
                            maxWidth = kpx.x(648)
                        }
                    }
//                    rightTextView?.apply {
//                        autoBg {
//                            //width = kpx.x(48)
//                            //height = kpx.x(48)
//                            width = kpx.x(45)
//                            height = kpx.x(45)
//                            autoBg(R.mipmap.kera_reload)//重新刷新
//                            isAutoCenterVertical=true
//                            isAutoRight=true
//                            autoRightPadding=kpx.x(12f)
//                        }
//                    }
                    //右边文本(二维码扫描)
                    rightTextView?.apply {
                        //layoutParams(px.x(94), toolbarHeight)
                        autoBg {
                            width = kpx.x(48)
                            height = kpx.x(48)
                            autoBg(R.mipmap.kera_scan_black)
                            isAutoCenterVertical = true
                            isAutoRight = true
                            autoRightPadding = kpx.x(24f)
                        }
                        if (!KWebSiteActivity.isShowScan){
                            visibility=View.GONE;
                        }
                    }
                }
                progressLine = kColorLineProgressBar {

                }.lparams {
                    width = matchParent
                    height = kpx.x(3)
                }
                swipeRefreshLayout=swipeRefreshLayout {
                    webView = kwebView {

                    }
                }.lparams {
                    width = matchParent
                    height = matchParent
                }
            }
        }?.view
    }

//    //显示进度条
//    open fun showProgress() {
//        _dialogProgressbar(false)?.show()
//    }
//
//    open fun dissmissProgress() {
//        _dialogProgressbar(false)?.dismiss()
//    }

    //设置进度条
    open fun setProgress(progress: Int) {
        progressLine?.progress = progress
        if (progress >= 100) {
            progressLine?.isShowBgLine = true//显示横线
        } else {
            progressLine?.isShowBgLine = false
        }
    }

}