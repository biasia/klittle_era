package cn.oi.klittle.era.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

///fixme 简书：https://www.jianshu.com/p/9bdb2d519309
///fixme 百度文档地址：https://lbsyun.baidu.com/index.php?title=uri/api/android
//                        var endLatitude = "29.833187"//目的地纬度
//                        var endLongitude = "121.55153"//目的地经度
//                        var endName = "宁波市鄞州区宁波鄞州万达广场"//目的地名称
//                        KAddressMapUtils.goBaiduMap(context, endLatitude, endLongitude, endName)
///                       //起点位置
//                        var startLatitude = "29.934521"//起点纬度
//                        var startLongitude = "121.537466"//起点经度
//                        var startName = "江花玻璃（庄桥）"//起点位置
//                        //终点位置
//                        var endLatitude = "29.833187"//目的地纬度
//                        var endLongitude = "121.55153"//目的地经度
//                        var endName = "宁波市鄞州区宁波鄞州万达广场"//目的地名称
//                        KAddressMapUtils.goBaiduMap(context, startLatitude, startLongitude, startName, endLatitude, endLongitude, endName)
//fixme 高德地图api地址：https://lbs.amap.com/api/amap-mobile/guide/android/route
//fixme 腾讯地图api地址：https://lbs.qq.com/webApi/uriV1/uriGuide/uriWebRoute
public class KAddressMapUtils {

    /**
     * 跳转到地图;自动选择百度，高德，腾讯。地图。
     * 当前位置到目的地。
     *
     * @param context
     * @param endLatitude  目的地纬度,如：29.934521
     * @param endLongitude 目的地经度，如：121.55153
     * @param endName      目的地名称，如：xxx莫莫地
     */
    public static boolean goMap(Context context, String endLatitude, String endLongitude, String endName) {
        if (context != null) {
            try {
                //优先跳转到百度地图
                boolean b1 = goBaiduMap(context, endLatitude, endLongitude, endName);
                if (b1) {
                    return true;
                } else {
                    //跳转到高德地图
                    boolean b2 = goGaoDeMap(context, endLatitude, endLongitude, endName);
                    if (b2) {
                        return true;
                    } else {
                        //跳转到腾讯地图
                        boolean b3 = goTencentMap(context, endLatitude, endLongitude, endName);
                        if (b3) {
                            return true;
                        }
                    }
                }
            } catch (Exception e) {
                //没有安装百度地图
                KLoggerUtils.INSTANCE.e("地图跳转异常：\t" + e.getMessage());
            }
        }
        return false;
    }


    /**
     * 跳转到地图;自动选择百度，高德，腾讯。地图。
     * 指定起点位置到终点位置
     *
     * @param context
     * @param starLatitude  起始纬度
     * @param starLongitude 起始经度
     * @param starName      起始位置名称
     * @param endLatitude   目的地纬度,如：29.934521
     * @param endLongitude  目的地经度，如：121.55153
     * @param endName       目的地名称，如：xxx莫莫地
     * @return
     */
    public static boolean goMap(Context context, String starLatitude, String starLongitude, String starName, String endLatitude, String endLongitude, String endName, String RoutePreference) {
        if (context != null) {
            try {
                //优先跳转到百度地图
                boolean b1 = goBaiduMap(context, starLatitude, starLongitude, starName, endLatitude, endLongitude, endName, RoutePreference);
                if (b1) {
                    return true;
                } else {
                    //跳转到高德地图
                    boolean b2 = goGaoDeMap(context, starLatitude, starLongitude, starName, endLatitude, endLongitude, endName);
                    if (b2) {
                        return true;
                    } else {
                        //跳转到腾讯地图
                        boolean b3 = goTencentMap(context, starLatitude, starLongitude, starName, endLatitude, endLongitude, endName);
                        if (b3) {
                            return true;
                        }
                    }
                }
            } catch (Exception e) {
                KLoggerUtils.INSTANCE.e("地图跳转异常：\t" + e.getMessage());
            }
        }
        return false;
    }

    /**
     * 跳转到百度地图
     * 当前位置到目的地。
     *
     * @param context
     * @param endLatitude  目的地纬度,如：29.934521
     * @param endLongitude 目的地经度，如：121.55153
     * @param endName      目的地名称，如：xxx莫莫地
     */
    public static boolean goBaiduMap(Context context, String endLatitude, String endLongitude, String endName) {
        if (context != null) {
            try {
                Uri uri = Uri.parse("baidumap://map/direction?destination=latlng:"
                        + endLatitude + ","
                        + endLongitude + "|name:" + endName
                        + "&mode=driving");//mode=driving 是驾车模式
//                导航模式，
//                可选transit（公交）、
//                driving（驾车）、
//                walking（步行）和riding（骑行）
//                默认:driving
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//必不可少，不然context会报错。
                context.startActivity(intent);
                return true;//跳转成功。
            } catch (Exception e) {
                //没有安装百度地图
                KLoggerUtils.INSTANCE.e("百度地图跳转异常：\t" + e.getMessage());
            }
        }
        return false;
    }

    /**
     * 跳转到百度地图，亲测有效。
     * 指定起点位置到终点位置
     *
     * @param context
     * @param starLatitude    起始纬度
     * @param starLongitude   起始经度
     * @param starName        起始位置名称
     * @param endLatitude     目的地纬度,如：29.934521
     * @param endLongitude    目的地经度，如：121.55153
     * @param endName         目的地名称，如：xxx莫莫地
     * @param RoutePreference 路线偏好，如：最短时间，最短距离，避开高速
     * @return
     */
    public static boolean goBaiduMap(Context context, String starLatitude, String starLongitude, String starName, String endLatitude, String endLongitude, String endName, String RoutePreference) {
        if (context != null) {
            if (KStringUtils.INSTANCE.isEmpty(starLatitude) || KStringUtils.INSTANCE.isEmpty(starLongitude)) {
                //当前位置到终点位置
                return goBaiduMap(context, endLatitude, endLongitude, endName);
            }
            try {
                //起始位置，默认是当前位置
                String origin = "origin=latlng:" + starLatitude + ","
                        + starLongitude + "|name:" + starName;
                //终点位置
                String destination = "&destination=latlng:" + endLatitude + ","
                        + endLongitude + "|name:" + endName;
                String strUrl = "baidumap://map/direction?"
                        + origin
                        + destination
                        + "&coord_type=bd09ll"
                        + "&mode=driving";//       + "&mode=driving";
//                fixme 坐标类型，必选参数。 coord_type 如开发者不传递正确的坐标类型参数，会导致地点坐标位置偏移。
//                示例：
//                coord_type= bd09ll
//                允许的值为：
//                bd09ll（百度经纬度坐标）
//                bd09mc（百度墨卡托坐标）
//                gcj02（经国测局加密的坐标）
//                wgs84（gps获取的原始坐标）
//                fixme 导航模式，
//                可选transit（公交）、
//                driving（驾车）、
//                walking（步行）和riding（骑行）
//                默认:driving
//                fixme type 路线选择，只对 driving 有效。并且 src 必传，不然不保证服务。
                if (RoutePreference != null) {
//                BLK:躲避拥堵(自驾);
//                TIME:最短时间(自驾);
//                DIS:最短路程(自驾);
//                FEE:少走高速(自驾);
//                HIGHWAY:高速优先;
//                DEFAULT:推荐（自驾，地图app不选择偏好）;
//                默认:地图app所选偏好
//                String RoutePreference = "最短时间";
//                String RoutePreference = "最短距离";
                    String car_type = null;
                    if (RoutePreference.contains("最短时间") || RoutePreference.contains("最少时间")) {
                        car_type = "TIME";
                    } else if (RoutePreference.contains("最短距离") || RoutePreference.contains("最短路程")) {
                        car_type = "DIS";
                    } else if (RoutePreference.contains("避开高速") || RoutePreference.contains("少走高速")) {
                        car_type = "FEE";
                    } else {

                    }
                    if (car_type != null) {
                        strUrl = strUrl + "&car_type=" + car_type + "&type=" + car_type;
                    }
                }
                strUrl = strUrl + "&src=" + context.getPackageName();//fixme 亲测 src统计来源	必选 不传此参数，不保证服务；（这个必须传，不然路线选择type可能无效，）
//                Log.e("test", "strUrl:" + strUrl);
                Uri uri = Uri.parse(strUrl);//mode=driving 是驾车模式
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//必不可少，不然context会报错。
                context.startActivity(intent);
                return true;//跳转成功。
            } catch (Exception e) {
                //没有安装百度地图
                KLoggerUtils.INSTANCE.e("百度地图跳转异常：\t" + e.getMessage());
            }
        }
        return false;
    }


    /**
     * 跳转到高德地图
     * 当前位置到目的地。
     *
     * @param context
     * @param endLatitude  目的地纬度,如：29.934521
     * @param endLongitude 目的地经度，如：121.55153
     * @param endName      目的地名称，如：xxx莫莫地
     */
    public static boolean goGaoDeMap(Context context, String endLatitude, String endLongitude, String endName) {
        if (context != null) {
            try {
                //t = 0（驾车）= 1（公交）= 2（步行）= 3（骑行）= 4（火车）= 5（长途客车）（骑行仅在V7.8.8以上版本支持）
                //String dat = "amapuri://route/plan/?sid=&slat=&slon=&sname=A&did=&dlat=" + endLatitude + "&dlon=" + endLongitude + "&dname=" + endName + "&dev=0&t=0";
                String dat = "amapuri://route/plan/?dlat=" + endLatitude + "&dlon=" + endLongitude + "&dname=" + endName + "&dev=0&t=0";
                Uri uri = Uri.parse(dat);//mode=driving 是驾车模式
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.autonavi.minimap");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//必不可少，不然context会报错。
                context.startActivity(intent);
                return true;//跳转成功。
            } catch (Exception e) {
                //没有安装高德地图
                KLoggerUtils.INSTANCE.e("高德地图跳转异常：\t" + e.getMessage());
            }
        }
        return false;
    }

    /**
     * 跳转到高德地图，亲测有效。
     * 指定起点位置到终点位置
     *
     * @param context
     * @param starLatitude  起始纬度
     * @param starLongitude 起始经度
     * @param starName      起始位置名称
     * @param endLatitude   目的地纬度,如：29.934521
     * @param endLongitude  目的地经度，如：121.55153
     * @param endName       目的地名称，如：xxx莫莫地
     * @return
     */
    public static boolean goGaoDeMap(Context context, String starLatitude, String starLongitude, String starName, String endLatitude, String endLongitude, String endName) {
        if (context != null) {
            if (KStringUtils.INSTANCE.isEmpty(starLatitude) || KStringUtils.INSTANCE.isEmpty(starLongitude)) {
                //当前位置到终点位置
                return goGaoDeMap(context, endLatitude, endLongitude, endName);
            }
            try {
                //t = 0（驾车）= 1（公交）= 2（步行）= 3（骑行）= 4（火车）= 5（长途客车）（骑行仅在V7.8.8以上版本支持）
                //String dat = "amapuri://route/plan/?sid=&slat=" + starLatitude + "&slon=" + starLongitude + "&sname=" + starName + "A&did=&dlat=" + endLatitude + "&dlon=" + endLongitude + "&dname=" + endName + "&dev=0&t=0";
                String dat = "amapuri://route/plan/?sid=&slat=" + starLatitude + "&slon=" + starLongitude + "&sname=" + starName + "A&did=&dlat=" + endLatitude + "&dlon=" + endLongitude + "&dname=" + endName + "&dev=0&t=0";
                Uri uri = Uri.parse(dat);//mode=driving 是驾车模式
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.autonavi.minimap");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//必不可少，不然context会报错。
                context.startActivity(intent);
                return true;//跳转成功。
            } catch (Exception e) {
                //没有安装高德地图
                KLoggerUtils.INSTANCE.e("高德地图跳转异常：\t" + e.getMessage());
            }
        }
        return false;
    }

    /**
     * 跳转到腾讯地图
     * 当前位置到目的地。
     *
     * @param context
     * @param endLatitude  目的地纬度,如：29.934521
     * @param endLongitude 目的地经度，如：121.55153
     * @param endName      目的地名称，如：xxx莫莫地
     */
    public static boolean goTencentMap(Context context, String endLatitude, String endLongitude, String endName) {
        if (context != null) {
            try {
//                type 类型
//                路线规划方式参数：
//                公交：bus
//                驾车：drive
//                步行：walk（仅适用移动端）
                String dat = "qqmap://map/routeplan?type=drive&from=我的位置&fromcoord=0,0"
                        + "&to=" + endName
                        + "&tocoord=" + endLatitude + "," + endLongitude
                        + "&policy=1&referer=myapp";
                Uri uri = Uri.parse(dat);//mode=driving 是驾车模式
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.tencent.map");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//必不可少，不然context会报错。
                context.startActivity(intent);
                return true;//跳转成功。
            } catch (Exception e) {
                //没有安装腾讯地图
                KLoggerUtils.INSTANCE.e("腾讯地图跳转异常：\t" + e.getMessage());
            }
        }
        return false;
    }

    /**
     * 跳转到腾讯地图，亲测有效。
     * 指定起点位置到终点位置
     *
     * @param context
     * @param starLatitude  起始纬度
     * @param starLongitude 起始经度
     * @param starName      起始位置名称
     * @param endLatitude   目的地纬度,如：29.934521
     * @param endLongitude  目的地经度，如：121.55153
     * @param endName       目的地名称，如：xxx莫莫地
     * @return
     */
    public static boolean goTencentMap(Context context, String starLatitude, String starLongitude, String starName, String endLatitude, String endLongitude, String endName) {
        if (context != null) {
            if (KStringUtils.INSTANCE.isEmpty(starLatitude) || KStringUtils.INSTANCE.isEmpty(starLongitude)) {
                //当前位置到终点位置
                return goTencentMap(context, endLatitude, endLongitude, endName);
            }
            try {
                String dat = "qqmap://map/routeplan?type=drive&from=" + starName + "&fromcoord=" + starLatitude + "," + starLongitude
                        + "&to=" + endName
                        + "&tocoord=" + endLatitude + "," + endLongitude
                        + "&policy=1&referer=myapp";
                Uri uri = Uri.parse(dat);//mode=driving 是驾车模式
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setPackage("com.tencent.map");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//必不可少，不然context会报错。
                context.startActivity(intent);
                return true;//跳转成功。
            } catch (Exception e) {
                //没有安装腾讯地图
                KLoggerUtils.INSTANCE.e("腾讯地图跳转异常：\t" + e.getMessage());
            }
        }
        return false;
    }

}
