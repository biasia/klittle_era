package com.idogfooding.glassscanner.widget.rv

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cn.oi.klittle.era.widget.recycler.KRecyclerView
import cn.oi.klittle.era.widget.recycler.nested.code.NestedRecyclerView


/**
 * 嵌套RecyclerView父类RecyclerView；解决 RecyclerView嵌套RecyclerView滑动冲突的问题。这个效果不好，没有KRecyclerViewAtRecycleView好。
 * 测试发现，垂直滑动正常，水平滑动可能还是有点卡顿。
 */
class KNestedRecyclerView : NestedRecyclerView {
    constructor(context: Context) : super(context) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {}

    constructor(viewGroup: ViewGroup) : super(viewGroup.context) {
        viewGroup.addView(this)//直接添加进去,省去addView(view)
    }

}