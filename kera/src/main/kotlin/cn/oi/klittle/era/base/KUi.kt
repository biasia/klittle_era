package cn.oi.klittle.era.base

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import cn.oi.klittle.era.R
import cn.oi.klittle.era.dialog.date.KDateChooseBetweenDialog
import cn.oi.klittle.era.dialog.date.KDateChooseDialog
import cn.oi.klittle.era.dialog.date.KDateChooseHoursBetweenDialog
import cn.oi.klittle.era.dialog.date.KDateChooseHoursDialog
import cn.oi.klittle.era.dialog.input.KTextInputDialog
import cn.oi.klittle.era.dialog.progressbar.KProgressDialog
import cn.oi.klittle.era.dialog.timi.KTimiAlertDialog
import cn.oi.klittle.era.dialog.timi.KTimiDialog
import cn.oi.klittle.era.dialog.timi.KTopTimiDialog
import cn.oi.klittle.era.dialog.version.KVersionLevelDialog
import cn.oi.klittle.era.entity.feature.KDateChooseEntity

//                    fixme 调用案例一:其中ui2集成KUi
//                    var ui2: ui2? = ui2()
//                    ui2?.addView(this)?.lparams {
//                        width = matchParent
//                        height = wrapContent
//                    }

//                    fixme 案例二：kui（）方法是KBaseUi里集成的。KBaseui里面能够添加子KBaseui
//                    var ui2: ui2? = ui2()
//                    kui(ui2) {
//
//                    }.lparams {
//                        width = matchParent
//                        height = wrapContent
//                    }

abstract class KUi : KBaseUi() {

    companion object {

        fun setLayoutParamsDefaultForMatch(itemView: View?, width: Int = MATCH_PARENT, height: Int = MATCH_PARENT) {
            setLayoutParams(itemView, width, height)
        }

        /**
         * 设置控件宽高;默认是 WRAP_CONTENT自适应。
         */
        fun setLayoutParams(view: View?, width: Int = WRAP_CONTENT, height: Int = WRAP_CONTENT) {
            view?.let {
                if (it.layoutParams == null) {
                    var layoutParams = ViewGroup.LayoutParams(width, height)
                    it.layoutParams = layoutParams
                } else {
                    it.layoutParams?.let {
                        it.width = width
                        it.height = height
                    }
                }
                it.requestLayout()//重新布局。
            }
        }
    }

    //fixme 视图View
    private var contentView: View? = null
    public open fun getContentView(ctx: Context? = getActivity()): View? {
        if (contentView == null) {
            contentView = createView(ctx)//fixme 获取视图。
        }
        return contentView
    }

    /**
     * 添加视图;
     * @param viewGroup 父容器
     * @param width 宽 （默认是WRAP_CONTENT自适应）
     * @param height 高
     */
    open fun addView(viewGroup: ViewGroup, width: Int? = null, height: Int? = null): View? {
        viewGroup?.let {
            var content = getContentView(it.context)
            content?.let {
                if (it.parent != null) {
                    return content//fixme 一个view只能有一个父容器;防止重复添加。多次添加会报错的。除非 调用removeView()，移除后就可以重新添加了。
                }
            }
            var layoutParams: ViewGroup.LayoutParams? = null
            if (width != null && height != null) {
                //layoutParams=ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT)
                layoutParams = ViewGroup.LayoutParams(width, height)
            } else if (width != null) {
                layoutParams = ViewGroup.LayoutParams(width, WRAP_CONTENT)
            } else if (height != null) {
                layoutParams = ViewGroup.LayoutParams(WRAP_CONTENT, height)
            }
            if (layoutParams != null) {
                it?.addView(content, layoutParams)
            } else {
                it?.addView(content)//fixme viewGroup.addView（）默认宽高也是 WRAP_CONTENT
            }
            return content
        }
        return contentView
    }

    //fixme 视图销毁方法，子类可以重写。KBaseActivity里的finish()会自动调用销毁。
    override fun destroy(activity: Activity?) {
        super.destroy(activity)
    }

    /**
     * fixme 以下 dialog前面加下划线 _ ;即：_dialog（）是为了防止和子类冲突。
     */

    /**
     * fixme 显示网络进度条
     * @param isLocked 是否屏蔽返回键，默认屏幕
     */
    open fun _showProgressbar(isLocked: Boolean = true) {
        getActivity()?.let {
            if (it is KBaseActivity) {
                it._showProgressbar(isLocked)
            }
        }
    }

    /**
     * fixme 关闭网络进度条
     */
    open fun _dismissProgressbar() {
        getActivity()?.let {
            if (it is KBaseActivity) {
                it._dismissProgressbar()
            }
        }
    }

    /**
     * fixme 0.网络进度条
     * @param isLocked 是否屏蔽返回键，默认屏幕
     */
    open fun _dialogProgressbar(isLocked: Boolean = true): KProgressDialog? {
        getActivity()?.let {
            if (it is KBaseActivity) {
                return it._dialogProgressbar(isLocked)
            }
        }
        return null
    }


    //1.显示弹窗信息;只有一个确认按钮
    open fun _dialogInfo(mession: String?): KTimiDialog? {
        getActivity()?.let {
            if (it is KBaseActivity) {
                return it._dialogInfo(mession)
            }
        }
        return null
    }

    //2.显示信息，有标题，确认和取消两个按钮
    open fun _dialogAlert(mession: String?): KTimiAlertDialog? {
        getActivity()?.let {
            if (it is KBaseActivity) {
                return it._dialogAlert(mession)
            }
        }
        return null
    }

    //3.显示顶部弹窗信息
    open fun _dialogTopInfo(mession: String?): KTopTimiDialog? {
        getActivity()?.let {
            if (it is KBaseActivity) {
                return it._dialogTopInfo(mession)
            }
        }
        return null
    }

    /**
     * 4.文本输入框
     *@param mession 文本框内容
     * @param callBack 文本框回调内容
     */
    open fun _dialogTextInputAlert(mession: String?, callBack: ((text: String) -> Unit)? = null): KTextInputDialog? {
        getActivity()?.let {
            if (it is KBaseActivity) {
                return it._dialogTextInputAlert(mession, callBack)
            }
        }
        return null
    }

    /**
     * 6.日期弹窗
     * @param date 当前选中日期
     * @param callback 日期选中回调
     */
    open fun _dialogDate(date: KDateChooseEntity? = null, callback: (dateChoose: KDateChooseEntity) -> Unit): KDateChooseDialog? {
        getActivity()?.let {
            if (it is KBaseActivity) {
                return it._dialogDate(date, callback)
            }
        }
        return null
    }

    /**
     * 7.日期弹窗,精确到时分
     * @param date 当前选中日期
     * @param callback 日期选中回调
     */
    open fun _dialogDateHours(date: KDateChooseEntity? = null, callback: (dateChoose: KDateChooseEntity) -> Unit): KDateChooseHoursDialog? {
        getActivity()?.let {
            if (it is KBaseActivity) {
                return it._dialogDateHours(date, callback)
            }
        }
        return null
    }

    /**
     * 8.日期弹窗(间隔时间)
     * @param start 开始选中日期
     * @param end 结束选中日期
     * @param callback 日期选中回调。
     */
    open fun _dialogDateBetween(start: KDateChooseEntity? = null, end: KDateChooseEntity? = null, callback: (start: KDateChooseEntity, end: KDateChooseEntity) -> Unit): KDateChooseBetweenDialog? {
        getActivity()?.let {
            if (it is KBaseActivity) {
                return it._dialogDateBetween(start, end, callback)
            }
        }
        return null
    }

    /**
     * 9.日期弹窗(间隔时间，精确到时分)
     * @param start 开始选中日期
     * @param end 结束选中日期
     * @param callback 日期选中回调。
     */
    open fun _dialogDateHoursBetween(start: KDateChooseEntity? = null, end: KDateChooseEntity? = null, callback: (start: KDateChooseEntity, end: KDateChooseEntity) -> Unit): KDateChooseHoursBetweenDialog? {
        getActivity()?.let {
            if (it is KBaseActivity) {
                return it._dialogDateHoursBetween(start, end, callback)
            }
        }
        return null
    }

    /**
     * 10.软件更新
     * @param url apk更新下载地址
     * @param mession 更新内容
     */
    open fun _dialogVersion(url: String?, mession: String? = KBaseUi.getString(R.string.kversion_update_mession)): KVersionLevelDialog? {
        getActivity()?.let {
            if (it is KBaseActivity) {
                return it._dialogVersion(url, mession)
            }
        }
        return null
    }

}