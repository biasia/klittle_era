package cn.oi.klittle.era.dialog.date

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.TextView
import cn.oi.klittle.era.R
import cn.oi.klittle.era.base.KBaseDialog
import cn.oi.klittle.era.entity.feature.KDateChooseEntity
import cn.oi.klittle.era.comm.kpx
import cn.oi.klittle.era.utils.KCalendarUtils
import cn.oi.klittle.era.utils.KProportionUtils
import cn.oi.klittle.era.utils.KStringUtils
import cn.oi.klittle.era.widget.KRollerView

/**
 * 日期选择器(开始日期 至 结束日期)，精确到：年，月，日,时，分
 * Created by 彭治铭 on 2018/6/3.
 */
//使用说明
//                        var dateBetween = KDateChooseHoursBetweenDialog(ctx, KDateChooseEntity(), KDateChooseEntity()).setCallBack { dateChooseStart, dateChooseEnd ->
//                            //KLoggerUtils.e("回调\t开始日期:\t" + dateChooseStart.toString() + "\t结束日期:\t" + dateChooseEnd.toString())
//                            //字符判断大小，同样页可以判断日期。
//                            if (KStringUtils.compareTo(dateChooseStart?.toString(), dateChooseEnd?.toString()) > 0) {
//                                KToast.showInfo(getString(R.string.starLowEnd))//fixme 开始日期不能大于结束日期
//                            }
//                        }.end()//选择结束日期;dateBetween.star()选中开始日期
//                        dateBetween.show()
open class KDateChooseHoursBetweenDialog(ctx: Context, var dateChooseStart: KDateChooseEntity = KDateChooseEntity(), var dateChooseEnd: KDateChooseEntity = KDateChooseEntity(), isStatus: Boolean = true, isTransparent: Boolean = true) : KBaseDialog(ctx, R.layout.kera_dialog_date_choose_hours_between, isStatus, isTransparent) {
    val yyyy: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_yyyy) }
    val month: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_MM_) }
    val dd: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_dd) }
    val HH: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_HH) }
    val mm: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_mm_) }

    val txtStart: TextView? by lazy { findViewById<TextView>(R.id.crown_txt_start) }//开始日期文本
    val viewStar: View? by lazy { findViewById<View>(R.id.crown_view_start) }//开始日期横线
    val linearStart: View? by lazy { findViewById<View>(R.id.linearStart) }//开始日期
    val date_start: View? by lazy { findViewById<View>(R.id.date_start) }//开始日期

    val txtEnd: TextView? by lazy { findViewById<TextView>(R.id.crown_txt_end) }//结束日期文本
    val viewEnd: View? by lazy { findViewById<View>(R.id.crown_view_end) }//结束日期横线
    val linearEnd: View? by lazy { findViewById<View>(R.id.linearEend) }//结束日期
    val date_end: View? by lazy { findViewById<View>(R.id.date_end) }//结束日期


    val yyyy2: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_yyyy2) }
    val month2: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_MM2_) }
    val dd2: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_dd2) }
    val HH2: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_HH2) }
    val mm2: KRollerView? by lazy { findViewById<KRollerView>(R.id.crown_roller_mm2_) }

    //开始日期选择
    open fun star(): KDateChooseHoursBetweenDialog {
        txtStart?.setTextColor(Color.parseColor("#3886C6"))
        viewStar?.setBackgroundColor(Color.parseColor("#418fde"))

        txtEnd?.setTextColor(Color.parseColor("#000000"))
        viewEnd?.setBackgroundColor(Color.parseColor("#DADADA"))

        date_end?.visibility = View.INVISIBLE
        date_start?.visibility = View.VISIBLE
        return this
    }

    //结束日期选择
    open fun end(): KDateChooseHoursBetweenDialog {
        txtEnd?.setTextColor(Color.parseColor("#3886C6"))
        viewEnd?.setBackgroundColor(Color.parseColor("#418fde"))

        txtStart?.setTextColor(Color.parseColor("#000000"))
        viewStar?.setBackgroundColor(Color.parseColor("#DADADA"))

        date_start?.visibility = View.INVISIBLE
        date_end?.visibility = View.VISIBLE
        return this
    }

    init {
        if (ctx is Activity) {
            //KProportionUtils.getInstance().adapterWindow(ctx, dialog?.window)//适配
            findViewById<View>(R.id.crown_date2_parent)?.let {
                KProportionUtils.getInstance().adapterAllView(ctx, it, false, false)
            }
        }
        dialog?.window?.setWindowAnimations(R.style.kera_window_bottom)//动画
        //取消
        findViewById<View>(R.id.crown_txt_cancel)?.setOnClickListener {
            dismiss()
        }
        //完成
        findViewById<View>(R.id.crown_txt_ok)?.setOnClickListener {
            dismiss()
        }

        linearStart?.setOnClickListener {
            //开始日期
            star()
        }

        linearEnd?.setOnClickListener {
            //结束日期
            end()
        }
        star()//默认选择开始日期
        txtStart?.setText(dateChooseStart.toStringFormm())
        txtEnd?.setText(dateChooseEnd.toStringFormm())
        //年
        var list_yyyy = ArrayList<String>()
        for (i in 2010..2040) {
            list_yyyy.add(i.toString())
        }
        yyyy?.setLineColor(Color.TRANSPARENT)?.setItems(list_yyyy)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#888888"))?.setSelectTextColor(Color.parseColor("#444444"))
        yyyy2?.setLineColor(Color.TRANSPARENT)?.setItems(list_yyyy)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#888888"))?.setSelectTextColor(Color.parseColor("#444444"))
        //监听
        yyyy?.setItemSelectListener(object : KRollerView.ItemSelectListener {
            override fun onItemSelect(item: String?, position: Int) {
                item?.let {
                    dateChooseStart.yyyy = item
                }
                txtStart?.setText(dateChooseStart.toStringFormm())
            }
        })
        yyyy2?.setItemSelectListener(object : KRollerView.ItemSelectListener {
            override fun onItemSelect(item: String?, position: Int) {
                item?.let {
                    dateChooseEnd.yyyy = item
                }
                txtEnd?.setText(dateChooseEnd.toStringFormm())
            }
        })
        //月
        var list_MM = ArrayList<String>()
        for (i in 1..12) {
            list_MM.add(i.toString())
        }
        month?.setLineColor(Color.TRANSPARENT)?.setItems(list_MM)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#888888"))?.setSelectTextColor(Color.parseColor("#444444"))
        month?.setItemSelectListener(object : KRollerView.ItemSelectListener {
            override fun onItemSelect(item: String?, position: Int) {
                item?.let {
                    dateChooseStart.MM = it
                }
                txtStart?.setText(dateChooseStart.toStringFormm())
                //月份监听
                updateDays()
            }
        })
        month2?.setLineColor(Color.TRANSPARENT)?.setItems(list_MM)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#888888"))?.setSelectTextColor(Color.parseColor("#444444"))
        month2?.setItemSelectListener(object : KRollerView.ItemSelectListener {
            override fun onItemSelect(item: String?, position: Int) {
                //月份监听
                item?.let {
                    dateChooseEnd.MM = it
                }
                txtEnd?.setText(dateChooseEnd.toStringFormm())
                updateDays2()
            }
        })
        //日
        dd?.setLineColor(Color.TRANSPARENT)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#888888"))?.setSelectTextColor(Color.parseColor("#444444"))
        dd2?.setLineColor(Color.TRANSPARENT)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#888888"))?.setSelectTextColor(Color.parseColor("#444444"))
        dd?.setItemSelectListener(object : KRollerView.ItemSelectListener {
            override fun onItemSelect(item: String?, position: Int) {
                //日期监听
                item?.let {
                    dateChooseStart.dd = it
                }
                txtStart?.setText(dateChooseStart.toStringFormm())
            }
        })
        dd2?.setItemSelectListener(object : KRollerView.ItemSelectListener {
            override fun onItemSelect(item: String?, position: Int) {
                //日期监听
                item?.let {
                    dateChooseEnd.dd = it
                }
                txtEnd?.setText(dateChooseEnd.toStringFormm())
            }
        })

        //时
        var list_hours = ArrayList<String>()
        for (i in 0..23) {
            list_hours.add(i.toString())
        }
        HH?.setLineColor(Color.TRANSPARENT)?.setItems(list_hours)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#444444"))?.setSelectTextColor(Color.parseColor("#444444"))

        //监听
        HH?.setItemSelectListener(object : KRollerView.ItemSelectListener {
            override fun onItemSelect(item: String?, position: Int) {
                item?.let {
                    dateChooseStart.HH = item
                }
                txtStart?.setText(dateChooseStart.toStringFormm())
            }
        })

        HH2?.setLineColor(Color.TRANSPARENT)?.setItems(list_hours)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#444444"))?.setSelectTextColor(Color.parseColor("#444444"))

        HH2?.setItemSelectListener(object : KRollerView.ItemSelectListener {
            override fun onItemSelect(item: String?, position: Int) {
                //日期监听
                item?.let {
                    dateChooseEnd.HH = it
                }
                txtEnd?.setText(dateChooseEnd.toStringFormm())
            }
        })

        //分
        var list_minutes = ArrayList<String>()
        for (i in 0..59) {
            list_minutes.add(i.toString())
        }
        mm?.setLineColor(Color.TRANSPARENT)?.setItems(list_minutes)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#444444"))?.setSelectTextColor(Color.parseColor("#444444"))

        mm?.setItemSelectListener(object : KRollerView.ItemSelectListener {
            override fun onItemSelect(item: String?, position: Int) {
                item?.let {
                    dateChooseStart.mm = item
                }
                txtStart?.setText(dateChooseStart.toStringFormm())
            }
        })

        mm2?.setLineColor(Color.TRANSPARENT)?.setItems(list_minutes)?.setTextSize(kpx.x(40f))?.setCount(5)
                ?.setDefaultTextColor(Color.parseColor("#444444"))?.setSelectTextColor(Color.parseColor("#444444"))

        mm2?.setItemSelectListener(object : KRollerView.ItemSelectListener {
            override fun onItemSelect(item: String?, position: Int) {
                //日期监听
                item?.let {
                    dateChooseEnd.mm = it
                }
                txtEnd?.setText(dateChooseEnd.toStringFormm())
            }
        })

        //fixme 设置数据滚轮循环效果
        yyyy?.isCyclic = true
        month?.isCyclic = true
        dd?.isCyclic = true
        yyyy2?.isCyclic = true
        month2?.isCyclic = true
        dd2?.isCyclic = true
        HH?.isCyclic = true
        mm?.isCyclic = true
        HH2?.isCyclic = true
        mm2?.isCyclic = true

        isDismiss(true)
    }

    open fun updateDays() {
        //日，联动，更加月份而改变；fixme 开始日期月份天数联动。
        var list_dd = ArrayList<String>()
        var MM = month?.currentItemValue
        MM = KStringUtils.addZero(MM)
        val mDay = KCalendarUtils.getMonthOfDay(yyyy?.currentItemValue + "-" + MM, "yyyy-MM")//天数
        for (i in 1..mDay) {
            list_dd.add(i.toString())
        }
        dd?.setItems(list_dd)
        dateChooseStart.dd?.toFloat()?.toInt()?.let {
            if (it > mDay) {
                dateChooseStart.dd = mDay?.toString()
                txtStart?.setText(dateChooseStart.toStringFormm())
            }
        }
    }

    open fun updateDays2() {
        //日，联动，根据月份而改变；fixme 结束日期月份天数联动。
        var list_dd = ArrayList<String>()
        var MM2 = month2?.currentItemValue
        MM2 = KStringUtils.addZero(MM2)
        val mDay = KCalendarUtils.getMonthOfDay(yyyy2?.currentItemValue + "-" + MM2, "yyyy-MM")//天数
        for (i in 1..mDay) {
            list_dd.add(i.toString())
        }
        dd2?.setItems(list_dd)
        dateChooseEnd.dd?.toFloat()?.toInt()?.let {
            if (it > mDay) {
                dateChooseEnd.dd = mDay?.toString()
                txtEnd?.setText(dateChooseEnd.toStringFormm())
            }
        }
    }

    override fun onShow() {
        super.onShow()
        try {
            updateDays()
            updateDays2()
            //选中
            yyyy?.setCurrentPostion(yyyy!!.getItemPostion(dateChooseStart.yyyy))
            month?.setCurrentPostion(month!!.getItemPostion(dateChooseStart.MM))
            dd?.setCurrentPostion(dd!!.getItemPostion(dateChooseStart.dd))
            HH?.setCurrentPostion(HH!!.getItemPostion(dateChooseStart.HH))
            mm?.setCurrentPostion(mm!!.getItemPostion(dateChooseStart.mm))

            yyyy2?.setCurrentPostion(yyyy2!!.getItemPostion(dateChooseEnd.yyyy))
            month2?.setCurrentPostion(month2!!.getItemPostion(dateChooseEnd.MM))
            dd2?.setCurrentPostion(dd2!!.getItemPostion(dateChooseEnd.dd))
            HH2?.setCurrentPostion(HH2!!.getItemPostion(dateChooseStart.HH))
            mm2?.setCurrentPostion(mm2!!.getItemPostion(dateChooseStart.mm))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onDismiss() {
        super.onDismiss()
    }

    //日期返回回调
    open fun setCallBack(callbak: (dateChooseStart: KDateChooseEntity, dateChooseEnd: KDateChooseEntity) -> Unit): KDateChooseHoursBetweenDialog {
        //完成
        findViewById<View>(R.id.crown_txt_ok)?.setOnClickListener {
            dateChooseStart.yyyy = yyyy?.currentItemValue
            dateChooseStart.MM = month?.currentItemValue
            dateChooseStart.dd = dd?.currentItemValue
            dateChooseStart.HH = HH?.currentItemValue
            dateChooseStart.mm = mm?.currentItemValue

            dateChooseEnd.yyyy = yyyy2?.currentItemValue
            dateChooseEnd.MM = month2?.currentItemValue
            dateChooseEnd.dd = dd2?.currentItemValue
            dateChooseEnd.HH = HH2?.currentItemValue
            dateChooseEnd.mm = mm2?.currentItemValue

            callbak(dateChooseStart, dateChooseEnd)
            dismiss()
        }
        return this
    }

}