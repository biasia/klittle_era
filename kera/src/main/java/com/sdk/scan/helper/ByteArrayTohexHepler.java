package com.sdk.scan.helper;

//fixme NFC 卡号读取。
//获取 Tag 读取 ID 得到字节数组 转字符串 转码 得到卡号（默认16进制 这请自便）
//Long cardNo = Long.parseLong(ByteArrayTohexHepler.flipHexStr(ByteArrayToHexString(tag.getId())), 16);
public class ByteArrayTohexHepler  {

    public static String flipHexStr(String s) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i <= s.length() - 2; i = i + 2) {
            result.append(new StringBuilder(s.substring(i, i + 2)).reverse());
        }
        return result.reverse().toString();
    }

    // 16转10进制
    public static String ByteArrayToHexString(byte[] inarray) {
        int i, j, in;
        String[] hex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
        StringBuilder out = new StringBuilder();

        for (j = 0; j < inarray.length; ++j) {
            in = (int) inarray[j] & 0xff;
            i = (in >> 4) & 0x0f;
            out.append(hex[i]);
            i = in & 0x0f;
            out.append(hex[i]);
        } return out.toString();
    }
}