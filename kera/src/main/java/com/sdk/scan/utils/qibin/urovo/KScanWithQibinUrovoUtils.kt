package com.sdk.scan.utils.qibin.urovo

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import cn.oi.klittle.era.base.KBaseActivityManager
import cn.oi.klittle.era.utils.KAppUtils
import cn.oi.klittle.era.utils.KLoggerUtils
import com.sdk.scan.act.KScanActivity

/**
 * 东集；旗滨有两个PDA
 * 旗滨PDA扫描功能对接;这个类，不要写成静态类；就写成普通的类。这样各个Activity调用，不会冲突。
 */
class KScanWithQibinUrovoUtils {
    companion object {
        var isPDA_Qibin2: Boolean? = null;

        /**
         * fixme 判断是否为旗滨的PDA
         * 旗滨的PDA(东集); bran:	seuic	branName:	SEUIC AUTOID Q9
         * 目前对接的旗滨PDA（凯立）:bran:	kaicom	branName:	KAICOM K7-40
         * @return
         */
        fun isPDA_QibinUrovo(): Boolean {
            if (isPDA_Qibin2 != null) {
                return isPDA_Qibin2!!;
            }
            val bran = KAppUtils.getDeviceBrand().toLowerCase().trim();
            val branName = KAppUtils.getDeviceName()
            //KLoggerUtils.e("bran:\t"+bran+"\tbranName:\t"+branName);
            //bran:	urovo	branName:	Urovo DT40
            if (bran != null && bran == "urovo" || branName != null && branName == "Urovo DT40") {
                isPDA_Qibin2 = true;
                return true
            } else {
                isPDA_Qibin2 = false;
                return false;
            }
        }
    }

    private var _action: String = "android.intent.ACTION_DECODE_DATA";
    private var _action_data: String = "barcode_string";

    /**
     * 旗滨PDA扫描接收；
     * fixme 必须在旗滨PDA里的扫描头设置app里（密码：169852），手动打开 系统扫描，和 扫描回调 这两个开关才有效；
     */
    private var resultReceiver: BroadcastReceiver? = null
    private var isRegisterReceiver = false;//判断是否已注册
    fun onResultReceiver(
        activity: Activity,
        onScanResult: ((barcodeType: Byte, barcodeStr: String) -> Unit)? = null
    ) {
        if (resultReceiver == null) {
            //KLoggerUtils.e("扫描注册:" + activity);
            try {
                if (isPDA_QibinUrovo()) {
                    if (resultReceiver == null) {
                        //接收返回数据
                        resultReceiver = object : BroadcastReceiver() {
                            override fun onReceive(context: Context, intent: Intent) {
                                try {
                                    //KLoggerUtils.e("扫描接收：\t" + intent.action + "\tactivity:" + activity);
                                    if (intent.getAction().equals(_action)) {
                                        if (KScanActivity.isFastScan()) {
                                            return//fixme 防止重复扫描
                                        }
                                        //KLoggerUtils.e("当前Activity：\t" + KBaseActivityManager.getInstance().stackTopActivity + "\t" + activity);
                                        //当前Activity：	com.example.myapplication.MainActivity5@b1f9259	com.example.myapplication.MainActivity5@b1f9259
                                        //fixme 只对当前Activity进行回调。亲测有效。
                                        if (KBaseActivityManager.getInstance().stackTopActivity === activity) {
                                            var barcode =
                                                intent.getStringExtra(_action_data)?.trim();
                                            //Log.e("MainActivity", "barcode = " + barcode)
                                            if (barcode != null) {
                                                onScanResult?.let {
                                                    it(0, barcode)
                                                }
                                            }
                                        }
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                        //KLoggerUtils.e("扫描注册成功");
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                KLoggerUtils.e("扫描注册异常：\t" + e.message);
            }
        }
        if (resultReceiver != null && !isRegisterReceiver) {
            try {
                var filter = IntentFilter(_action)
                //filter.addAction(_action)
                filter.setPriority(Integer.MAX_VALUE);
                activity?.registerReceiver(resultReceiver, filter)//广播注册接收
                isRegisterReceiver = true;
            } catch (e: Exception) {
                e.printStackTrace()
                KLoggerUtils.e("扫描注册异常2：\t" + e.message);
            }
        }
    }

    /**
     * 取消扫描注册广播；旗滨的PDA，关闭Activity时必须取消注册广播；不然会异常，下次就接收不到广播了。
     */
    fun unregisterReceiver(activity: Activity) {
        try {
            if (isPDA_QibinUrovo() && resultReceiver != null && isRegisterReceiver) {
                activity?.unregisterReceiver(resultReceiver);
                isRegisterReceiver = false;
            }
        } catch (e: Exception) {
            KLoggerUtils.e("扫描销毁异常：\t" + activity + "\t" + e.message);
        }
    }


}