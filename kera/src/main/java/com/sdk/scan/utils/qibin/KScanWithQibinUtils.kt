package com.sdk.scan.utils.qibin

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import cn.oi.klittle.era.base.KBaseActivityManager
import cn.oi.klittle.era.utils.KAppUtils
import cn.oi.klittle.era.utils.KLoggerUtils
import com.sdk.scan.act.KScanActivity

/**
 * 凯立
 * 旗滨PDA扫描功能对接;这个类，不要写成静态类；就写成普通的类。这样各个Activity调用，不会冲突。
 */
class KScanWithQibinUtils {
    companion object {
        var isPDA_Qibin: Boolean? = null;

        /**
         * fixme 判断是否为旗滨的PDA
         * 旗滨的PDA（东集）; bran:	seuic	branName:	SEUIC AUTOID Q9
         * 目前对接的旗滨PDA(凯立):bran:	kaicom	branName:	KAICOM K7-40
         * @return
         */
        fun isPDA_Qibin(): Boolean {
            if (isPDA_Qibin != null) {
                return isPDA_Qibin!!;
            }
            val bran = KAppUtils.getDeviceBrand().toLowerCase().trim();
            val branName = KAppUtils.getDeviceName()
            //KLoggerUtils.e("bran:\t"+bran+"\tbranName:\t"+branName);
            if (bran != null && bran == "kaicom" || branName != null && branName == "KAICOM K7-40") {
                isPDA_Qibin = true;
                return true
            } else {
                isPDA_Qibin = false;
                return false;
            }
        }
    }

    private var _action: String = "com.android.receive_scan_action";
    private var _action_data: String = "data";

    /**
     * 旗滨PDA扫描接收；
     * fixme 必须在旗滨PDA里的扫描头设置app里（密码：169852），手动打开 系统扫描，和 扫描回调 这两个开关才有效；
     */
    private var resultReceiver: BroadcastReceiver? = null
    fun onResultReceiver(
        activity: Activity,
        onScanResult: ((barcodeType: Byte, barcodeStr: String) -> Unit)? = null
    ) {
        if (resultReceiver == null) {
            //KLoggerUtils.e("扫描注册");
            try {
                if (isPDA_Qibin()) {
                    if (resultReceiver == null) {
                        //接收返回数据
                        resultReceiver = object : BroadcastReceiver() {
                            override fun onReceive(context: Context, intent: Intent) {
                                try {
                                    //KLoggerUtils.e("扫描接收：\t" + intent.action);
                                    if (intent.getAction().equals(_action)) {
                                        if (KScanActivity.isFastScan()) {
                                            return//fixme 防止重复扫描
                                        }
                                        //KLoggerUtils.e("当前Activity：\t" + KBaseActivityManager.getInstance().stackTopActivity + "\t" + activity);
                                        //当前Activity：	com.example.myapplication.MainActivity5@b1f9259	com.example.myapplication.MainActivity5@b1f9259
                                        //fixme 只对当前Activity进行回调。亲测有效。
                                        if (KBaseActivityManager.getInstance().stackTopActivity === activity) {
                                            var barcode =
                                                intent.getStringExtra(_action_data)?.trim();
                                            //Log.e("MainActivity", "barcode = " + String(barcode!!))
                                            if (barcode != null) {
                                                onScanResult?.let {
                                                    it(0, barcode)
                                                }
                                            }
                                        }
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            }
                        }
                        var filter = IntentFilter(_action)
                        //filter.addAction(_action)
                        activity.registerReceiver(resultReceiver, filter)//广播注册接收
                        //KLoggerUtils.e("扫描注册成功");
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                KLoggerUtils.e("扫描注册异常：\t" + e.message);
            }
        }
    }

    /**
     * 取消扫描注册广播；旗滨的PDA，关闭Activity时必须取消注册广播；不然会异常，下次就接收不到广播了。
     */
    fun unregisterReceiver(activity: Activity) {
        try {
            if (isPDA_Qibin() && resultReceiver != null) {
                activity?.unregisterReceiver(resultReceiver);
            }
        } catch (e: Exception) {
        }
    }


}