//package com.sdk.scan.utils.qibin;
//
//import android.app.Activity;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.os.Bundle;
//import android.support.design.widget.FloatingActionButton;
//import android.support.design.widget.Snackbar;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.view.Gravity;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import com.kaicom.broad.broadcastdemo.Broadcast;
//
//public class BroadCastDemo extends Activity {
//    private Broadcast bc;
//    private EditText prefix, suffix, interval, pda_sn;
//    private int offset = 0;
//    private BroadcastReceiver mBrReceiver;
//    private boolean statusbar = true, apk_install = true;
//    private String sn;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_broad_cast_demo);
//        prefix = (EditText) findViewById(R.id.et_prefix);
//        suffix = (EditText) findViewById(R.id.et_suffix);
//        interval = (EditText) findViewById(R.id.et_interval);
//        pda_sn = (EditText) findViewById(R.id.et_pda_sn);
//        bc = Broadcast.getInstance(this);
//        getsystemscandata();//注册接收数据广播，以TOAST形式显示，退出界面也可以查看数据
//
//
//        Intent intent =new Intent("com.android.service_settings");
//        intent.putExtra("pda_sn","string");//机器码设置成“string”
//        this.sendBroadcast(intent);
//
//
//        findViewById(R.id.turn_message_sounds_on).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bc.TurnOnOffScanMessageTone(true);//扫描声音开
//            }
//        });
//        findViewById(R.id.turn_message_sounds_off).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bc.TurnOnOffScanMessageTone(false);//扫描声音关
//            }
//        });
//        findViewById(R.id.turn_ScanMessage_Vibrator_on).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bc.TurnOnOffScanMessageVibrator(true);//扫描震动开
//            }
//        });
//        findViewById(R.id.turn_ScanMessage_Vibrator_off).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bc.TurnOnOffScanMessageVibrator(false);//扫描震动关
//            }
//        });
//        findViewById(R.id.turn_Continue_Scan_on).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bc.sendContinueConfig(true);//连续扫描开
//            }
//        });
//        findViewById(R.id.turn_Continue_Scan_off).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bc.sendContinueConfig(false);//连续扫描关
//            }
//        });
//        findViewById(R.id.set_prefix).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bc.sendPrefixConfig(prefix.getText().toString().trim());//设置前缀字符
//            }
//        });
//
//        findViewById(R.id.set_suffix).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bc.sendSuffixConfig(suffix.getText().toString().trim());//设置后缀字符
//            }
//        });
//        findViewById(R.id.set_interval).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bc.sendIntervalConfig(Integer.parseInt(interval.getText().toString().trim()));//设置扫描间隔
//            }
//        });
//        findViewById(R.id.set_pda_sn).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                bc.setPdaSystime(pda_sn.getText().toString().trim());
//            }
//        });
//        final Button btn_statusba = (Button) findViewById(R.id.set_pda_statusbar);
//        btn_statusba.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (statusbar) {
//                    bc.setPdaStatusbar("on");
//                    btn_statusba.setText("状态栏on");
//                } else {
//                    bc.setPdaStatusbar("off");
//                    btn_statusba.setText("状态栏off");
//                }
//                statusbar = !statusbar;
//            }
//        });
////        final Button apd_install = (Button) findViewById(R.id.set_pda_install);
////        apd_install.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                if (apk_install) {
////                    bc.setPdaStatusbar("on");
////                    apd_install.setText("apk安装on");
////                } else {
////                    bc.setPdaStatusbar("off");
////                    apd_install.setText("apk安装off");
////                }
////                apk_install = !apk_install;
////            }
////        });
//        final Button pda_sn = (Button) findViewById(R.id.get_pda_sn);
//        pda_sn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                pda_sn.setText("唯一码：" + getsn());
//            }
//        });
//        findViewById(R.id.app_finish).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                unregisterReceiver(mBrReceiver);
//                Toast.makeText(getApplicationContext(), "反注册广播完成", Toast.LENGTH_SHORT).show();
//                try {
//                    Thread.sleep(1000);
//                } catch (Exception e) {
//                }
//                finish();
//            }
//        });
//
//    }
//
//    /**
//     * 获取接受到的扫描数据,注册广播
//     */
//    public void getsystemscandata() {
//        final String getstr = "com.android.receive_scan_action";
//        mBrReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                if (intent.getAction().equals(getstr)) {
//                    String datat = intent.getStringExtra("data");
//                    Toast toast = Toast.makeText(getApplicationContext(),
//                            "data: " + datat, Toast.LENGTH_SHORT);
////                            "data: " + datat + "\n" + getstr, Toast.LENGTH_SHORT);
//                    toast.setGravity(Gravity.CENTER, 0, offset);// 居中显示 x,y
//                    toast.show();
//                    offset = offset + 50;//设置TOAST显示偏移，便于区分不同次接收到的数据
//                    if (offset > 400) offset = 0;
//                }
//            }
//        };
//        IntentFilter filter = new IntentFilter(getstr);
//        registerReceiver(mBrReceiver, filter);
//        Toast.makeText(this, "注册广播完成，自动接收数据", Toast.LENGTH_LONG).show();
//
//    }
//
//    @Override
//    public void onBackPressed() {
//        //super.onBackPressed();
//        Intent i = new Intent(Intent.ACTION_MAIN);
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        i.addCategory(Intent.CATEGORY_HOME);
//        startActivity(i);
//    }
//
//    /**
//     * 获取sn
//     */
//    public String getsn() {
//        Intent intent = new Intent("com.android.receive_get_pda_sn");
//        sendBroadcast(intent);
//        BroadcastReceiver mBrReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                if (intent.getAction().equals("com.android.receive_pda_sn")) {
//                    String datat = intent.getStringExtra("pda_sn");
//                    // tv_para2.setText(" " + datat);
//                    sn = datat;
//                }
//            }
//        };
//        IntentFilter filter = new IntentFilter("com.android.receive_pda_sn");
//        registerReceiver(mBrReceiver, filter);
//        return sn;
//
//    }
//}
