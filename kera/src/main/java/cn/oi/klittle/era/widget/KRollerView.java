package cn.oi.klittle.era.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.Gravity;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.oi.klittle.era.comm.kpx;
import cn.oi.klittle.era.exception.KCatchException;
import cn.oi.klittle.era.utils.KLoggerUtils;
import cn.oi.klittle.era.utils.KRegexUtils;
import cn.oi.klittle.era.utils.KScrimUtil;
import cn.oi.klittle.era.utils.KStringUtils;
import cn.oi.klittle.era.widget.datepicker.WheelPicker;
//import cn.android.support.v7.lib.sin.crown.view.picker.WheelPicker;
//        使用说明
//        RollerView view = findViewById(R.id.myFramLayout);
//        final List<String> items = new ArrayList<>();
//        for (int i = 1; i <= 12; i++) {
//            items.add(i + "月");
//        }
//        view.setItems(items);//添加数据
//        view.setItemSelectListener(new RollerView.ItemSelectListener() {//添加回调
//            @Override
//            public void onItemSelect(String item, int postion) {
//                //Log.e("test", "数据:\t" + item + "\t下标:\t" + postion);
//            }
//        });
//        view.setCurrentPostion(6,true);//选中指定下标

//注意：android:layout_width="@dimen/x100_100" 設置寬度和高度時候，一定要設置明確的值。不能設置為0dp ,不然子View無法顯示。
//fixme hasAtmospheric 是否显示空气感效果
//fixme isCyclic 数据是否循环展示
//fixme isCurved 滚轮是否为卷曲效果

/**
 * 滚轮选择器【新版】,解决华为mate9+荣耀8不显示问题
 * Created by 彭治铭 on 2018/3/29.
 */

public class KRollerView extends WheelPicker {

    /**
     * 设置中间两条线条的颜色
     *
     * @param lineColor
     */
    public KRollerView setLineColor(int lineColor) {
        this.lineColor = lineColor;
        invalidate();
        return this;
    }

    /**
     * 设置线条的高度【边框的宽度】
     *
     * @param strokeWidth
     */
    public KRollerView setStrokeWidth(int strokeWidth) {
        this.strokeWidth = strokeWidth;
        invalidate();
        return this;
    }

    /**
     * 设置选中字体的颜色
     *
     * @param selectTextColor
     */
    public KRollerView setSelectTextColor(int selectTextColor) {
        setSelectedItemTextColor(selectTextColor);
        return this;
    }

    /**
     * 设置默认字体的颜色
     *
     * @param defaultTextColor
     */
    public KRollerView setDefaultTextColor(int defaultTextColor) {
        setItemTextColor(defaultTextColor);
        return this;
    }

    int textSize = 0;

    //设置字体大小
    public KRollerView setTextSize(float textSize) {
        setItemTextSize((int) textSize);
        this.textSize = (int) textSize;
        //线条的位置
        startLine = mDrawnCenterY - getItemTextSize() - getItemTextSize() / 4;
        endLine = mDrawnCenterY + getItemTextSize() / 2;
        postInvalidate();
        return this;
    }

    /**
     * 设置回调接口，返回选中的数据和下标
     *
     * @param itemSelectListener
     */
    public KRollerView setItemSelectListener(final ItemSelectListener itemSelectListener) {
        setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelPicker picker, Object data, int position) {
                itemSelectListener.onItemSelect(data.toString(), position);
            }
        });
        return this;
    }

    /**
     * 設置當前顯示item的個數【一定要在設置數據之前，設置。必須】
     *
     * @param count
     */
    public KRollerView setCount(int count) {
        setVisibleItemCount(count);
        return this;
    }

    //保存数据的下标
    public Map<String, Integer> map = new HashMap();

    public List<String> items;//数据集合

    /**
     * fixme ======================================================================================= 设置数据集合
     *
     * @param items
     */
    public KRollerView setItems(List<String> items) {
        if (this.items == null) {
            this.items = new ArrayList<>();//与参数传入的数据不会发生关联。
        }
        this.items.clear();
        this.items.addAll(items);
        setData(this.items);
        for (int i = 0; i < items.size(); i++) {
            map.put(items.get(i), i);//fixme 保存数据的下标
        }
        return this;
    }

    //getCurrentItemValue() 获取当前选中的值
    //getCurrentPostion() 获取当前选中的下标

    /**
     * 根据数据，获取对应下标；fixme 兼容数字类型0，如：1和 01
     *
     * @param data
     * @return
     */
    public int getItemPostion(String data) {
        if (data != null) {
            if (map.containsKey(data)) {
                return map.get(data);
            } else {
                //fixme 数字类型兼容0，如：1和 01
                if (data.length() == 1 && KRegexUtils.INSTANCE.isNumber(data, 1)) {
                    //fixme 没有0，加上0。
                    String data2 = "0" + data;
                    if (map.containsKey(data2)) {
                        return map.get(data2);
                    }
                } else if (data.length() == 2 && data.substring(0, 1).equals("0") && KRegexUtils.INSTANCE.isNumber(data, 2)) {
                    //fixme 有0，去除0。
                    String data2 = data.substring(1);
                    if (map.containsKey(data2)) {
                        return map.get(data2);
                    }
                }
            }
        }
        return 0;
    }

    /**
     * 选中指定下标【数据集合的实际下标】。
     *
     * @param position
     * @return
     */
    public KRollerView setCurrentPostion(int position) {
        try {
            setSelectedItemPosition(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    /**
     * 选中指定的数据。
     *
     * @param value
     * @return
     */
    public KRollerView setCurrentValue(String value) {
        try {
            if (value != null && items != null && value.trim().length() > 0) {
                for (int i = 0; i < items.size(); i++) {
                    if (items.get(i).trim().equals(value.trim())) {//判断数据是否相等
                        if (getCurrentPostion() != i) {
                            setCurrentPostion(i);
                        }
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    //获取当前选中下标【不算空格。是原数据的下标，即实际下标。】
    public int getCurrentPostion() {
        return getCurrentItemPosition();
    }

    //获取当前选中的值
    public String getCurrentItemValue() {
        try {
            return items.get(getCurrentItemPosition());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public KRollerView(Context context) {
        super(context);
        try {
            setCurved(true);//设置卷尺效果
            setCyclic(true);//fixme 默认设置数据滚轮循环显示。
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public KRollerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        try {
            setCurved(true);//fixme 设置卷尺效果
            setCyclic(true);//fixme 默认设置数据滚轮循环显示。
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (paint == null) {
            paint = new Paint();
            paint.setAntiAlias(true);
            paint.setDither(true);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        try {
            if (mVisibleItemCount <= 0) {
                mVisibleItemCount = 5;//item显示个数
            }
            int chidHeith = h / mVisibleItemCount;
            //设置默认字体大小
            if (textSize <= 0) {
                int textSize = (int) (chidHeith * 0.72);//字体大小。默认就是这个了。
                setItemTextSize(textSize);
            }
            //fixme computeCurrentItemRect() 父类绘制中间选中区域。mHalfItemHeightOffset控制间距的偏移量。
            startLine = mWheelCenterY - mHalfItemHeight - mHalfItemHeightOffset;
            endLine = mWheelCenterY + mHalfItemHeight + mHalfItemHeightOffset;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Paint paint;
    private int startLine;
    private int endLine;
    private Path linePath = new Path();
    public int lineColor = Color.parseColor("#000000");//线条颜色
    public int strokeWidth = (int) kpx.INSTANCE.x(2);//线条的宽度
    public boolean isDrawLine = true;//fixme 是否绘制中间两条线条。
    public boolean isDrawShadow = true;//fixme 是否绘制渐变阴影效果。

    @Override
    public void draw(Canvas canvas) {
        try {
            if (mData != null && mData.size() > 0) {
                super.draw(canvas);
            }
            if (isDrawLine) {
                paint.setStyle(Paint.Style.STROKE);
                paint.setColor(lineColor);//线条颜色
                paint.setStrokeWidth(strokeWidth);//线条宽度
                paint.setShader(null);
                //中间两条线
                linePath.reset();
                //fixme 第一条线
                linePath.moveTo(0, startLine);
                linePath.lineTo(getWidth(), startLine);
                //fixme 第二条线
                linePath.moveTo(0, endLine);
                linePath.lineTo(getWidth(), endLine);
                canvas.drawPath(linePath, paint);
            }
        } catch (Exception e) {
            KLoggerUtils.INSTANCE.e("滚轮控制绘制异常：\t" + KCatchException.getExceptionMsg(e), true);
        }
    }

    //回调
    public interface ItemSelectListener {
        //原始数据，和下标
        void onItemSelect(String item, int position);
    }

}
