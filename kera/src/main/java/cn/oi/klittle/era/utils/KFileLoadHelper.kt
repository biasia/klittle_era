package cn.oi.klittle.era.utils

import cn.oi.klittle.era.exception.KCatchException
import cn.oi.klittle.era.utils.KCacheUtils.TIME_DAY
import cn.oi.klittle.era.utils.KCacheUtils.getSecret
import cn.oi.klittle.era.utils.KCacheUtils.putSecret
import cn.oi.klittle.era.utils.KLoggerUtils.e

object KFileLoadHelper {

    private fun getUrlFileCacheLengthKey(url: String): String? {
        return "kurl_leng:$url"
    }

    //缓存网络文件大小
    fun setUrlFileCacheLength(url: String?, max: Long) {
        if (url != null) {
            KCacheUtils.putSecret(getUrlFileCacheLengthKey(url), max, TIME_DAY) //本地缓存网络文件长度，保存时间为一天（24小时）
        }
    }

    //获取网络文件大小
    fun getUrlFileCacheLength(url: String?): Long {
        if (url != null) {
            var max: Long? = KCacheUtils.getSecret(getUrlFileCacheLengthKey(url))
            max?.let {
                return it
            }
        }
        return 0
    }
}